
install(FILES
        h2tablereader.hh
        h2tables.hh
        h2tables.inc
        h2tabulated.hh
        DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/dumux/material/components)
