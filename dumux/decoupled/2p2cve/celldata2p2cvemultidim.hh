// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
#ifndef DUMUX_CELLDATA2P2CVEMULTIDIM_HH
#define DUMUX_CELLDATA2P2CVEMULTIDIM_HH

#include "celldata2p2cve.hh"

/**
 * \file
 * \brief  Class including data of one grid cell
 */

namespace Dumux
{

/*!
 * \ingroup IMPES
 */
//! Class including the variables and data of discretized data of the constitutive relations for one grid cell.
/*! The variables of two-phase flow, which are phase pressures and saturations are stored in this class.
 * Further, resulting cell values for constitutive relationships like
 * mobilities, fractional flow functions and capillary pressure are stored.
 * Additionally, data assigned to cell-cell interfaces, so-called flux-data are stored.
 *
 * \tparam TypeTag The problem TypeTag
 */
template<class TypeTag>
class CellData2P2CVEMultiDim: public CellData2P2CVE<TypeTag>
{
private:
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using GridView = typename GetPropType<TypeTag, Properties::GridGeometry>::GridView;
    using Grid = GetPropType<TypeTag, Properties::Grid>;
    using Problem = GetPropType<TypeTag, Properties::Problem>;

    using Element = typename GridView::Traits::template Codim<0>::Entity;
    using Indices = GetPropType<TypeTag, Properties::Indices>;

    enum
    {
        wPhaseIdx = Indices::wPhaseIdx, nPhaseIdx = Indices::nPhaseIdx,
        wCompIdx = Indices::wCompIdx, nCompIdx = Indices::nCompIdx,
        numPhases = getPropValue<TypeTag, Properties::NumPhases>(),
        numComponents = getPropValue<TypeTag, Properties::NumComponents>(),
        dim = GridView::dimension, dimWorld = GridView::dimensionworld
    };

    typedef Dune::FieldVector<Scalar, dimWorld> GlobalPosition;
    typedef std::array<unsigned int, dim> CellArray;

public:
    /*!
     * \brief A container for all necessary variables to map an old solution to a new grid
     * If the primary variables (pressure, total concentrations) are mapped to a new grid,
     * the secondary variables can be calulated. For the mapping between sons and father, it
     * is in addition necessary to know about how many sons live in each father ("count").
     * While only one phase pressure is PV, the method updateMaterialLaws() that
     * recalculates the secondary variables needs both phase pressures (initiated via the
     * capillary pressure of the last time step) to start the iteration to determine
     * appropriate phase composition and pc. Hence, both phase pressures are mapped
     * to the new solution. The other parameters are needed to calculate values for father/son cells,
     * e.g., the gasPlumeDist and many others are needed to map from a VE cell onto a full dimensional cell,
     * the density is needed to determine the correct pressure value to map from full dimensional
     * cells to VE cells.
     */
    struct AdaptedValues
    {
        Dune::FieldVector<Scalar,2> totalConcentration; //!< Transport primary variables
        Dune::FieldVector<Scalar,2> pressure; //!< Pressure primary variables
        Scalar cellVolume; //!< Cell volume for transformation of volume-specific primary variables
        FluxData2P2C<TypeTag> fluxData; //!< Information on old flux direction
        int count; //!< counts the number of cells averaged
        Scalar gasPlumeDist; //!< Distance of gas plume from bottom
        Scalar minGasPlumeDist; //!< Distance of former gas plume from bottom
        Scalar epsilonHeight; //!< Corrected bottom height due to volume mismatch in liquid phase
        Scalar densityLiquidInPlume;
        Scalar densityLiquidBelowPlume;
        Dune::FieldVector<Scalar,2> massFractionLiquidInPlume;
        Dune::FieldVector<Scalar,2> massFractionLiquidBelowPlume;
        Dune::FieldVector<Scalar,2> massFractionGas;
        int veModel; //!< Type of VE model
        Dune::FieldVector<Scalar,2> density;
        AdaptedValues()
        {
            totalConcentration=0.;
            pressure = 0.;
            cellVolume = 0.;
            count = 0;
            gasPlumeDist = 0.;
            minGasPlumeDist = 1e100;
            epsilonHeight = 0.;
            densityLiquidInPlume = 0.;
            densityLiquidBelowPlume = 0.;
            massFractionLiquidInPlume = 0.;
            massFractionLiquidBelowPlume = 0.;
            massFractionGas = 0.;
            veModel = 0;
            density = 0.;
        }
    };

    typedef AdaptedValues LoadBalanceData;

    //! Constructs a CellData2P object
    CellData2P2CVEMultiDim()
    {
        this->setVeModel(2);
    }

    //! Stores values to be adapted in an adaptedValues container
    /**
     * Stores values to be adapted from the current CellData objects into
     * the adaptation container in order to be mapped on a new grid.
     *
     * \param adaptedValues Container for model-specific values to be adapted
     * \param element The element to be stored
     */
    void storeAdaptionValues(AdaptedValues& adaptedValues, const Element& element)
    {
        adaptedValues.totalConcentration[wCompIdx]= this->massConcentration(wCompIdx);
        adaptedValues.totalConcentration[nCompIdx]= this->massConcentration(nCompIdx);
        adaptedValues.pressure[wPhaseIdx] = this->pressure(wPhaseIdx);
        adaptedValues.pressure[nPhaseIdx] = this->pressure(nPhaseIdx);
        adaptedValues.cellVolume = element.geometry().volume();
        adaptedValues.fluxData=this->fluxData();
        adaptedValues.gasPlumeDist = this->gasPlumeDist();
        adaptedValues.minGasPlumeDist = this->minGasPlumeDist();
        adaptedValues.epsilonHeight = this->epsilonHeight();
        adaptedValues.densityLiquidInPlume = this->densityLiquidInPlume();
        adaptedValues.densityLiquidBelowPlume = this->densityLiquidBelowPlume();
        adaptedValues.massFractionLiquidInPlume[wCompIdx] = this->massFractionLiquidInPlume(wCompIdx);
        adaptedValues.massFractionLiquidInPlume[nCompIdx] = this->massFractionLiquidInPlume(nCompIdx);
        adaptedValues.massFractionLiquidBelowPlume[wCompIdx] = this->massFractionLiquidBelowPlume(wCompIdx);
        adaptedValues.massFractionLiquidBelowPlume[nCompIdx] = this->massFractionLiquidBelowPlume(nCompIdx);
        adaptedValues.massFractionGas[wCompIdx] = this->massFraction(nPhaseIdx, wCompIdx);
        adaptedValues.massFractionGas[nCompIdx] = this->massFraction(nPhaseIdx, nCompIdx);
        adaptedValues.veModel = this->veModel();
        adaptedValues.density[wPhaseIdx] = this->density(wPhaseIdx);
        adaptedValues.density[nPhaseIdx] = this->density(nPhaseIdx);
    }

    //! Stores sons entries into father element for averaging
    /**
     * Sum up the adaptedValues (sons values) into father element. We store from leaf
     * upwards, so sons are stored first, then cells on the next leaf (=fathers)
     * can be averaged. The stored values are wrong by one factor which is the number of sons
     * associated with the current father. This is corrected in setAdaptionValues by dividing
     * there through the count.
     *
     * \param adaptedValues Container for model-specific values to be adapted
     * \param adaptedValuesFather Values to be adapted of father cell
     * \param fatherElement The element of the father
     */
    static void storeAdaptionValues(AdaptedValues& adaptedValues,
                                    AdaptedValues& adaptedValuesFather,
                                    const Element& fatherElement,
                                    Problem& problem)
    {
        adaptedValuesFather.totalConcentration[wCompIdx]
            += adaptedValues.cellVolume * adaptedValues.totalConcentration[wCompIdx] / fatherElement.geometry().volume();
        adaptedValuesFather.totalConcentration[nCompIdx]
            += adaptedValues.cellVolume * adaptedValues.totalConcentration[nCompIdx] / fatherElement.geometry().volume();

        adaptedValuesFather.cellVolume = fatherElement.geometry().volume();

        adaptedValuesFather.density[wPhaseIdx] =
            std::max(adaptedValues.density[wPhaseIdx], adaptedValuesFather.density[wPhaseIdx]);
        adaptedValuesFather.density[nPhaseIdx] =
            std::max(adaptedValues.density[nPhaseIdx], adaptedValuesFather.density[nPhaseIdx]);
        const Scalar gravity = problem.gravity().two_norm();
        const CellArray numberOfCells = problem.numberOfCells();
        const int MaxLevel = getParam<int>("GridAdapt.MaxLevel");
        const double deltaZ = problem.aquiferHeight()/(numberOfCells[dim-1]*std::pow(2, MaxLevel));
        adaptedValuesFather.pressure[wPhaseIdx] =
            std::max(adaptedValues.pressure[wPhaseIdx], adaptedValuesFather.pressure[wPhaseIdx]);
        adaptedValuesFather.pressure[nPhaseIdx] =
            std::max(adaptedValues.pressure[nPhaseIdx], adaptedValuesFather.pressure[nPhaseIdx]);
        if(fatherElement.level() == 0 && adaptedValuesFather.count == 2)//TODO: this is hardcoded for blue refinement
        {
            adaptedValuesFather.pressure[wPhaseIdx] = (adaptedValuesFather.pressure[wPhaseIdx] +
                adaptedValuesFather.density[wPhaseIdx]*gravity*deltaZ/2.0);
            adaptedValuesFather.pressure[nPhaseIdx] = (adaptedValuesFather.pressure[nPhaseIdx] +
                adaptedValuesFather.density[nPhaseIdx]*gravity*deltaZ/2.0);
        }

        adaptedValuesFather.minGasPlumeDist = adaptedValues.minGasPlumeDist;
    }
    //! Set adapted values in CellData
    /**
     * This methods stores reconstructed values into the cellData object, by
     * this setting a newly mapped solution to the storage container of the
     * sequential models.
     *
     * \param adaptedValues Container for model-specific values to be adapted
     * \param element The element where things are stored.
     */
    void setAdaptionValues(AdaptedValues& adaptedValues, const Element& element)
    {
        this->setMassConcentration(wCompIdx,
                                   adaptedValues.totalConcentration[wCompIdx]);
        this->setMassConcentration(nCompIdx,
                                   adaptedValues.totalConcentration[nCompIdx]);
        this->manipulateFluidState().setPressure(wPhaseIdx,
                          adaptedValues.pressure[wPhaseIdx]);
        this->manipulateFluidState().setPressure(nPhaseIdx,
                          adaptedValues.pressure[nPhaseIdx]);

        //copy flux directions
        this->fluxData()=adaptedValues.fluxData;

        //copy minGasPlumeDist
        this->setMinGasPlumeDist(adaptedValues.minGasPlumeDist);

        // always recalculate volume derivatives
        this->volumeDerivativesAvailable(false);
    }

    //! Reconstructs sons entries from data of father cell
    /**
     * Reconstructs a new solution from a father cell into a newly
     * generated son cell. New cell is stored into the global
     * adaptationMap.
     *
     * \param adaptionMap Global map storing all values to be adapted
     * \param father Entity Pointer to the father cell
     * \param son Entity Pointer to the newly created son cell
     * \param problem The problem
     */
    static void reconstructAdaptionValues(Dune::PersistentContainer<Grid, AdaptedValues>& adaptionMap,
            const Element& father, const Element& son, Problem& problem)
    {
        AdaptedValues& adaptedValues = adaptionMap[son];
        AdaptedValues& adaptedValuesFather = adaptionMap[father];

        const GlobalPosition globalPos = son.geometry().center();
        const CellArray numberOfCells = problem.numberOfCells();
        const int MaxLevel = getParam<int>("GridAdapt.MaxLevel");
        const double deltaZ = problem.aquiferHeight()/(numberOfCells[dim-1]*std::pow(2, MaxLevel));
        const Scalar localTransform = father.geometry().center()[dim-1] - problem.aquiferHeight()/2.0;
        const Scalar top = (globalPos[dim - 1] + deltaZ/2.0) - localTransform;
        const Scalar bottom = (globalPos[dim - 1] - deltaZ/2.0) - localTransform;
        const Scalar positionZ = globalPos[dim - 1] - localTransform;

        Dune::FieldVector<Scalar,2> concentrationIntegral;
        concentrationIntegral = problem.pressureModel().concentrationIntegral(bottom, top, adaptedValuesFather, father);
        adaptedValues.totalConcentration[wCompIdx] = concentrationIntegral[wCompIdx]/(top-bottom);
        adaptedValues.totalConcentration[nCompIdx] = concentrationIntegral[nCompIdx]/(top-bottom);
        adaptedValues.pressure[wPhaseIdx] =
            problem.pressureModel().reconstPressure(positionZ, wPhaseIdx, adaptedValuesFather, father);
        adaptedValues.pressure[nPhaseIdx] =
            problem.pressureModel().reconstPressure(positionZ, nPhaseIdx, adaptedValuesFather, father);

        adaptedValues.minGasPlumeDist = adaptedValuesFather.minGasPlumeDist;
    }
};
}
#endif
