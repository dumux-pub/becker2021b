// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
#ifndef DUMUX_GRIDADAPTIONINDICATOR2P2CVEFULLD_HH
#define DUMUX_GRIDADAPTIONINDICATOR2P2CVEFULLD_HH

#include <dumux/decoupled/2pve/gridadaptionindicator2pvefulld.hh>

/**
 * @file
 * @brief  Class defining a standard, saturation dependent indicator for grid adaption
 */
namespace Dumux
{
/*!\ingroup IMPES
 * @brief  Class defining a standard, saturation dependent indicator for grid adaption
 *
 * \tparam TypeTag The problem TypeTag
 */
template<class TypeTag>
class GridAdaptionIndicator2P2CVEFullD: public GridAdaptionIndicator2PVEFullD<TypeTag>
{
private:
    using ParentType = GridAdaptionIndicator2PVEFullD<TypeTag>;
    using Problem = GetPropType<TypeTag, Properties::Problem>;
    using GridView = typename GetPropType<TypeTag, Properties::GridGeometry>::GridView;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using Element = typename GridView::Traits::template Codim<0>::Entity;

    using SolutionTypes = GetProp<TypeTag, Properties::SolutionTypes>;
    using ScalarSolutionType = typename SolutionTypes::ScalarSolution;
    using ElementMapper = typename SolutionTypes::ElementMapper;

    using SpatialParams = GetPropType<TypeTag, Properties::SpatialParams>;
    using MaterialLaw = GetPropType<TypeTag, Properties::MaterialLaw>;

    using Indices = GetPropType<TypeTag, Properties::Indices>;

    using FluidSystem = GetPropType<TypeTag, Properties::FluidSystem>;
    using FluidState = GetPropType<TypeTag, Properties::FluidState>;

    enum
    {
        sw = Indices::saturationW,
        sn = Indices::saturationNw
    };
    enum
    {
        wPhaseIdx = Indices::wPhaseIdx,
        nPhaseIdx = Indices::nPhaseIdx
    };
    enum
    {
        dim = GridView::dimension, dimWorld = GridView::dimensionworld,
        wCompIdx = Indices::wPhaseIdx, nCompIdx = Indices::nPhaseIdx,
        numPhases = getPropValue<TypeTag, Properties::NumPhases>()
    };
    enum VEModel
    {
        sharpInterface,
        capillaryFringe
    };

    using CellData = GetPropType<TypeTag, Properties::CellData>;
    using GlobalPosition = Dune::FieldVector<Scalar, dimWorld>;

    using CellArray = std::array<unsigned int, dim>;
    using DimVector = Dune::FieldVector<Scalar, dim>;
    using PhaseVector = Dune::FieldVector<Scalar, numPhases>;


public:
    /*! \brief Calculates the indicator used for refinement/coarsening for each column.
     *
     * This indicator is based on the saturation profile of the wetting phase compared to the VE-saturation profile.
     */
    void calculateIndicator()
    {
        const CellArray numberOfCells = problem_.numberOfCells();
        const int numberOfColumns = numberOfCells[0];
        const Scalar aquiferHeight = problem_.aquiferHeight();
        const int maxLevel = getParam<int>("GridAdapt.MaxLevel");
        const double deltaZ = aquiferHeight/(numberOfCells[dim - 1]*std::pow(2, maxLevel));
        const Scalar gravity = problem_.gravity().two_norm();
        // prepare an indicator for refinement
        if(indicatorVector_.size() != numberOfColumns)
        {
            indicatorVector_.resize(numberOfColumns);
        }
        indicatorVector_ = 0.0;

        const int veModel = getParam<int>("VE.VEModel");
        std::multimap<int, Element> mapAllColumns = problem_.getColumnMap();
        for(int i=0;i<numberOfColumns;i++)
        {
            if(i < 5)//never have VE in the first columns
            {
                indicatorVector_[i] = 2;
                continue;
            }

            const int columnModel = problem_.getColumnModel(i);
            if(columnModel == 0 || columnModel == 1)
                continue;
            Scalar averageTotalConcGas = 0.0;// average total gas concentration in column
            PhaseVector columnRefPressures;// reference pressures in column
            columnRefPressures[wPhaseIdx] = 0.0;
            columnRefPressures[nPhaseIdx] = 1e100;
            Scalar columnMinPressureW = 1e100;
            Scalar totalVolume = 0.0;// total volume of column
            Scalar gasVolume = 0.0;// volume with SatN>0.0 in one column;
            Scalar minGasPlumeDist = 0.0;
            typename std::map<int, Element>::iterator it = mapAllColumns.lower_bound(i);
            dummy_ = it->second;
            for (; it != mapAllColumns.upper_bound(i); ++it)
            {
                const Element& element = it->second;
                const int globalIdxI = problem_.variables().index(element);
                const CellData& cellData = problem_.variables().cellData(globalIdxI);
                const Scalar volume = element.geometry().volume();
                minGasPlumeDist = cellData.minGasPlumeDist();// should be the same value within one column
                averageTotalConcGas += cellData.totalConcentration(nCompIdx) * volume;
                totalVolume += volume;

                if(element.geometry().center()[dim-1] > minGasPlumeDist)
                    columnRefPressures[wPhaseIdx] = std::max(columnRefPressures[wPhaseIdx], cellData.pressure(wPhaseIdx));
                columnRefPressures[nPhaseIdx] = std::min(columnRefPressures[nPhaseIdx], cellData.pressure(nPhaseIdx));
                columnMinPressureW = std::min(columnMinPressureW, cellData.pressure(wPhaseIdx));
            }
            averageTotalConcGas = averageTotalConcGas/totalVolume;
            if(columnRefPressures[wPhaseIdx] < 1e-8)
                columnRefPressures[wPhaseIdx] = columnMinPressureW;

            const std::vector<Scalar> distances = calculateGasPlumeDist(averageTotalConcGas, columnRefPressures, minGasPlumeDist);
            const Scalar gasPlumeDist = distances[0];
            minGasPlumeDist = distances[1];

            Scalar errorSat = 0.0;
            Scalar errorRelPerm = 0.0;
            it = mapAllColumns.lower_bound(i);
            for (; it != mapAllColumns.upper_bound(i); ++it)
            {
                const Element& element = it->second;
                const Scalar resSatW = problem_.spatialParams().materialLawParams(element).swr();
                const Scalar resSatN = problem_.spatialParams().materialLawParams(element).snr();
                const int globalIdxI = problem_.variables().index(element);
                CellData& cellData = problem_.variables().cellData(globalIdxI);

                auto father(element);
                while(father.level() > 0)
                    father = father.father();
                const Scalar localTransform = father.geometry().center()[dim-1] - problem_.aquiferHeight()/2.0;
                const GlobalPosition globalPos = element.geometry().center();
                const Scalar top = (globalPos[dim - 1] + deltaZ/2.0) - localTransform;
                const Scalar bottom = (globalPos[dim - 1] - deltaZ/2.0) - localTransform;

                const Scalar satW = cellData.saturation(wPhaseIdx);
                const Scalar krw = MaterialLaw::krw(problem_.spatialParams().materialLawParams(element), satW);

                if(veModel == 0)//calculate error for VE model
                {
                    if (top <= minGasPlumeDist)
                    {
                        errorSat += std::abs(deltaZ * (satW - 1.0));
                        errorRelPerm += std::abs(deltaZ * (krw - 1.0));
                    }
                    else if (bottom >= gasPlumeDist)
                    {
                        errorSat += std::abs(deltaZ * (satW - resSatW));
                        errorRelPerm += std::abs(deltaZ * (krw - 0.0));
                    }
                    else
                    {
                        Scalar lowerDelta = std::min(0.0, minGasPlumeDist - bottom);
                        Scalar middleDelta = std::min(top, gasPlumeDist) - std::max(bottom, minGasPlumeDist);
                        Scalar upperDelta = std::min(0.0, top - gasPlumeDist);
                        errorSat += std::abs(lowerDelta * (satW - 1.0))
                        + std::abs(middleDelta * (satW - (1.0-resSatN)))
                        + std::abs(upperDelta * (satW - resSatW));
                        errorRelPerm += std::abs(lowerDelta * (krw - 1.0))
                        + std::abs(middleDelta * (krw - 1.0))
                        + std::abs(upperDelta * (krw - 0.0));
                    }
                }
                if(veModel == 1)//calculate error for capillary fringe model
                {
                    if (top <= minGasPlumeDist)
                    {
                        errorSat += std::abs(deltaZ * (satW - 1.0));
                        errorRelPerm += std::abs(deltaZ * (krw - 1.0));
                    }
                    else if (bottom >= gasPlumeDist)
                    {
                        errorSat += calculateErrorSatIntegral(bottom, top, satW,
                                                              gasPlumeDist, minGasPlumeDist, columnRefPressures);
                        errorRelPerm += calculateErrorKrwIntegral(bottom, top, satW,
                                                                  gasPlumeDist, minGasPlumeDist, columnRefPressures);
                    }
                    else
                    {
                        Scalar lowerDelta = std::min(0.0, gasPlumeDist - bottom);
                        Scalar middleDelta = std::min(top, gasPlumeDist) - std::max(bottom, minGasPlumeDist);

                        errorSat += std::abs(lowerDelta * (satW - 1.0)) + std::abs(middleDelta * (satW - (1.0-resSatN)));
                        errorRelPerm += std::abs(lowerDelta * (krw - 1.0)) + std::abs(middleDelta * (krw - 1.0));

                        if(top > gasPlumeDist)
                        {
                            errorSat += calculateErrorSatIntegral(gasPlumeDist, top, satW,
                                                                  gasPlumeDist, minGasPlumeDist, columnRefPressures);
                            errorRelPerm += calculateErrorKrwIntegral(gasPlumeDist, top, satW,
                                                                      gasPlumeDist, minGasPlumeDist, columnRefPressures);
                        }
                    }
                }
                // set new minGasPlumeDist
                cellData.setMinGasPlumeDist(minGasPlumeDist);
            }
//            indicatorVector_[i] = errorSat/(aquiferHeight - gasPlumeDist);
            indicatorVector_[i] = errorRelPerm/(aquiferHeight - gasPlumeDist);
            if(errorRelPerm < 1e-8 || gasPlumeDist > aquiferHeight - 1e-8)
                indicatorVector_[i] = 0;
//             std::cout << " indicatorVector_: " << indicatorVector_[i]
//             << " errorRelPerm " << errorRelPerm
//             << " gasPlumeDist " << gasPlumeDist << " ";
        }
        Scalar absoluteError = getParam<Scalar>("GridAdapt.AbsoluteError");
        refineBound_ = (1.0 + refinetol_) * absoluteError;
        coarsenBound_= (1.0 - coarsentol_) * absoluteError;

        #if HAVE_MPI
        // communicate updated values
        using DataHandle = VectorCommDataHandleEqual<ElementMapper, ScalarSolutionType, 0/*elementCodim*/>;
        DataHandle dataHandle(problem_.elementMapper(), indicatorVector_);
        problem_.gridView().template communicate<DataHandle>(dataHandle,
                                                             Dune::InteriorBorder_All_Interface,
                                                             Dune::ForwardCommunication);

        using std::max;
        refineBound_ = problem_.gridView().comm().max(refineBound_);
        coarsenBound_ = problem_.gridView().comm().max(coarsenBound_);

        #endif
    }

    /*! \brief Calculates gasPlumeDist, distance of gas plume from bottom
     *
     * Stores minGasPlumeDist for all grid cells
     */
    const std::vector<Scalar> calculateGasPlumeDist(const Scalar totalGasConcentration,
                                       const PhaseVector columnRefPressures,
                                       Scalar minGasPlumeDist) const
    {
        const int veModel = getParam<int>("VE.VEModel");
        const Scalar aquiferHeight = problem_.aquiferHeight();
        const Scalar gravity = problem_.gravity().two_norm();

        const Scalar resSatW = problem_.spatialParams().materialLawParams(dummy_).swr();
        const Scalar resSatN = problem_.spatialParams().materialLawParams(dummy_).snr();
        const Scalar porosity = problem_.spatialParams().porosity(dummy_);
        const Scalar entryP = problem_.spatialParams().materialLawParams(dummy_).pe();
        const Scalar lambda = problem_.spatialParams().materialLawParams(dummy_).lambda();

        const FluidState& fluidStatePlume = problem_.pressureModel().fluidStatePlume(dummy_, columnRefPressures);
        const Scalar X_l_gComp = fluidStatePlume.massFraction(wPhaseIdx, nCompIdx);
        const Scalar X_g_gComp = fluidStatePlume.massFraction(nPhaseIdx, nCompIdx);
        const Scalar densityW = fluidStatePlume.density(wPhaseIdx);
        const Scalar densityN = fluidStatePlume.density(nPhaseIdx);

        if(minGasPlumeDist > aquiferHeight)
            minGasPlumeDist = aquiferHeight;

        Scalar gasPlumeDist = 0.0;

        if(totalGasConcentration < 1e-12)
            gasPlumeDist = aquiferHeight;
        else if (veModel == sharpInterface) //calculate gasPlumeDist for sharp interface ve model
        {
            const Scalar minGasPlumeIntegral = (aquiferHeight - minGasPlumeDist) *
            (porosity * (1-resSatW) * densityN * X_g_gComp + porosity * resSatW * densityW * X_l_gComp);
            //calculate gasPlumeDist for gasPlumeDist > minGasPlumeDist
            if (totalGasConcentration * aquiferHeight < minGasPlumeIntegral - 1e-6)
            {
                gasPlumeDist = (totalGasConcentration * aquiferHeight + porosity * densityN * X_g_gComp *
                (minGasPlumeDist * resSatN - aquiferHeight * (1-resSatW))
                + porosity * densityW * X_l_gComp * (minGasPlumeDist * (1-resSatN) - aquiferHeight * resSatW)) /
                (porosity * (1-resSatW-resSatN) * (densityW * X_l_gComp - densityN * X_g_gComp));
            }
            //calculate gasPlumeDist for gasPlumeDist < minGasPlumeDist and calculate new minGasPlumeDist
            else if (totalGasConcentration * aquiferHeight > minGasPlumeIntegral + 1e-6)
            {
                gasPlumeDist = aquiferHeight - (aquiferHeight * totalGasConcentration /
                ((porosity * (1-resSatW) * densityN * X_g_gComp) + (porosity * resSatW * densityW * X_l_gComp)));
                minGasPlumeDist = gasPlumeDist;
            }
            //calculate gasPlumeDist for gasPlumeDist = minGasPlumeDist
            else
            {
                gasPlumeDist = minGasPlumeDist;
            }
        }

        else if (veModel == capillaryFringe) //calculate gasPlumeDist for capillary fringe model
        {
            const Scalar minGasPlumeIntegral =
            porosity * densityN * X_g_gComp * (aquiferHeight - minGasPlumeDist)
            + porosity * (densityW * X_l_gComp - densityN * X_g_gComp) *
            (1.0 / (1.0 - lambda) * std::pow(((aquiferHeight - minGasPlumeDist) * (densityW - densityN) * gravity
            + entryP),(1.0 - lambda))
            * (1.0 - resSatW - resSatN) * std::pow(entryP, lambda) / ((densityW - densityN) * gravity)
            + resSatW * (aquiferHeight - minGasPlumeDist)
            - entryP * (1.0 - resSatW - resSatN) / ((1.0 - lambda) * (densityW - densityN) * gravity));

            const Scalar residualIntegral = (aquiferHeight - minGasPlumeDist) * porosity * (resSatN * densityN * X_g_gComp
            + (1 - resSatN) * densityW * X_l_gComp);

            Scalar residual = 1.0;
            // calculate gasPlumeDist for gasPlumeDist > minGasPlumeDist
            // check if gas concentration is high enough to sustain minGasPlumeDist
            if (totalGasConcentration * aquiferHeight < minGasPlumeIntegral - 1e-6
                && totalGasConcentration * aquiferHeight > residualIntegral)
            {
                //GasPlumeDist>0 (always in this case)
                gasPlumeDist = minGasPlumeDist; //XiStart
                //solve equation for
                int count = 0;
                for (;count < 100; count++)
                {
                    residual =
                    porosity * densityN * X_g_gComp * (aquiferHeight - gasPlumeDist)
                    + porosity * (densityW * X_l_gComp - densityN * X_g_gComp) *
                    (1.0 / (1.0 - lambda) * std::pow(((aquiferHeight - gasPlumeDist) * (densityW - densityN) * gravity
                    + entryP),(1.0 - lambda))
                    * (1.0 - resSatW - resSatN) * std::pow(entryP, lambda) / ((densityW - densityN) * gravity)
                    + resSatW * (aquiferHeight - gasPlumeDist)
                    - entryP * (1.0 - resSatW - resSatN) / ((1.0 - lambda) * (densityW - densityN) * gravity))
                    + (gasPlumeDist - minGasPlumeDist) * porosity *
                    (resSatN * densityN * X_g_gComp + (1 - resSatN) * densityW * X_l_gComp)
                    - totalGasConcentration * aquiferHeight;

                    if (fabs(residual) < 1e-10)
                    {
                        break;
                    }

                    const Scalar derivation =
                    - porosity * densityN * X_g_gComp
                    + porosity * (densityW * X_l_gComp - densityN * X_g_gComp) *
                    (std::pow(((aquiferHeight - gasPlumeDist) * (densityW - densityN) * gravity + entryP), -lambda)
                    * (resSatN + resSatW - 1.0) * std::pow(entryP, lambda) - resSatW)
                    + porosity * (resSatN * densityN * X_g_gComp + (1 - resSatN) * densityW * X_l_gComp);

                    gasPlumeDist = gasPlumeDist - residual / (derivation);

                    if (count == 99)
                        std::cout << std::setprecision(15) << "Iterating gasPlumeDist not successfull, count is at maximum, exit iteration. "
                        << "Iteration crashed in following routine: "
                        << "gasPlumeDist > minGasPlumeDist, with: "
                        << "totalGasConcentration = " << totalGasConcentration
                        << ", minGasPlumeIntegral = " << minGasPlumeIntegral
                        << ", residual = " << residual
                        << ", derivation = " << derivation
                        << ", minGasPlumeDist = " << minGasPlumeDist
                        << " and gasPlumeDist: " << gasPlumeDist << "."
                        << std::endl;
                }
            }
            // calculate gasPlumeDist for gasPlumeDist < minGasPlumeDist (or unphysical minGasPlumeDist)
            // and calculate new minGasPlumeDist
            else if (totalGasConcentration * aquiferHeight > minGasPlumeIntegral + 1e-6
                || (totalGasConcentration * aquiferHeight < minGasPlumeIntegral - 1e-6 &&
                totalGasConcentration * aquiferHeight < residualIntegral))
            {
                //check if GasPlumeDist>0, GasPlumeDist=0, GasPlumeDist<0
                const Scalar fullIntegral =
                porosity * densityN * X_g_gComp * aquiferHeight
                + porosity * (densityW * X_l_gComp - densityN * X_g_gComp) *
                (1.0 / (1.0 - lambda) * std::pow((aquiferHeight * (densityW - densityN) * gravity +
                entryP),(1.0 - lambda))
                * (1.0 - resSatW - resSatN) * std::pow(entryP, lambda) / ((densityW - densityN) * gravity)
                + resSatW * aquiferHeight
                - entryP * (1.0 - resSatW - resSatN) / ((1.0 - lambda) * (densityW - densityN) * gravity));
                //GasPlumeDist>0
                if (totalGasConcentration * aquiferHeight < fullIntegral - 1e-6)
                {
                    gasPlumeDist = minGasPlumeDist; //XiStart
                    //solve equation for
                    int count = 0;
                    for (; count < 100; count++)
                    {
                        residual =
                        porosity * densityN * X_g_gComp * (aquiferHeight - gasPlumeDist)
                        + porosity * (densityW * X_l_gComp - densityN * X_g_gComp) *
                        (1.0 / (1.0 - lambda) * std::pow(((aquiferHeight - gasPlumeDist) * (densityW - densityN) * gravity +
                        entryP),(1.0 - lambda))
                        * (1.0 - resSatW - resSatN) * std::pow(entryP, lambda) / ((densityW - densityN) * gravity)
                        + resSatW * (aquiferHeight - gasPlumeDist)
                        - entryP * (1.0 - resSatW - resSatN) / ((1.0 - lambda) * (densityW - densityN) * gravity))
                        - totalGasConcentration * aquiferHeight;

                        if (fabs(residual) < 1e-12)
                        {
                            break;
                        }

                        const Scalar derivation =
                        - porosity * densityN * X_g_gComp
                        + porosity * (densityW * X_l_gComp - densityN * X_g_gComp) *
                        (std::pow(((aquiferHeight - gasPlumeDist) * (densityW - densityN) * gravity + entryP), -lambda)
                        * (- 1.0 + resSatW + resSatN) * std::pow(entryP, lambda) - resSatW);

                        gasPlumeDist = gasPlumeDist - residual / (derivation);

                        if (count == 99)
                            std::cout << "Iterating gasPlumeDist not successfull, count is at maximum, exit iteration. "
                            << "Iteration crashed in following routine: "
                            << "gasPlumeDist < minGasPlumeDist, GasPlumeDist > 0, with: "
                            << "totalGasConcentration = " << totalGasConcentration
                            << ", minGasPlumeIntegral = " << minGasPlumeIntegral
                            << ", fullIntegral = " << fullIntegral
                            << ", residual = " << residual
                            << ", derivation = " << derivation
                            << ", minGasPlumeDist = " << minGasPlumeDist
                            << " and gasPlumeDist: " << gasPlumeDist << "."
                            << std::endl;
                    }
                }
                //GasPlumeDist<0
                else if (totalGasConcentration * aquiferHeight > fullIntegral + 1e-6)
                {
                    gasPlumeDist = 0.0; //XiStart
                    //solve equation
                    int count = 0;
                    for (; count < 100; count++)
                    {
                        residual =
                        porosity * densityN * X_g_gComp * aquiferHeight
                        + porosity * (densityW * X_l_gComp - densityN * X_g_gComp) *
                        (1.0 / (1.0 - lambda) * std::pow(((aquiferHeight - gasPlumeDist) * (densityW - densityN) * gravity + entryP), (1.0 - lambda))
                        * (1.0 - resSatW - resSatN)* std::pow(entryP, lambda) / ((densityW - densityN) * gravity)
                        + resSatW * aquiferHeight
                        - 1.0 / (1.0 - lambda) * std::pow((entryP - gasPlumeDist * (densityW - densityN)
                        * gravity),(1.0 - lambda)) * (1.0 - resSatW - resSatN) * std::pow(entryP, lambda)
                        / ((densityW - densityN) * gravity))
                        - totalGasConcentration * aquiferHeight;

                        if (fabs(residual) < 1e-10)
                        {
                            break;
                        }

                        const Scalar derivation =
                        porosity * (densityW * X_l_gComp - densityN * X_g_gComp) *
                        std::pow(((aquiferHeight - gasPlumeDist) * (densityW - densityN) * gravity + entryP), -lambda)
                        * (- 1.0 + resSatN + resSatW) * std::pow(entryP, lambda)
                        + std::pow((entryP - gasPlumeDist * (densityW - densityN) * gravity), -lambda)
                        * (1.0 - resSatN - resSatW) * std::pow(entryP, lambda);

                        gasPlumeDist = gasPlumeDist - residual / (derivation);

                        if (count == 99)
                            std::cout << "Iterating gasPlumeDist not successfull, count is at maximum, exit iteration. "
                            << "Iteration crashed in following routine: "
                            << "gasPlumeDist < minGasPlumeDist, GasPlumeDist < 0, with: "
                            << "totalGasConcentration = " << totalGasConcentration
                            << ", minGasPlumeIntegral = " << minGasPlumeIntegral
                            << ", fullIntegral = " << fullIntegral
                            << ", residual = " << residual
                            << ", derivation = " << derivation
                            << ", minGasPlumeDist = " << minGasPlumeDist
                            << " and gasPlumeDist: " << gasPlumeDist << "."
                            << std::endl;
                    }
                }
                //GasPlumeDist=0
                else
                {
                    gasPlumeDist = 0.0;
                }
                minGasPlumeDist = std::max(0.0, gasPlumeDist);
            }
            //calculate gasPlumeDist for gasPlumeDist = minGasPlumeDist
            else
            {
                gasPlumeDist = minGasPlumeDist;
            }
        }
        std::vector<Scalar> distances(2);
        distances[0] = gasPlumeDist;
        distances[1] = minGasPlumeDist;
        return distances;
    }


    /*! \brief Calculates integral of difference of wetting saturation over z
     *
     */
    const Scalar calculateErrorSatIntegral(const Scalar lowerBound,
                                           const Scalar upperBound,
                                           const Scalar satW,
                                           const Scalar gasPlumeDist,
                                           const Scalar minGasPlumeDist,
                                           const PhaseVector columnRefPressures) const
    {
        const int intervalNumber = 10;
        const Scalar deltaZ = (upperBound - lowerBound)/intervalNumber;

        Scalar satIntegral = 0.0;
        for(int count=0; count<intervalNumber; count++ )
        {
            satIntegral += std::abs((reconstSaturation(lowerBound + count*deltaZ, gasPlumeDist, minGasPlumeDist, columnRefPressures)
            + reconstSaturation(lowerBound + (count+1)*deltaZ, gasPlumeDist, minGasPlumeDist, columnRefPressures))/2.0 - satW);
        }
        satIntegral = satIntegral * deltaZ;

        return satIntegral;
    }

    /*! \brief Calculates integral of difference of relative wetting permeability over z
     *
     */
    const Scalar calculateErrorKrwIntegral(const Scalar lowerBound,
                                     const Scalar upperBound,
                                     const Scalar satW,
                                     const Scalar gasPlumeDist,
                                     const Scalar minGasPlumeDist,
                                     const PhaseVector columnRefPressures) const
    {
        const int intervalNumber = 10;
        const Scalar deltaZ = (upperBound - lowerBound)/intervalNumber;

        Scalar krwIntegral = 0.0;
        for(int count=0; count<intervalNumber; count++ )
        {
            Scalar sat1 = reconstSaturation(lowerBound + count*deltaZ, gasPlumeDist, minGasPlumeDist, columnRefPressures);
            Scalar sat2 = reconstSaturation(lowerBound + (count+1)*deltaZ, gasPlumeDist, minGasPlumeDist, columnRefPressures);
            Scalar krw1 = MaterialLaw::krw(problem_.spatialParams().materialLawParams(dummy_), sat1);
            Scalar krw2 = MaterialLaw::krw(problem_.spatialParams().materialLawParams(dummy_), sat2);
            Scalar krw = MaterialLaw::krw(problem_.spatialParams().materialLawParams(dummy_), satW);
            krwIntegral += std::abs((krw1 + krw2)/2.0 - krw);
        }
        krwIntegral = krwIntegral * deltaZ;

        return krwIntegral;
    }

    /*! \brief Calculates gasPlumeDist, distance of gas plume from bottom
     *
     * Stores minGasPlumeDist for all grid cells
     */
    const Scalar reconstSaturation(const Scalar height,
                                   const Scalar gasPlumeDist,
                                   const Scalar minGasPlumeDist,
                                   const PhaseVector columnRefPressures) const
    {
        const Scalar resSatW = problem_.spatialParams().materialLawParams(dummy_).swr();
        const Scalar resSatN = problem_.spatialParams().materialLawParams(dummy_).snr();
        const Scalar entryP = problem_.spatialParams().materialLawParams(dummy_).pe();
        const Scalar lambda = problem_.spatialParams().materialLawParams(dummy_).lambda();
        const int veModel = getParam<int>("VE.VEModel");

        const FluidState& fluidStatePlume = problem_.pressureModel().fluidStatePlume(dummy_, columnRefPressures);
        const Scalar densityW = fluidStatePlume.density(wPhaseIdx);
        const Scalar densityN = fluidStatePlume.density(nPhaseIdx);

        Scalar reconstSaturation = 0.0;
        if (veModel == sharpInterface) //reconstruct phase saturation for sharp interface ve model
        {
            if(height <= minGasPlumeDist)
            {
                reconstSaturation = 1.0;
            }
            else if(height > minGasPlumeDist && height <= gasPlumeDist)
            {
                reconstSaturation = 1.0 - resSatN;
            }
            else if(height > gasPlumeDist)
            {
                reconstSaturation = resSatW;
            }
        }
        else if (veModel == capillaryFringe) //reconstruct phase saturation for capillary fringe model
        {
            if(height <= minGasPlumeDist)
            {
                reconstSaturation = 1.0;
            }
            else if(height > minGasPlumeDist && height <= gasPlumeDist)
            {
                reconstSaturation = 1.0 - resSatN;
            }
            else if(height > gasPlumeDist)
            {
                reconstSaturation = std::pow(((height - gasPlumeDist) * (densityW - densityN)
                * problem_.gravity().two_norm() + entryP), (-lambda))
                * std::pow(entryP, lambda) * (1.0 - resSatW - resSatN) + resSatW;
            }
        }

        return reconstSaturation;
    }






//        /*! \brief Calculates the indicator used for refinement/coarsening for each column.
//         *
//         * This indicator is based on the velocity of the non-wetting phase. TODO: not working, implement like in test_dec2p
//         */
//        void calculateIndicator()
//        {
//            CellArray numberOfCells = GET_RUNTIME_PARAM_FROM_GROUP_CSTRING(TypeTag, CellArray, "Grid", Cells);
//            int numberOfColumns = numberOfCells[0];
//            // prepare an indicator for refinement
//            if(indicatorVector_.size() != numberOfColumns)
//            {
//                indicatorVector_.resize(numberOfColumns);
//            }
//            indicatorVector_ = 0.0;
//
//            Scalar velocityNwVerticalMaxTotal = 0.0;
//            std::multimap<int, Element> mapAllColumns = problem_.getColumnMap();
//            for(int i=0;i<numberOfColumns;i++)
//            {
//                Scalar velocityNwVerticalMax = 0.0;
//                typename std::map<int, Element>::iterator it = mapAllColumns.lower_bound(i);
//                for (; it != mapAllColumns.upper_bound(i); ++it)
//                {
//                    Element element = it->second;
//                    int eIdx = problem_.variables().index(element);
//                    for (const auto& intersection : intersections(problem_.gridView(), element))
//                    {
//                        GlobalPosition velocityNw = problem_.variables().cellData(eIdx).fluxData().velocity(nPhaseIdx, intersection.indexInInside());
//                        GlobalPosition gravityNormalized = problem_.gravity();
//                        gravityNormalized /= problem_.gravity().two_norm();
//                        velocityNwVerticalMax = std::max(velocityNwVerticalMax, -(velocityNw * gravityNormalized));
//                    }
//                }
//                indicatorVector_[i] = velocityNwVerticalMax;
//                velocityNwVerticalMaxTotal = std::max(velocityNwVerticalMaxTotal, velocityNwVerticalMax);
//            }
//            refineBound_ = refinetol_ * velocityNwVerticalMaxTotal;
//            coarsenBound_ = coarsentol_ * velocityNwVerticalMaxTotal;
//
//            #if HAVE_MPI
//            // communicate updated values
//            typedef VectorExchange<ElementMapper, ScalarSolutionType> DataHandle;
//            DataHandle dataHandle(problem_.elementMapper(), indicatorVector_);
//            problem_.gridView().template communicate<DataHandle>(dataHandle,
//                                                 Dune::InteriorBorder_All_Interface,
//                                                 Dune::ForwardCommunication);
//
//            refineBound_ = problem_.gridView().comm().max(refineBound_);
//            coarsenBound_ = problem_.gridView().comm().max(coarsenBound_);
//
//            #endif
//        }








//        /*! \brief Calculates the indicator used for refinement/coarsening for each column.
//         *
//         * This indicator is based on the saturation and the position.
//         */
//        void calculateIndicator()
//        {
//            CellArray numberOfCells = GET_RUNTIME_PARAM_FROM_GROUP_CSTRING(TypeTag, CellArray, "Grid", Cells);
//            int numberOfColumns = numberOfCells[0];
//            // prepare an indicator for refinement
//            if(indicatorVector_.size() != numberOfColumns)
//            {
//                indicatorVector_.resize(numberOfColumns);
//            }
//            indicatorVector_ = 0.0;
//            std::multimap<int, Element> mapAllColumns = problem_.getColumnMap();
//            for(int i=0;i<numberOfColumns;i++)
//            {
//                typename std::map<int, Element>::iterator it = mapAllColumns.lower_bound(i);
//                for (; it != mapAllColumns.upper_bound(i); ++it)
//                {
//                    int globalIdxI = problem_.variables().index(it->second);
//                    GlobalPosition globalPos = (it->second).geometry().center();
//                    Scalar satW = problem_.variables().cellData(globalIdxI).saturation(wPhaseIdx);
//                    //mark 2D column depending on indicator
//                    if(problem_.timeManager().time() < 5e4)
//                    {
//                        if(globalPos[0] < 20.0)
//                        {
//                            indicatorVector_[i] = 1.0;
//                            continue;
//                        }
//                        else if(satW < 0.98)
//                        {
//                            indicatorVector_[i] = 1.0;
//                            continue;
//                        }
//                    }
//                    else if(problem_.timeManager().time() > 5e4 && problem_.timeManager().time() < 7e4)
//                    {
//                        if(globalPos[0] < 40.0)
//                        {
//                            indicatorVector_[i] = 1.0;
//                            continue;
//                        }
//                    }
//                    else
//                    {
//                        if(globalPos[0] < 20.0)
//                        {
//                            indicatorVector_[i] = 1.0;
//                            continue;
//                        }
//                    }
//                }
//            }
//            refineBound_ = absoluteError;
//            coarsenBound_ = absoluteError;
//            #if HAVE_MPI
//            // communicate updated values
//            typedef VectorExchange<ElementMapper, ScalarSolutionType> DataHandle;
//            DataHandle dataHandle(problem_.elementMapper(), indicatorVector_);
//            problem_.gridView().template communicate<DataHandle>(dataHandle,
//                                     Dune::InteriorBorder_All_Interface,
//                                     Dune::ForwardCommunication);
//
//            refineBound_ = problem_.gridView().comm().max(refineBound_);
//            coarsenBound_ = problem_.gridView().comm().max(coarsenBound_);
//
//            #endif
//        }


    /*! \brief Indicator function for marking of grid cells for refinement
     *
     * Returns true if an element should be refined.
     *
     *  \param element A grid element
     */
    bool refine(int columnNumber)
    {
        return (indicatorVector_[columnNumber] > refineBound_);
    }

    /*! \brief Indicator function for marking of grid cells for coarsening
     *
     * Returns true if an element should be coarsened.
     *
     *  \param element A grid element
     */
    bool coarsen(int columnNumber)
    {
        return (indicatorVector_[columnNumber] < coarsenBound_);
    }

    /*! \brief Initializes the adaption indicator class*/
    void init()
    {
        refineBound_ = 0.;
        coarsenBound_ = 0.;
    };

    /*! @brief Constructs a GridAdaptionIndicator instance
     *
     *  This standard indicator is based on the saturation gradient.
     *  It checks the local gradient compared to the maximum global gradient.
     *  The indicator is compared locally to a refinement/coarsening threshold to decide whether
     *  a cell should be marked for refinement or coarsening or should not be adapted.
     *
     * \param problem The problem object
     */
    GridAdaptionIndicator2P2CVEFullD (Problem& problem) : GridAdaptionIndicator2PVEFullD<TypeTag>(problem), problem_(problem)
    {
        refinetol_ = getParam<Scalar>("GridAdapt.RefineTolerance");
        coarsentol_ = getParam<Scalar>("GridAdapt.CoarsenTolerance");
    }

protected:
    Problem& problem_;
    Scalar refinetol_;
    Scalar coarsentol_;
    Scalar refineBound_;
    Scalar coarsenBound_;
    ScalarSolutionType indicatorVector_;
    static const int saturationType_ = getPropValue<TypeTag, Properties::SaturationFormulation>();
    Element dummy_;
};
}

#endif
