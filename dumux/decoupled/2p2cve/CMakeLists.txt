
install(FILES
        celldata2p2cve.hh
        celldata2p2cvemultidim.hh
        fvpressure2p2cve.hh
        fvpressure2p2cvemultidim.hh
        fvtransport2p2cve.hh
        fvtransport2p2cvemultidim.hh
        gridadaptionindicator2p2cvefulld.hh
        variableclassadaptive2p2cvefulld.hh
        DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/dumux/decoupled/2p2cve)
