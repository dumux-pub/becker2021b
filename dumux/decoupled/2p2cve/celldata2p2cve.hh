// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup SequentialTwoPTwoCModel
 * \brief Storage container for discretized data of the constitutive relations for one element
 */

#ifndef DUMUX_ELEMENTDATA2P2CVE_HH
#define DUMUX_ELEMENTDATA2P2CVE_HH

#include <dumux/porousmediumflow/2p2c/sequential/celldata.hh>

namespace Dumux {
/*!
 * \ingroup SequentialTwoPTwoCModel
 * \brief Storage container for discretized data of the constitutive relations for one element
 *
 * This class stores all cell-centered (FV-Scheme) values for sequential compositional two-phase flow
 * models that are used by both pressure and transport model. All fluid data are already stored in the
 * fluidstate, so the CellData contains the fluidstate object for the current element.
 * At the moment, the compositional model does not use fluxVariables that are stored on the interfaces.
 *
 * \tparam TypeTag The Type Tag
 */
template<class TypeTag>
class CellData2P2CVE: public CellData2P2C<TypeTag>
{
private:
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using FluxData = FluxData2P2C<TypeTag>;
    using FluidState = GetPropType<TypeTag, Properties::FluidState>;
    using GridView = typename GetPropType<TypeTag, Properties::GridGeometry>::GridView;

    using Indices = GetPropType<TypeTag, Properties::Indices>;

    enum
    {
        wPhaseIdx = Indices::wPhaseIdx, nPhaseIdx = Indices::nPhaseIdx,
        wCompIdx = Indices::wPhaseIdx, nCompIdx = Indices::nPhaseIdx,
        contiWEqIdx = Indices::contiWEqIdx
    };
    enum
    {
        numPhases = getPropValue<TypeTag, Properties::NumPhases>(),
        numComponents = getPropValue<TypeTag, Properties::NumComponents>(),
        dim = GridView::dimension
    };

    int veModel_;
    Scalar minGasPlumeDist_;
    Scalar gasPlumeDist_;
    Scalar epsilonHeight_;
    Scalar densityLiquidInPlume_;
    Scalar densityLiquidBelowPlume_;
    Scalar mobilityLiquidBelowPlume_;
    Scalar mobilityInPlume_[numPhases];
    Scalar massFractionLiquidInPlume_[numComponents];
    Scalar massFractionLiquidBelowPlume_[numComponents];
    Scalar viscosityInPlume_[numPhases];// viscosity below plume is stored as viscosity
    bool isUpwindCellLiquidBelowPlume_[2*dim];
public:

    //! Constructor for a local CellData object
    CellData2P2CVE()
    {
        veModel_ = getParam<int>("VE.VEModel");
        minGasPlumeDist_ = 1e100;
        gasPlumeDist_ = 1e100;
        epsilonHeight_ = 0.0;

        for (int i = 0; i < numPhases; ++i)
        {
            mobilityInPlume_[i] = 0.0;
            viscosityInPlume_[i] = 0.0;
        }

        for (int i = 0; i < numComponents; ++i)
        {
            massFractionLiquidInPlume_[i] = 0.0;
            massFractionLiquidBelowPlume_[i] = 0.0;
        }

        for (int i = 0; i < 2*dim; ++i)
        {
            isUpwindCellLiquidBelowPlume_[i] = false;
        }

        mobilityLiquidBelowPlume_ = 0.0;
        densityLiquidInPlume_ = 0.0;
        densityLiquidBelowPlume_ = 0.0;
    }

    /*!\brief Returns veModel, the type of VE model or no VE model
     */
    const int veModel() const
    {
        return veModel_;
    }

    /*!\brief Sets veModel, the type of VE model or no VE model
     */
    void setVeModel(int index)
    {
        veModel_ = index;
    }

    /*!\brief Returns gasPlumeDist_, the height of plume distance from bottom
     */
    Scalar gasPlumeDist() const
    {
        return gasPlumeDist_;
    }

    /*!\brief Sets gasPlumeDist_, the height of plume distance from bottom
     *
     * \param Xi gasPlumeDist
     */
    void setGasPlumeDist(Scalar Xi)
    {
        gasPlumeDist_ = Xi;
    }

    /*!\brief Returns minGasPlumeDist_, the minimum height of plume distance from bottom
     */
    Scalar minGasPlumeDist() const
    {
        return minGasPlumeDist_;
    }

    /*!\brief Sets minGasPlumeDist_, the minimum height of plume distance from bottom
     *
     * \param Xi gasPlumeDist
     */
    void setMinGasPlumeDist(Scalar Xi)
    {
        minGasPlumeDist_ = Xi;
    }

    /*!\brief Returns epsilon height, the corrected bottom height due to volume mismatch in liquid phase
     */
    Scalar epsilonHeight() const
    {
        return epsilonHeight_;
    }

    /*!\brief Sets epsilon height, the corrected bottom height due to volume mismatch in liquid phase
     *
     * \param epsilon epsilon height
     */
    void setEpsilonHeight(Scalar epsilon)
    {
        epsilonHeight_ = epsilon;
    }

    /*!\brief Returns density of liquid phases within plume region
     */
    Scalar densityLiquidInPlume() const
    {
        return densityLiquidInPlume_;
    }

    /*!\brief Sets density of liquid phases within plume region
     *
     * \param phaseIdx index of the Phase
     * \param densityInPlume phase density in plume
     */
    void setDensityLiquidInPlume(Scalar densityLiquidInPlume)
    {
        densityLiquidInPlume_ = densityLiquidInPlume;
    }

    /*!\brief Returns densityLiquidBelowPlume, the phase density of the wetting phase below the plume
     */
    Scalar densityLiquidBelowPlume() const
    {
        return densityLiquidBelowPlume_;
    }

    /*!\brief Sets densityLiquidBelowPlume, the phase density of the wetting phase below the plume
     *
     * \param densityLiquidBelowPlume phase density in plume
     */
    void setDensityLiquidBelowPlume(Scalar densityLiquidBelowPlume)
    {
        densityLiquidBelowPlume_ = densityLiquidBelowPlume;
    }

    /*!
     * \brief Return phase mobilities averaged in plume
     * @param phaseIdx index of the Phase
     */
    Scalar mobilityInPlume(int phaseIdx) const
    {
        return mobilityInPlume_[phaseIdx];
    }

    /*!
     * \brief Set phase mobilities averaged in plume
     * \param phaseIdx index of the Phase
     * \param value Value to be stored
     */
    void setMobilityInPlume(int phaseIdx, Scalar value)
    {
        mobilityInPlume_[phaseIdx]=value;
    }

    /*!
     * \brief Return liquid phase mobility averaged below plume
     * @param phaseIdx index of the Phase
     */
    Scalar mobilityLiquidBelowPlume() const
    {
        return mobilityLiquidBelowPlume_;
    }

    /*!
     * \brief Set liquid phase mobility averaged below plume
     * \param phaseIdx index of the Phase
     * \param value Value to be stored
     */
    void setMobilityLiquidBelowPlume(Scalar value)
    {
        mobilityLiquidBelowPlume_ = value;
    }

    /*!
     * \brief Return mass fraction of a component  in a phase \f$\mathrm{[-]}\f$
     */
    Scalar massFractionLiquidInPlume(int compIdx) const
    {
        return massFractionLiquidInPlume_[compIdx];
    }

    /*!
     * \brief Set the mass fraction of a component  in a phase \f$\mathrm{[-]}\f$
     */
    void setMassFractionLiquidInPlume(int compIdx, Scalar value)
    {
        massFractionLiquidInPlume_[compIdx] = value;
    }

    /*!
     * \brief Return mass fraction of a component  in a phase \f$\mathrm{[-]}\f$
     */
    Scalar massFractionLiquidBelowPlume(int compIdx) const
    {
        return massFractionLiquidBelowPlume_[compIdx];
    }

    /*!
     * \brief Set the mass fraction of a component  in a phase \f$\mathrm{[-]}\f$
     */
    void setMassFractionLiquidBelowPlume(int compIdx, Scalar value)
    {
        massFractionLiquidBelowPlume_[compIdx] = value;
    }

    /*!
     * \brief Return phase viscosity in plume
     * @param phaseIdx index of the Phase
     */
    Scalar viscosityInPlume(int phaseIdx) const
    {
        return viscosityInPlume_[phaseIdx];
    }

    /*!
     * \brief Set phase viscosity in plume
     * \param phaseIdx index of the Phase
     * \param value Value to be stored
     */
    void setViscosityInPlume(int phaseIdx, Scalar value)
    {
        viscosityInPlume_[phaseIdx]=value;
    }

    /*!
     * \brief Indicates if current cell is the upwind cell below the plume for a given interface
     * \param indexInInside Local face index seen from current cell
     */
    const bool& isUpwindCellLiquidBelowPlume(int indexInInside) const
    {
        return isUpwindCellLiquidBelowPlume_[indexInInside];
    }
    /*!
     * \brief Specifies if current cell is the upwind cell below the plume for a given interface
     * \param indexInInside Local face index seen from current cell
     * \param value Value: true (->outflow) or false (-> inflow)
     */
    void setUpwindCellLiquidBelowPlume(int indexInInside, bool value)
    {
        isUpwindCellLiquidBelowPlume_[indexInInside] = value;
    }

    void setPressure(int phaseIdx, Scalar value)
    {
        this->manipulateFluidState().setPressure(phaseIdx, value);
    }
};
}
#endif
