// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup SequentialTwoPTwoCModel
 * \brief Finite volume 2p2c pressure model
 */
#ifndef DUMUX_FVPRESSURE2P2CVEMULTIDIM_HH
#define DUMUX_FVPRESSURE2P2CVEMULTIDIM_HH

// dumux environment
#include "fvpressure2p2cve.hh"
#include <dumux/material/fluidstates/pseudo1p2c.hh>

namespace Dumux {
/*!
 * \ingroup SequentialTwoPTwoCModel
 * \brief The finite volume model for the solution of the compositional pressure equation.
 *
 * Provides a Finite Volume implementation for the pressure equation of a compressible
 * system with two components. An IMPES-like method is used for the sequential
 * solution of the problem.  Diffusion is neglected, capillarity can be regarded.
 * Isothermal conditions and local thermodynamic
 * equilibrium are assumed.  Gravity is included.
 * \f[
         c_{total}\frac{\partial p}{\partial t} + \sum_{\kappa} \frac{\partial v_{total}}{\partial C^{\kappa}}
         \nabla \cdot \left( \sum_{\alpha} X^{\kappa}_{\alpha} \varrho_{\alpha} \bf{v}_{\alpha}\right)
          = \sum_{\kappa} \frac{\partial v_{total}}{\partial C^{\kappa}} q^{\kappa},
 *  \f]
 *  where \f$\bf{v}_{\alpha} = - \lambda_{\alpha} \bf{K} \left(\nabla p_{\alpha} + \rho_{\alpha} \bf{g} \right) \f$.
 *  \f$ c_{total} \f$ represents the total compressibility, for constant porosity this yields
 *  \f$ - \frac{\partial V_{total}}{\partial p_{\alpha}} \f$,
 *  \f$p_{\alpha} \f$ denotes the phase pressure, \f$ \bf{K} \f$ the absolute permeability,
 *  \f$ \lambda_{\alpha} \f$ the phase mobility,
 *  \f$ \rho_{\alpha} \f$ the phase density and \f$ \bf{g} \f$ the gravity constant and
 *  \f$ C^{\kappa} \f$ the total Component concentration.
 * See paper SPE 99619 or "Analysis of a Compositional Model for Fluid
 * Flow in Porous Media" by Chen, Qin and Ewing for derivation.
 *
 * The pressure base class FVPressure assembles the matrix and right-hand-side vector and solves for the pressure vector,
 * whereas this class provides the actual entries for the matrix and RHS vector.
 * The partial derivatives of the actual fluid volume \f$ v_{total} \f$ are gained by using a secant method.
 *
 */
template<class TypeTag> class FVPressure2P2CVEMultiDim
: public FVPressure2P2CVE<TypeTag>
{
    //the model implementation
    using Implementation = GetPropType<TypeTag, Properties::PressureModel>;
    using ParentType = FVPressure2P2CVE<TypeTag>;

    using GridView = typename GetPropType<TypeTag, Properties::GridGeometry>::GridView;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using Problem = GetPropType<TypeTag, Properties::Problem>;

    using SpatialParams = GetPropType<TypeTag, Properties::SpatialParams>;
    using MaterialLaw = GetPropType<TypeTag, Properties::MaterialLaw>;

    using Indices = GetPropType<TypeTag, Properties::Indices>;
    using BoundaryTypes = GetPropType<TypeTag, Properties::SequentialBoundaryTypes>;

    using FluidSystem = GetPropType<TypeTag, Properties::FluidSystem>;
    using FluidState = GetPropType<TypeTag, Properties::FluidState>;
    using FluidState1p2c = PseudoOnePTwoCFluidState<Scalar, FluidSystem>;

    using CellData = GetPropType<TypeTag, Properties::CellData>;
    using AdaptedValues = typename CellData::AdaptedValues;
    enum
    {
        dim = GridView::dimension, dimWorld = GridView::dimensionworld
    };
    enum
    {
        pw = Indices::pressureW,
        pn = Indices::pressureN,
        pGlobal = Indices::pressureGlobal
    };
    enum
    {
        wPhaseIdx = Indices::wPhaseIdx, nPhaseIdx = Indices::nPhaseIdx,
        wCompIdx = Indices::wPhaseIdx, nCompIdx = Indices::nPhaseIdx,
        contiWEqIdx = Indices::contiWEqIdx, contiNEqIdx = Indices::contiNEqIdx,
        numPhases = getPropValue<TypeTag, Properties::NumPhases>(),
        numComponents = getPropValue<TypeTag, Properties::NumComponents>()
    };
    enum VEModel
    {
        sharpInterface,
        capillaryFringe
    };


    /*!
     * \brief Indices of matrix and rhs entries
     * During the assembling of the global system of equations get-functions are called
     * (getSource(), getFlux(), etc.), which return global matrix or right hand side entries
     * in a vector. These can be accessed using following indices:
     */
    enum
    {
        rhs = 1,//!<index for the right hand side entry
        matrix = 0//!<index for the global matrix entry

    };

    // using declarations to abbreviate several dune classes...
    using Element = typename GridView::Traits::template Codim<0>::Entity;
    using Intersection = typename GridView::Intersection;

    // convenience shortcuts for Vectors/Matrices
    using GlobalPosition = Dune::FieldVector<Scalar, dimWorld>;
    using DimMatrix = Dune::FieldMatrix<Scalar, dim, dim>;
    using PhaseVector = Dune::FieldVector<Scalar, numPhases>;
    using ComponentVector = Dune::FieldVector<Scalar, numComponents>;
    using PrimaryVariables = GetPropType<TypeTag, Properties::PrimaryVariables>;

    // the typenames used for the stiffness matrix and solution vector
    using Matrix = GetPropType<TypeTag, Properties::PressureCoefficientMatrix>;
    using CellArray = std::array<unsigned int, dimWorld>;

protected:
    //! @copydoc FVPressure::EntryType
    using EntryType = Dune::FieldVector<Scalar, 2>;

    Problem& problem()
    {
        return problem_;
    }
    const Problem& problem() const
    {
        return problem_;
    }

public:
    //function which assembles the system of equations to be solved
    void assemble(bool first);

    //initializes the matrix to store the system of equations
    void initializeMatrix();

    void getFlux(EntryType& entries, const Intersection& intersection, const CellData& cellDataI, const bool first);

    //! Adapt primary variables vector after adapting the grid
    void adaptPressure()
    {
        int gridSize = problem().gridView().size(0);
        this->pressure().resize(gridSize);

        for(int i=0; i< gridSize; i++)
        {
            this->pressure()[i]
            = problem().variables().cellData(i).pressure(this->pressureType);
        }
    }

    /*!\brief Returns the reconstructed phase pressure, assuming gasPlumeDist is known
     *
     * \param phaseIdx Index of a fluid phase
     */
    using ParentType::reconstPressure;
    Scalar reconstPressure(const Scalar height,
                           const int phaseIdx,
                           const AdaptedValues& valuesCoarse,
                           const Element& elementCoarse) const
    {
        const int eIdxGlobal = problem_.variables().index(elementCoarse);
        const int veModel = valuesCoarse.veModel;
        const Scalar coarsePressureW = valuesCoarse.pressure[wPhaseIdx];
        const Scalar densityWBelowPlume = valuesCoarse.densityLiquidBelowPlume;
        const Scalar densityWInPlume = valuesCoarse.densityLiquidInPlume;
        const Scalar densityNInPlume = valuesCoarse.density[nPhaseIdx];
        const Scalar gasPlumeDist = valuesCoarse.gasPlumeDist;
        const Scalar minGasPlumeDist = valuesCoarse.minGasPlumeDist;
        const Scalar gravity = problem_.gravity().two_norm();

        Scalar reconstPressure[numPhases];
        if(veModel == sharpInterface)
        {
            if(height <= minGasPlumeDist)
            {
                reconstPressure[wPhaseIdx] = coarsePressureW - densityWBelowPlume * gravity * height;
                reconstPressure[nPhaseIdx] = reconstPressure[wPhaseIdx];
            }
            else if(height > minGasPlumeDist && height <= gasPlumeDist)
            {
                reconstPressure[wPhaseIdx] = coarsePressureW - densityWBelowPlume * gravity * minGasPlumeDist
                - densityWInPlume * gravity * (height - minGasPlumeDist);
                reconstPressure[nPhaseIdx] = reconstPressure[wPhaseIdx];
            }
            else if(height > gasPlumeDist)
            {
                reconstPressure[wPhaseIdx] = coarsePressureW - densityWBelowPlume * gravity * minGasPlumeDist
                - densityWInPlume * gravity * (gasPlumeDist - minGasPlumeDist) - densityNInPlume * gravity * (height - gasPlumeDist);
                reconstPressure[nPhaseIdx]= reconstPressure[wPhaseIdx];
            }
        }
        else if(veModel == capillaryFringe)
        {
            if(height <= minGasPlumeDist)
            {
                reconstPressure[wPhaseIdx] = coarsePressureW - densityWBelowPlume * gravity * height;
                reconstPressure[nPhaseIdx] = reconstPressure[wPhaseIdx] + entryP_;
            }
            else if(height > minGasPlumeDist && height <= gasPlumeDist)
            {
                reconstPressure[wPhaseIdx] = coarsePressureW - densityWBelowPlume * gravity * minGasPlumeDist
                - densityWInPlume * gravity * (height - minGasPlumeDist);
                reconstPressure[nPhaseIdx] = reconstPressure[wPhaseIdx] + entryP_;
            }
            else if(height > gasPlumeDist)
            {
                reconstPressure[wPhaseIdx] = coarsePressureW - densityWBelowPlume * gravity * minGasPlumeDist
                - densityWInPlume * gravity * (height - minGasPlumeDist);
                reconstPressure[nPhaseIdx] = coarsePressureW - densityWBelowPlume * gravity * minGasPlumeDist
                - densityWInPlume * gravity * (gasPlumeDist - minGasPlumeDist) + entryP_ - densityNInPlume * gravity * (height - gasPlumeDist);
            }
        }
        else
        {
            reconstPressure[wPhaseIdx] = coarsePressureW;//reconstruct phase pressures for no ve model
            reconstPressure[nPhaseIdx] = valuesCoarse.pressure[nPhaseIdx];
        }

        return reconstPressure[phaseIdx];
    }

    /*! \brief Calculates wetting saturation integral over height given two bounds and a container holding coarse cell information
     */
    using ParentType::saturationIntegral;
    Scalar saturationIntegral(const Scalar lowerBound,
                              const Scalar upperBound,
                              const AdaptedValues& valuesCoarse,
                              const Element& elementCoarse) const
    {
        const int veModel = valuesCoarse.veModel;
        const Scalar gasPlumeDist = valuesCoarse.gasPlumeDist;
        const Scalar minGasPlumeDist = valuesCoarse.minGasPlumeDist;
        const Scalar resSatW = problem_.spatialParams().materialLawParams(elementCoarse).swr();
        const Scalar resSatN = problem_.spatialParams().materialLawParams(elementCoarse).snr();

        //different integration areas:
        //1. from bottom to minGasPlumeDist, 2. from minGasPlumeDist to gasPlumeDist, 3. from gasPlumeDist to top
        Scalar integral = 0.0;

        if(lowerBound < minGasPlumeDist)
        {
            const Scalar upperPartBound = std::min(minGasPlumeDist, upperBound);
            integral += upperPartBound - lowerBound;
        }
        if(upperBound > minGasPlumeDist && lowerBound < gasPlumeDist )
        {
            const Scalar lowerpartBound = std::max(minGasPlumeDist, lowerBound);
            const Scalar upperPartBound = std::min(gasPlumeDist, upperBound);
            integral += (upperPartBound - lowerpartBound)*(1.0-resSatN);
        }
        if(upperBound > gasPlumeDist)
        {
            const Scalar lowerPartBound = std::max(gasPlumeDist, lowerBound);
            if(veModel == sharpInterface)
            {
                integral += (upperBound - lowerPartBound)*resSatW;
            }
            else if(veModel == capillaryFringe)
            {
                const Scalar densityW = valuesCoarse.densityLiquidInPlume;
                const Scalar densityN = valuesCoarse.density[nPhaseIdx];

                integral += (std::pow(entryP_, lambda_) * (1 - resSatW - resSatN)) /
                ((1 - lambda_) * (densityW - densityN) * problem_.gravity().two_norm()) *
                (std::pow(((upperBound - gasPlumeDist) * (densityW - densityN) *
                problem_.gravity().two_norm() + entryP_), (1 - lambda_))
                - std::pow(((lowerPartBound - gasPlumeDist) * (densityW - densityN) *
                problem_.gravity().two_norm() + entryP_), (1 - lambda_)))
                + resSatW * (upperBound - lowerPartBound);
            }
        }

        return integral;
    }

    /*! \brief Calculates concentration integral over height given two bounds and a container holding coarse cell information
     */
    using ParentType::concentrationIntegral;
    Dune::FieldVector<Scalar,numComponents> concentrationIntegral(const Scalar lowerBound,
                                                      const Scalar upperBound,
                                                      const AdaptedValues& valuesCoarse,
                                                      const Element& elementCoarse) const
    {
        Scalar minGasPlumeDist = valuesCoarse.minGasPlumeDist;

        //different integration areas:
        //1. from bottom to minGasPlumeDist, 2. from minGasPlumeDist to top
        Dune::FieldVector<Scalar,numComponents> integral(0);

        if(lowerBound < minGasPlumeDist + 1e-10) // we want to make sure this is accessed even if minGasPlumeDist is 0
        {
            const Scalar upperPartBound = std::min(minGasPlumeDist, upperBound);
            integral[wCompIdx] += valuesCoarse.densityLiquidBelowPlume * valuesCoarse.massFractionLiquidBelowPlume[wCompIdx]
                                  * (upperPartBound - lowerBound);
            integral[wCompIdx] -= valuesCoarse.densityLiquidBelowPlume * valuesCoarse.massFractionLiquidBelowPlume[wCompIdx]
                                  * valuesCoarse.epsilonHeight * (upperPartBound - lowerBound)/minGasPlumeDist;
                                  // add mass that is hidden in volume mismatch (only liquid phase below plume)
                                  // epsilonHeight is positive for negative volume mismatch
            integral[nCompIdx] += valuesCoarse.densityLiquidBelowPlume * valuesCoarse.massFractionLiquidBelowPlume[nCompIdx]
                                  * (upperPartBound - lowerBound);
            integral[nCompIdx] -= valuesCoarse.densityLiquidBelowPlume * valuesCoarse.massFractionLiquidBelowPlume[nCompIdx]
                                  * valuesCoarse.epsilonHeight * (upperPartBound - lowerBound)/minGasPlumeDist;
                                  // add mass that is hidden in volume mismatch (only liquid phase below plume)
                                  // epsilonHeight is positive for negative volume mismatch
        }
        if(upperBound > minGasPlumeDist + 1e-10)
        {
            const Scalar lowerpartBound = std::max(minGasPlumeDist, lowerBound);
            integral[wCompIdx] += valuesCoarse.density[nPhaseIdx] * valuesCoarse.massFractionGas[wCompIdx]
                                  * (upperBound - lowerpartBound);
            integral[wCompIdx] += (valuesCoarse.densityLiquidInPlume * valuesCoarse.massFractionLiquidInPlume[wCompIdx]
                                  - valuesCoarse.density[nPhaseIdx] * valuesCoarse.massFractionGas[wCompIdx])
                                  * saturationIntegral(lowerpartBound, upperBound, valuesCoarse, elementCoarse);
            integral[nCompIdx] += valuesCoarse.density[nPhaseIdx] * valuesCoarse.massFractionGas[nCompIdx]
                                  * (upperBound - lowerpartBound);
            integral[nCompIdx] += (valuesCoarse.densityLiquidInPlume * valuesCoarse.massFractionLiquidInPlume[nCompIdx]
                                  - valuesCoarse.density[nPhaseIdx] * valuesCoarse.massFractionGas[nCompIdx])
                                  * saturationIntegral(lowerpartBound, upperBound, valuesCoarse, elementCoarse);
        }

        const Scalar porosity = problem_.spatialParams().porosity(elementCoarse);
        integral *= porosity;

        return integral;
    }

    /*!
     * \brief Partial derivatives of the volumes w.r.t. changes in total concentration and pressure
     *
     * This method calculates the volume derivatives via a secant method, where the
     * secants are gained in a pre-computational step via the transport equation and
     * the last TS size.
     * The partial derivatives w.r.t. mass are defined as
     * \f$ \frac{\partial v}{\partial C^{\kappa}} = \frac{\partial V}{\partial m^{\kappa}}\f$
     *
     * \param globalPos The global position of the current element
     * \param element The current element
     */
    using ParentType::volumeDerivatives;
    const ComponentVector volumeDerivatives(const FluidState& ghostFluidState,
                                            const Element& element,
                                            ComponentVector mass) const
    {
        const int eIdxGlobal = problem_.variables().index(element);

        const CellData& cellData = problem_.variables().cellData(eIdxGlobal);

        // get cell temperature
        const Scalar temperature_ = cellData.temperature(wPhaseIdx);

        // initialize an Fluid state and a flash solver
        FluidState updFluidState;
        CompositionalFlash<Scalar, FluidSystem> flashSolver;

        /**********************************
         * a) get necessary variables
         **********************************/
        //determine phase pressures for flash calculation
        PhaseVector pressure(0.);
        for(int phaseIdx = 0; phaseIdx< numPhases; phaseIdx++)
            pressure[phaseIdx] = ghostFluidState.pressure(phaseIdx);

        // actual fluid volume
        // see Fritz 2011 (Dissertation) eq.3.76
        Scalar specificVolume(0.); // = \sum_{\alpha} \nu_{\alpha} / \rho_{\alpha}
        for(int phaseIdx = 0; phaseIdx< numPhases; phaseIdx++)
            specificVolume += ghostFluidState.phaseMassFraction(phaseIdx) / ghostFluidState.density(phaseIdx);
        Scalar volalt = mass.one_norm() * specificVolume;
        //    volalt = cellData.volumeError()+problem_.spatialParams().porosity(element);
        // = \sum_{\kappa} C^{\kappa} + \sum_{\alpha} \nu_{\alpha} / \rho_{\alpha}

        /**********************************
         * b) define increments
         **********************************/
        // increments for numerical derivatives
        ComponentVector massIncrement(0.);
        const CellArray numberOfCells = problem_.numberOfCells();
        const int reconstruction = getParam<int>("Grid.Reconstruction");
        const int numberOfCellsZ = numberOfCells[dim - 1]*std::pow(2, reconstruction);
        for(int compIdx = 0; compIdx< numComponents; compIdx++)
        {
            massIncrement[compIdx] = this->updateEstimate_[compIdx][eIdxGlobal]/numberOfCellsZ;// TODO: make more meaningful
            if(fabs(massIncrement[compIdx]) < 1e-8 * ghostFluidState.density(compIdx))
                massIncrement[compIdx] = 1e-8* ghostFluidState.density(compIdx);   // as phaseIdx = compIdx
        }

        /**********************************
         * c) Secant method for derivatives
         **********************************/

        // numerical derivative of fluid volume with respect to mass of components
        ComponentVector dv(0.);
        for (int compIdx = 0; compIdx<numComponents; compIdx++)
        {
            mass[compIdx] +=  massIncrement[compIdx];
            Scalar Z1 = mass[0] / mass.one_norm();
            flashSolver.concentrationFlash2p2c(updFluidState, Z1,
                                               pressure, problem_.spatialParams().porosity(element), temperature_);

            specificVolume=0.; // = \sum_{\alpha} \nu_{\alpha} / \rho_{\alpha}
            for(int phaseIdx = 0; phaseIdx< numPhases; phaseIdx++)
                specificVolume += updFluidState.phaseMassFraction(phaseIdx) / updFluidState.density(phaseIdx);

            dv[compIdx] = ((mass.one_norm() * specificVolume) - volalt) / massIncrement[compIdx];
            mass[compIdx] -= massIncrement[compIdx];

            //check routines if derivatives are meaningful
            using std::isnan;
            using std::isinf;
            if (isnan(dv[compIdx]) || isinf(dv[compIdx]) )
            {
                DUNE_THROW(Dune::MathError, "NAN/inf of dV_dm. If that happens in first timestep, try smaller firstDt!");
            }
        }

        return dv;
    }

    /*!
     * \brief Constructs a FVPressure2P2CVEMultiDim object
     * \param problem a problem class object
     */
    FVPressure2P2CVEMultiDim(Problem& problem) : FVPressure2P2CVE<TypeTag>(problem), problem_(problem), gravity_(problem.gravity())
    {
        ErrorTermFactor_ = getParam<Scalar>("Impet.ErrorTermFactor");
        ErrorTermLowerBound_ = getParam<Scalar>("Impet.ErrorTermLowerBound", 0.2);
        ErrorTermUpperBound_ = getParam<Scalar>("Impet.ErrorTermUpperBound");

        lambda_ = getParam<Scalar>("SpatialParams.Lambda");
        entryP_ = getParam<Scalar>("SpatialParams.EntryPressure");

        enableVolumeIntegral = getParam<bool>("Impet.EnableVolumeIntegral");
        regulateBoundaryPermeability = getPropValue<TypeTag, Properties::RegulateBoundaryPermeability>();
        if(regulateBoundaryPermeability)
        {
            minimalBoundaryPermeability = getParam<Scalar>("SpatialParams.MinBoundaryPermeability");
            Dune::dinfo << " Warning: Regulating Boundary Permeability requires correct subface indices on reference elements!"
                        << std::endl;
        }
    }

protected:
    Problem& problem_;
    const GlobalPosition& gravity_;
    bool enableVolumeIntegral; //!< Enables the volume integral of the pressure equation
    bool regulateBoundaryPermeability; //!< Enables regulation of permeability in the direction of a Dirichlet Boundary Condition
    Scalar minimalBoundaryPermeability; //!< Minimal limit for the boundary permeability
    Scalar ErrorTermFactor_; //!< Handling of error term: relaxation factor
    Scalar ErrorTermLowerBound_; //!< Handling of error term: lower bound for error dampening
    Scalar ErrorTermUpperBound_; //!< Handling of error term: upper bound for error dampening
    Scalar lambda_;
    Scalar entryP_;
    //! gives kind of pressure used (\f$ 0 = p_w \f$, \f$ 1 = p_n \f$, \f$ 2 = p_{global} \f$)
    static constexpr int pressureType = getPropValue<TypeTag, Properties::PressureFormulation>();
private:
    //! Returns the implementation of the problem (i.e. static polymorphism)
    Implementation &asImp_()
    {   return *static_cast<Implementation *>(this);}

    //! \copydoc IMPETProblem::asImp_()
    const Implementation &asImp_() const
    {   return *static_cast<const Implementation *>(this);}
};

//! function which assembles the system of equations to be solved
/*! This function assembles the Matrix and the RHS vectors to solve for
 * a pressure field with an Finite-Volume Discretization in an implicit
 * fashion. Compared to the method in FVPressure, this implementation
 * calculates fluxes near hanging nodes with the mpfa method using the method
 * getMpfaFlux(). Matrix and Right-hand-side entries are done therein.
 *
 * \param first Flag if pressure field is unknown
 */
template<class TypeTag>
void FVPressure2P2CVEMultiDim<TypeTag>::assemble(bool first)
{
    if(first)
    {
        ParentType::assemble(true);
        return;
    }

    initializeMatrix();
    // initialization: set matrix A_ to zero
    this->A_ = 0;
    this->f_ = 0;

    for (const auto& element : elements(problem().gridView()))
    {
        // get the global index of the cell
        int globalIdxI = problem().variables().index(element);

        // assemble interior element contributions
        if (element.partitionType() == Dune::InteriorEntity)
        {
            // get the cell data
            CellData& cellDataI = problem().variables().cellData(globalIdxI);

            Dune::FieldVector<Scalar, 2> entries(0.);

            /*****  source term ***********/
            problem().pressureModel().getSource(entries, element, cellDataI, first);
            this->f_[globalIdxI] += entries[rhs];

            /*****  flux term ***********/
            // iterate over all faces of the cell
            auto isEndIt = problem().gridView().iend(element);
            for (auto isIt = problem().gridView().ibegin(element); isIt != isEndIt; ++isIt)
            {
                const auto& intersection = *isIt;

                /************* handle interior face *****************/
                if (intersection.neighbor())
                {
                    auto elementNeighbor = intersection.outside();

                    int globalIdxJ = problem().variables().index(elementNeighbor);

                    //check for hanging nodes
                    //take a hanging node never from the element with smaller level
                    // calculate only from one side, but add matrix entries for both sides
                    // the last condition is needed to properly assemble in the presence
                    // of ghost elements
                    if (getPropValue<TypeTag, Properties::VisitFacesOnlyOnce>()
                        && (globalIdxI > globalIdxJ)
                        && elementNeighbor.partitionType() == Dune::InteriorEntity)
                        continue;

                    entries = 0;
                    //check for hanging nodes
                    problem().pressureModel().getFlux(entries, intersection, cellDataI, first);

                    //set right hand side
                    this->f_[globalIdxI] -= entries[rhs];

                    // set diagonal entry
                    this->A_[globalIdxI][globalIdxI] += entries[matrix];

                    // set off-diagonal entry
                    this->A_[globalIdxI][globalIdxJ] -= entries[matrix];

                    // The second condition is needed to not spoil the ghost element entries
                    if (getPropValue<TypeTag, Properties::VisitFacesOnlyOnce>()
                        && elementNeighbor.partitionType() == Dune::InteriorEntity)
                    {
                        this->f_[globalIdxJ] += entries[rhs];
                        this->A_[globalIdxJ][globalIdxJ] += entries[matrix];
                        this->A_[globalIdxJ][globalIdxI] -= entries[matrix];
                    }

                } // end neighbor

                /************* boundary face ************************/
                else
                {
                    entries = 0;
                    problem().pressureModel().getFluxOnBoundary(entries, intersection, cellDataI, first);

                    //set right hand side
                    this->f_[globalIdxI] += entries[rhs];
                    // set diagonal entry
                    this->A_[globalIdxI][globalIdxI] += entries[matrix];
                }
            } //end interfaces loop
            //            printmatrix(std::cout, this->A_, "global stiffness matrix", "row", 11, 3);

            /*****  storage term ***********/
            entries = 0;
            problem().pressureModel().getStorage(entries, element, cellDataI, first);
            this->f_[globalIdxI] += entries[rhs];
            // set diagonal entry
            this->A_[globalIdxI][globalIdxI] += entries[matrix];
        }
        // assemble overlap and ghost element contributions
        else
        {
            this->A_[globalIdxI] = 0.0;
            this->A_[globalIdxI][globalIdxI] = 1.0;
            this->f_[globalIdxI] = this->pressure()[globalIdxI];
        }

    } // end grid traversal
    //    printmatrix(std::cout, this->A_, "global stiffness matrix after assempling", "row", 11,3);
    //    printvector(std::cout, this->f_, "right hand side", "row", 10);
    return;
}

//! initializes the matrix to store the system of equations
/*! In comparison with the Tpfa method, an mpfa uses a larger flux stencil, hence more
 * matrix entries are required if not only the unique interaction region on the hanging
 * nodes are considered. The method checks weather the additonally regarded cells through
 * mpfa are already "normal" neighbors for fluxes through other interfaces, or if they
 * need to be added.
 */
template<class TypeTag>
void FVPressure2P2CVEMultiDim<TypeTag>::initializeMatrix()
{
    int gridSize_ = problem().gridView().size(0);
    // update RHS vector, matrix
    this->A_.setSize (gridSize_,gridSize_); //
    this->f_.resize(gridSize_);

    // determine matrix row sizes
    for (const auto& element : elements(problem().gridView()))
    {
        // cell index
        int globalIdxI = problem().variables().index(element);
        CellData& cellDataI = problem().variables().cellData(globalIdxI);

        // initialize row size
        int rowSize = 1;

        // set perimeter to zero
        cellDataI.perimeter() = 0;

        int numberOfIntersections = 0;
        // run through all intersections with neighbors
        const auto isEndIt = problem().gridView().iend(element);
        for (auto isIt = problem().gridView().ibegin(element); isIt != isEndIt; ++isIt)
        {
            const auto& intersection = *isIt;

            cellDataI.perimeter() += intersection.geometry().volume();
            numberOfIntersections++;
            if (intersection.neighbor())
            {
                rowSize++;
            }
        }

        cellDataI.fluxData().resize(numberOfIntersections);
        this->A_.setrowsize(globalIdxI, rowSize);
    }
    this->A_.endrowsizes();

    // determine position of matrix entries
    for (const auto& element : elements(problem().gridView()))
    {
        // cell index
        int globalIdxI = problem().variables().index(element);

        // add diagonal index
        this->A_.addindex(globalIdxI, globalIdxI);

        // run through all intersections with neighbors
        const auto isEndIt = problem().gridView().iend(element);
        for (auto isIt = problem().gridView().ibegin(element); isIt != isEndIt; ++isIt)
        {
            const auto& intersection = *isIt;

            if (intersection.neighbor())
            {
                // access neighbor
                int globalIdxJ = problem().variables().index(intersection.outside());

                // add off diagonal index
                this->A_.addindex(globalIdxI, globalIdxJ);
            }
        }
    }
    this->A_.endindices();

    return;
}

/*!
 * \brief Get flux at an interface between two cells
 *
 * for first == true, the flux is calculated in traditional fractional-flow forn as in FVPressure2P.
 * for first == false, the flux thorugh \f$ \gamma \f$  is calculated via a volume balance formulation
 *  \f[ - A_{\gamma} \mathbf{n}^T_{\gamma} \mathbf{K}  \sum_{\alpha} \varrho_{\alpha} \lambda_{\alpha}
 *    \mathbf{d}_{ij}  \left( \frac{p_{\alpha,j}^t - p^{t}_{\alpha,i}}{\Delta x} + \varrho_{\alpha} \mathbf{g}^T \mathbf{d}_{ij} \right)
 *               \sum_{\kappa} X^{\kappa}_{\alpha} \frac{\partial v_{t}}{\partial C^{\kappa}}
 *   + V_i \frac{A_{\gamma}}{U_i} \mathbf{d}^T \mathbf{K} \sum_{\alpha} \varrho_{\alpha} \lambda_{\alpha}
 *    \mathbf{d}_{ij}  \left( \frac{p_{\alpha,j}^t - p^{t}_{\alpha,i}}{\Delta x} + \varrho_{\alpha} \mathbf{g}^T \mathbf{d}_{ij} \right)
 *         \sum_{\kappa} X^{\kappa}_{\alpha}
 *         \frac{\frac{\partial v_{t,j}}{\partial C^{\kappa}_j}-\frac{\partial v_{t,i}}{\partial C^{\kappa}_i}}{\Delta x} \f]
 * This includes a boundary integral and a volume integral, because
 *  \f$ \frac{\partial v_{t,i}}{\partial C^{\kappa}_i} \f$ is not constant.
 * Here, \f$ \mathbf{d}_{ij} \f$ is the normalized vector connecting the cell centers, and \f$ \mathbf{n}_{\gamma} \f$
 * represents the normal of the face \f$ \gamma \f$.
 * \param entries The Matrix and RHS entries
 * \param intersection Intersection between cell I and J
 * \param cellDataI Data of cell I
 * \param first Flag if pressure field is unknown
 */
template<class TypeTag>
void FVPressure2P2CVEMultiDim<TypeTag>::getFlux(Dune::FieldVector<Scalar, 2>& entries,
                                      const Intersection& intersection,
                                      const CellData& cellDataI,
                                      const bool first)
{
    // access neighbor
    auto neighbor = intersection.outside();
    int eIdxGlobalJ = problem().variables().index(neighbor);
    CellData& cellDataJ = problem().variables().cellData(eIdxGlobalJ);

    const int veModelI = cellDataI.veModel();
    const int veModelJ = cellDataJ.veModel();

    if(veModelI != veModelJ)
    {
        entries = 0.;
        auto elementI = intersection.inside();
        int eIdxGlobalI = problem().variables().index(elementI);

        // get global coordinate of cell center
        GlobalPosition globalPos = elementI.geometry().center();
        GlobalPosition globalPosNeighbor = neighbor.geometry().center();

        // adjust for ghost cell and domain transformation.
        Scalar localTransformVE;
        if(veModelI < 2)
        {
            auto father(neighbor);
            while(father.level() > 0)
                father = father.father();
            const Scalar localTransform = father.geometry().center()[dim-1] - problem_.aquiferHeight()/2.0;
            localTransformVE = globalPos[dim-1] - problem_.aquiferHeight()/2.0;
            globalPos[dim-1] = (globalPos[dim-1] - problem_.aquiferHeight()/2.0) + (globalPosNeighbor[dim-1] - localTransform);
        }
        else if(veModelJ < 2)
        {
            auto father(elementI);
            while(father.level() > 0)
                father = father.father();
            const Scalar localTransform = father.geometry().center()[dim-1] - problem_.aquiferHeight()/2.0;
            localTransformVE = globalPosNeighbor[dim-1] - problem_.aquiferHeight()/2.0;
            globalPosNeighbor[dim-1] = (globalPosNeighbor[dim-1] - problem_.aquiferHeight()/2.0) + (globalPos[dim-1] - localTransform);
        }

        // boundaries of ghost cell
        const CellArray numberOfCells = problem_.numberOfCells();
        const int reconstruction = getParam<int>("Grid.Reconstruction");
        const double deltaZ = problem_.aquiferHeight()/(numberOfCells[dim - 1]*std::pow(2, reconstruction));

        Scalar top;
        Scalar bottom;
        Scalar positionZ;
        if(veModelI < 2)
        {
            top = (globalPos[dim - 1] + deltaZ/2.0) - localTransformVE;
            bottom = (globalPos[dim - 1] - deltaZ/2.0) - localTransformVE;
            positionZ = globalPos[dim - 1] - localTransformVE;
        }
        else if(veModelJ < 2)
        {
            top = (globalPosNeighbor[dim - 1] + deltaZ/2.0) - localTransformVE;
            bottom = (globalPosNeighbor[dim - 1] - deltaZ/2.0) - localTransformVE;
            positionZ = globalPosNeighbor[dim - 1] - localTransformVE;
        }

        // get phase pressures
        PhaseVector pressureI;
        pressureI[wPhaseIdx] = cellDataI.pressure(wPhaseIdx);
        pressureI[nPhaseIdx] = cellDataI.pressure(nPhaseIdx);
        PhaseVector pressureJ;
        pressureJ[wPhaseIdx] = cellDataJ.pressure(wPhaseIdx);
        pressureJ[nPhaseIdx] = cellDataJ.pressure(nPhaseIdx);

        // adjust for ghost cell
        if(veModelI < 2)
        {
            pressureI[wPhaseIdx] = problem_.pressureModel().reconstPressure(positionZ, wPhaseIdx, elementI);
            pressureI[nPhaseIdx] = problem_.pressureModel().reconstPressure(positionZ, nPhaseIdx, elementI);
        }
        else if(veModelJ < 2)
        {
            pressureJ[wPhaseIdx] = problem_.pressureModel().reconstPressure(positionZ, wPhaseIdx, neighbor);
            pressureJ[nPhaseIdx] = problem_.pressureModel().reconstPressure(positionZ, nPhaseIdx, neighbor);
        }

        // fluidstate in ghost cell
        FluidState ghostFluidState;
        CompositionalFlash<Scalar, FluidSystem> flashSolver;
        ComponentVector mass(0.);
        for(int compIdx = 0; compIdx< numComponents; compIdx++)
        {
            if(veModelI < 2)
            {
                mass[compIdx] = problem_.pressureModel().concentrationIntegral(bottom, top, elementI)[compIdx]/(top-bottom);
            }
            else if(veModelJ < 2)
            {
                mass[compIdx] = problem_.pressureModel().concentrationIntegral(bottom, top, neighbor)[compIdx]/(top-bottom);
            }
        }
        Scalar Z1 = mass[0] / mass.one_norm();
        if(veModelI < 2)
        {
            flashSolver.concentrationFlash2p2c(ghostFluidState, Z1, pressureI,
                                               problem_.spatialParams().porosity(elementI), cellDataI.temperature(wPhaseIdx));
        }
        else if(veModelJ < 2)
        {
            flashSolver.concentrationFlash2p2c(ghostFluidState, Z1, pressureJ,
                                               problem_.spatialParams().porosity(neighbor), cellDataJ.temperature(wPhaseIdx));
        }

        // cell volume & perimeter, assume linear map here
        Scalar volume = elementI.geometry().volume();
        Scalar perimeter = cellDataI.perimeter();
        if(veModelI < 2)
        {
            volume = neighbor.geometry().volume();
            perimeter = cellDataJ.perimeter();
        }

        const GlobalPosition& gravity_ = problem().gravity();

        // get absolute permeability
        DimMatrix permeabilityI(problem().spatialParams().intrinsicPermeability(elementI));

        // get mobilities and fractional flow factors
        PhaseVector mobilityI;
        mobilityI[wPhaseIdx] = cellDataI.mobility(wPhaseIdx);
        mobilityI[nPhaseIdx] = cellDataI.mobility(nPhaseIdx);
        PhaseVector mobilityJ;
        mobilityJ[wPhaseIdx] = cellDataJ.mobility(wPhaseIdx);
        mobilityJ[nPhaseIdx] = cellDataJ.mobility(nPhaseIdx);

        // adjust for ghost cell
        if(veModelI < 2)
        {
            mobilityI[wPhaseIdx] = problem_.pressureModel().relPermeabilityIntegral(bottom, top, elementI, wPhaseIdx)
            / (top-bottom) / ghostFluidState.viscosity(wPhaseIdx);
            mobilityI[nPhaseIdx] = problem_.pressureModel().relPermeabilityIntegral(bottom, top, elementI, nPhaseIdx)
            / (top-bottom) / ghostFluidState.viscosity(nPhaseIdx);
        }
        else if(veModelJ < 2)
        {
            mobilityJ[wPhaseIdx] = problem_.pressureModel().relPermeabilityIntegral(bottom, top, neighbor, wPhaseIdx)
            / (top-bottom) / ghostFluidState.viscosity(wPhaseIdx);
            mobilityJ[nPhaseIdx] = problem_.pressureModel().relPermeabilityIntegral(bottom, top, neighbor, nPhaseIdx)
            / (top-bottom) / ghostFluidState.viscosity(nPhaseIdx);
        }

        Scalar fractionalWI=0, fractionalNWI=0;
        if (first)
        {
            fractionalWI = mobilityI[wPhaseIdx]
            / (mobilityI[wPhaseIdx]+ mobilityI[nPhaseIdx]);
            fractionalNWI = mobilityI[nPhaseIdx]
            / (mobilityI[wPhaseIdx]+ mobilityI[nPhaseIdx]);
        }

        // get normal vector
        const GlobalPosition& unitOuterNormal = intersection.centerUnitOuterNormal();

        // get face volume
        Scalar faceArea = intersection.geometry().volume();

        // distance vector between barycenters
        GlobalPosition distVec = globalPosNeighbor - globalPos;

        // compute distance between cell centers
        Scalar dist = distVec.two_norm();

        GlobalPosition unitDistVec(distVec);
        unitDistVec /= dist;

        DimMatrix permeabilityJ
        = problem().spatialParams().intrinsicPermeability(neighbor);

        // compute vectorized permeabilities
        DimMatrix meanPermeability(0);
        harmonicMeanMatrix(meanPermeability, permeabilityI, permeabilityJ);

        Dune::FieldVector<Scalar, dim> permeability(0);
        meanPermeability.mv(unitDistVec, permeability);

        PhaseVector densityI;
        densityI[wPhaseIdx] = cellDataI.density(wPhaseIdx);
        densityI[nPhaseIdx] = cellDataI.density(nPhaseIdx);
        PhaseVector densityJ;
        densityJ[wPhaseIdx] = cellDataJ.density(wPhaseIdx);
        densityJ[nPhaseIdx] = cellDataJ.density(nPhaseIdx);

        if(veModelI < 2)
        {
            densityI[wPhaseIdx] = ghostFluidState.density(wPhaseIdx);
            densityI[nPhaseIdx] = ghostFluidState.density(nPhaseIdx);
        }
        else if(veModelJ < 2)
        {
            densityJ[wPhaseIdx] = ghostFluidState.density(wPhaseIdx);
            densityJ[nPhaseIdx] = ghostFluidState.density(nPhaseIdx);
        }

        // get average density for gravity flux
        Scalar rhoMeanW = 0.5 * (densityI[wPhaseIdx] + densityJ[wPhaseIdx]);
        Scalar rhoMeanNW = 0.5 * (densityI[nPhaseIdx] + densityJ[nPhaseIdx]);

        // reset potential gradients
        Scalar potentialW = 0;
        Scalar potentialNW = 0;

        if (first)     // if we are at the very first iteration we can't calculate phase potentials
        {
            // get fractional flow factors in neigbor
            Scalar fractionalWJ = mobilityJ[wPhaseIdx]
            / (mobilityJ[wPhaseIdx] + mobilityJ[nPhaseIdx]);
            Scalar fractionalNWJ = mobilityJ[nPhaseIdx]
            / (mobilityJ[wPhaseIdx] + mobilityJ[nPhaseIdx]);

            // perform central weighting
            Scalar lambda = (mobilityI[wPhaseIdx] + mobilityJ[wPhaseIdx]) * 0.5
            + (mobilityI[nPhaseIdx] + mobilityJ[nPhaseIdx]) * 0.5;

            entries[0] = fabs(lambda*faceArea*fabs(permeability*unitOuterNormal)/(dist));

            Scalar factor = (fractionalWI + fractionalWJ) * (rhoMeanW) * 0.5
            + (fractionalNWI + fractionalNWJ) * (rhoMeanNW) * 0.5;
            entries[1] = factor * lambda * faceArea * fabs(unitOuterNormal*permeability)
            * (gravity_ * unitDistVec);
        }
        else
        {
            // determine volume derivatives
            if (!cellDataJ.hasVolumeDerivatives())
                asImp_().volumeDerivatives(globalPosNeighbor, neighbor);

            PhaseVector dvI(0.);
            dvI[wPhaseIdx] = cellDataI.dv(wCompIdx);
            dvI[nPhaseIdx] = cellDataI.dv(nCompIdx);
            PhaseVector dvJ(0.);
            dvJ[wPhaseIdx] = cellDataJ.dv(wCompIdx);
            dvJ[nPhaseIdx] = cellDataJ.dv(nCompIdx);

            // adjust for ghost cell
            if(veModelI < 2)
            {
                dvI = volumeDerivatives(ghostFluidState, elementI, mass);
            }
            else if(veModelJ < 2)
            {
                dvJ = volumeDerivatives(ghostFluidState, neighbor, mass);
            }

            Scalar dv_dC1 = (dvJ[wCompIdx]
            + dvI[wCompIdx]) / 2.; // dV/dm1= dv/dC^1
            Scalar dv_dC2 = (dvJ[nCompIdx]
            + dvI[nCompIdx]) / 2.;

            Scalar graddv_dC1 = (dvJ[wCompIdx]
            - dvI[wCompIdx]) / dist;
            Scalar graddv_dC2 = (dvJ[nCompIdx]
            - dvI[nCompIdx]) / dist;

            // get mass fractions
            ComponentVector massFractionIW, massFractionIN;
            massFractionIW[wCompIdx] = cellDataI.massFraction(wPhaseIdx, wCompIdx);
            massFractionIW[nCompIdx] = cellDataI.massFraction(wPhaseIdx, nCompIdx);
            massFractionIN[wCompIdx] = cellDataI.massFraction(nPhaseIdx, wCompIdx);
            massFractionIN[nCompIdx] = cellDataI.massFraction(nPhaseIdx, nCompIdx);
            ComponentVector massFractionJW, massFractionJN;
            massFractionJW[wCompIdx] = cellDataJ.massFraction(wPhaseIdx, wCompIdx);
            massFractionJW[nCompIdx] = cellDataJ.massFraction(wPhaseIdx, nCompIdx);
            massFractionJN[wCompIdx] = cellDataJ.massFraction(nPhaseIdx, wCompIdx);
            massFractionJN[nCompIdx] = cellDataJ.massFraction(nPhaseIdx, nCompIdx);

            if(veModelI < 2)
            {
                massFractionIW[wCompIdx] = ghostFluidState.massFraction(wPhaseIdx, wCompIdx);
                massFractionIW[nCompIdx] = ghostFluidState.massFraction(wPhaseIdx, nCompIdx);
                massFractionIN[wCompIdx] = ghostFluidState.massFraction(nPhaseIdx, wCompIdx);
                massFractionIN[nCompIdx] = ghostFluidState.massFraction(nPhaseIdx, nCompIdx);
            }
            else if(veModelJ < 2)
            {
                massFractionJW[wCompIdx] = ghostFluidState.massFraction(wPhaseIdx, wCompIdx);
                massFractionJW[nCompIdx] = ghostFluidState.massFraction(wPhaseIdx, nCompIdx);
                massFractionJN[wCompIdx] = ghostFluidState.massFraction(nPhaseIdx, wCompIdx);
                massFractionJN[nCompIdx] = ghostFluidState.massFraction(nPhaseIdx, nCompIdx);
            }

            Scalar densityW = rhoMeanW;
            Scalar densityNW = rhoMeanNW;

            potentialW = (pressureI[wPhaseIdx] - pressureJ[wPhaseIdx])/dist;
            potentialNW = (pressureI[nPhaseIdx] - pressureJ[nPhaseIdx])/dist;

            potentialW += densityW * (unitDistVec * gravity_);
            potentialNW += densityNW * (unitDistVec * gravity_);

            // initialize convenience shortcuts
            Scalar lambdaW(0.), lambdaN(0.);
            Scalar dV_w(0.), dV_n(0.);        // dV_a = \sum_k \rho_a * dv/dC^k * X^k_a
            Scalar gV_w(0.), gV_n(0.);        // multipaper eq(3.3) line 3 analogon dV_w

            //do the upwinding of the mobility depending on the phase potentials
            ComponentVector massFractionWUpwind(0.);
            Scalar densityWUpwind(0.), mobilityWUpwind(0.);
            bool upwindW = false;
            if (potentialW > 0.)
            {
                massFractionWUpwind = massFractionIW;
                densityWUpwind = densityI[wPhaseIdx];
                mobilityWUpwind = mobilityI[wPhaseIdx];
                upwindW = true;
            }
            else if (potentialW < 0.)
            {
                massFractionWUpwind = massFractionJW;
                densityWUpwind = densityJ[wPhaseIdx];
                mobilityWUpwind = mobilityJ[wPhaseIdx];
                upwindW = true;
            }
            //else upwinding is not done because upwind direction cannot be stored for ghost cells!

            ComponentVector massFractionNUpwind(0.);
            Scalar densityNUpwind(0.), mobilityNUpwind(0.);
            bool upwindN = false;
            if (potentialNW > 0.)
            {
                massFractionNUpwind = massFractionIN;
                densityNUpwind = densityI[nPhaseIdx];
                mobilityNUpwind = mobilityI[nPhaseIdx];
                upwindN = true;
            }
            else if (potentialNW < 0.)
            {
                massFractionNUpwind = massFractionJN;
                densityNUpwind = densityJ[nPhaseIdx];
                mobilityNUpwind = mobilityJ[nPhaseIdx];
                upwindN = true;
            }
            //else upwinding is not done because upwind direction cannot be stored for ghost cells!

            //perform upwinding if desired
            if(!upwindW or (cellDataI.wasRefined() && cellDataJ.wasRefined() && elementI.father() == neighbor.father()))
            {
                if (cellDataI.wasRefined() && cellDataJ.wasRefined())
                {
                    problem().variables().cellData(eIdxGlobalI).setUpwindCell(intersection.indexInInside(), contiWEqIdx, false);
                    cellDataJ.setUpwindCell(intersection.indexInOutside(), contiWEqIdx, false);
                }

                Scalar averagedMassFraction[2];
                averagedMassFraction[wCompIdx]
                = harmonicMean(massFractionIW[wCompIdx], massFractionJW[wCompIdx]);
                averagedMassFraction[nCompIdx]
                = harmonicMean(massFractionIW[nCompIdx], massFractionJW[nCompIdx]);
                Scalar averageDensity = harmonicMean(densityI[wPhaseIdx], densityJ[wPhaseIdx]);

                //compute means
                dV_w = dv_dC1 * averagedMassFraction[wCompIdx] + dv_dC2 * averagedMassFraction[nCompIdx];
                dV_w *= averageDensity;
                gV_w = graddv_dC1 * averagedMassFraction[wCompIdx] + graddv_dC2 * averagedMassFraction[nCompIdx];
                gV_w *= averageDensity;
                lambdaW = harmonicMean(mobilityI[wPhaseIdx], mobilityJ[wPhaseIdx]);
            }
            else //perform upwinding
            {
                dV_w = (dv_dC1 * massFractionWUpwind[wCompIdx]
                + dv_dC2 * massFractionWUpwind[nCompIdx]);
                lambdaW = mobilityWUpwind;
                gV_w = (graddv_dC1 * massFractionWUpwind[wCompIdx]
                + graddv_dC2 * massFractionWUpwind[nCompIdx]);
                dV_w *= densityWUpwind;
                gV_w *= densityWUpwind;
            }

            if(!upwindN or (cellDataI.wasRefined() && cellDataJ.wasRefined()))
            {
                if (cellDataI.wasRefined() && cellDataJ.wasRefined())
                {
                    problem().variables().cellData(eIdxGlobalI).setUpwindCell(intersection.indexInInside(), contiNEqIdx, false);
                    cellDataJ.setUpwindCell(intersection.indexInOutside(), contiNEqIdx, false);
                }
                Scalar averagedMassFraction[2];
                averagedMassFraction[wCompIdx]
                = harmonicMean(massFractionIN[wCompIdx], massFractionJN[wCompIdx]);
                averagedMassFraction[nCompIdx]
                = harmonicMean(massFractionIN[nCompIdx], massFractionJN[nCompIdx]);
                Scalar averageDensity = harmonicMean(densityI[nPhaseIdx], densityJ[nPhaseIdx]);

                //compute means
                dV_n = dv_dC1 * averagedMassFraction[wCompIdx] + dv_dC2 * averagedMassFraction[nCompIdx];
                dV_n *= averageDensity;
                gV_n = graddv_dC1 * averagedMassFraction[wCompIdx] + graddv_dC2 * averagedMassFraction[nCompIdx];
                gV_n *= averageDensity;
                lambdaN = harmonicMean(mobilityI[nPhaseIdx], mobilityJ[nPhaseIdx]);
            }
            else
            {
                dV_n = (dv_dC1 *  massFractionNUpwind[wCompIdx]
                + dv_dC2 * massFractionNUpwind[nCompIdx]);
                lambdaN = mobilityNUpwind;
                gV_n = (graddv_dC1 * massFractionNUpwind[wCompIdx]
                + graddv_dC2 * massFractionNUpwind[nCompIdx]);
                dV_n *= densityNUpwind;
                gV_n *= densityNUpwind;
            }

            //calculate current matrix entry
            entries[matrix] = faceArea * (lambdaW * dV_w + lambdaN * dV_n)
            * fabs((unitOuterNormal*permeability)/(dist));
            if(enableVolumeIntegral)
                entries[matrix] -= volume * faceArea / perimeter * (lambdaW * gV_w + lambdaN * gV_n)
                * ((unitDistVec*permeability)/(dist));     // = boundary integral - area integral

            //calculate right hand side
            entries[rhs] = faceArea   * (densityW * lambdaW * dV_w + densityNW * lambdaN * dV_n);
            entries[rhs] *= fabs(unitOuterNormal * permeability);
            if(enableVolumeIntegral)
                entries[rhs] -= volume * faceArea / perimeter * (densityW * lambdaW * gV_w + densityNW * lambdaN * gV_n)
                * (unitDistVec * permeability);
            entries[rhs] *= (gravity_ * unitDistVec);

            //add rhs-term for VE-cell at interface: unknown pressure has to be written as reconstructed pressure in terms of height
            if(veModelI < 2 && positionZ <= cellDataI.minGasPlumeDist())
            {
                entries[rhs] -= faceArea * (lambdaW * dV_w + lambdaN * dV_n) * fabs((unitOuterNormal*permeability)/(dist))
                * cellDataI.densityLiquidBelowPlume() * gravity_.two_norm() * positionZ;
            }
            else if(veModelI == 0 && positionZ > cellDataI.minGasPlumeDist() && positionZ <= cellDataI.gasPlumeDist())
            {
                entries[rhs] -= faceArea * (lambdaW * dV_w + lambdaN * dV_n) * fabs((unitOuterNormal*permeability)/(dist))
                * (cellDataI.densityLiquidBelowPlume() * gravity_.two_norm() * cellDataI.minGasPlumeDist()
                + cellDataI.densityLiquidInPlume() * gravity_.two_norm() * (positionZ-cellDataI.minGasPlumeDist()));
            }
            else if(veModelI == 0 && positionZ > cellDataI.gasPlumeDist())
            {
                entries[rhs] -= faceArea * (lambdaW * dV_w + lambdaN * dV_n) * fabs((unitOuterNormal*permeability)/(dist))
                * (cellDataI.densityLiquidBelowPlume() * gravity_.two_norm() * cellDataI.minGasPlumeDist()
                + cellDataI.densityLiquidInPlume() * gravity_.two_norm() * (cellDataI.gasPlumeDist()-cellDataI.minGasPlumeDist())
                + cellDataI.density(nPhaseIdx) * gravity_.two_norm() * (positionZ-cellDataI.gasPlumeDist()));
            }
            else if(veModelI == 1 && positionZ > cellDataI.minGasPlumeDist())
            {
                entries[rhs] -= faceArea * (lambdaW * dV_w + lambdaN * dV_n) * fabs((unitOuterNormal*permeability)/(dist))
                * (cellDataI.densityLiquidBelowPlume() * gravity_.two_norm() * cellDataI.minGasPlumeDist()
                + cellDataI.densityLiquidInPlume() * gravity_.two_norm() * (positionZ-cellDataI.minGasPlumeDist()));
            }
            else if(veModelJ < 2 && positionZ <= cellDataJ.minGasPlumeDist())
            {
                entries[rhs] += faceArea * (lambdaW * dV_w + lambdaN * dV_n) * fabs((unitOuterNormal*permeability)/(dist))
                * cellDataJ.densityLiquidBelowPlume() * gravity_.two_norm() * positionZ;
            }
            else if(veModelJ == 0 && positionZ > cellDataJ.minGasPlumeDist()
                && positionZ <= cellDataJ.gasPlumeDist())
            {
                entries[rhs] += faceArea * (lambdaW * dV_w + lambdaN * dV_n) * fabs((unitOuterNormal*permeability)/(dist))
                * (cellDataJ.densityLiquidBelowPlume() * gravity_.two_norm() * cellDataJ.minGasPlumeDist()
                + cellDataJ.densityLiquidInPlume() * gravity_.two_norm() * (positionZ-cellDataJ.minGasPlumeDist()));
            }
            else if(veModelJ == 0 && positionZ > cellDataJ.gasPlumeDist())
            {
                entries[rhs] += faceArea * (lambdaW * dV_w + lambdaN * dV_n) * fabs((unitOuterNormal*permeability)/(dist))
                * (cellDataJ.densityLiquidBelowPlume() * gravity_.two_norm() * cellDataJ.minGasPlumeDist()
                + cellDataJ.densityLiquidInPlume() * gravity_.two_norm() * (cellDataJ.gasPlumeDist()-cellDataJ.minGasPlumeDist())
                + cellDataJ.density(nPhaseIdx) * gravity_.two_norm() * (positionZ-cellDataJ.gasPlumeDist()));
            }
            else if(veModelJ == 1 && positionZ > cellDataJ.minGasPlumeDist())
            {
                entries[rhs] += faceArea * (lambdaW * dV_w + lambdaN * dV_n) * fabs((unitOuterNormal*permeability)/(dist))
                * (cellDataJ.densityLiquidBelowPlume() * gravity_.two_norm() * cellDataJ.minGasPlumeDist()
                + cellDataJ.densityLiquidInPlume() * gravity_.two_norm() * (positionZ-cellDataJ.minGasPlumeDist()));
            }

            if(enableVolumeIntegral)
            {
                if(veModelI < 2 && positionZ <= cellDataI.minGasPlumeDist())
                {
                    entries[rhs] -= volume * faceArea / perimeter * (lambdaW * gV_w + lambdaN * gV_n)
                    * ((unitDistVec*permeability)/(dist))
                    * cellDataI.densityLiquidBelowPlume() * gravity_.two_norm() * positionZ;
                }
                else if(veModelI == 0 && positionZ > cellDataI.minGasPlumeDist()
                    && positionZ <= cellDataI.gasPlumeDist())
                {
                    entries[rhs] -= volume * faceArea / perimeter * (lambdaW * gV_w + lambdaN * gV_n)
                    * ((unitDistVec*permeability)/(dist))
                    * (cellDataI.densityLiquidBelowPlume() * gravity_.two_norm() * cellDataI.minGasPlumeDist()
                    + cellDataI.densityLiquidInPlume() * gravity_.two_norm() * (positionZ-cellDataI.minGasPlumeDist()));
                }
                else if(veModelI == 0 && positionZ > cellDataI.gasPlumeDist())
                {
                    entries[rhs] -= volume * faceArea / perimeter * (lambdaW * gV_w + lambdaN * gV_n)
                    * ((unitDistVec*permeability)/(dist))
                    * (cellDataI.densityLiquidBelowPlume() * gravity_.two_norm() * cellDataI.minGasPlumeDist()
                    + cellDataI.densityLiquidInPlume() * gravity_.two_norm()
                    * (cellDataI.gasPlumeDist()-cellDataI.minGasPlumeDist())
                    + cellDataI.density(nPhaseIdx) * gravity_.two_norm() * (positionZ-cellDataI.gasPlumeDist()));
                }
                else if(veModelI == 1 &&positionZ > cellDataI.minGasPlumeDist())
                {
                    entries[rhs] -= volume * faceArea / perimeter * (lambdaW * gV_w + lambdaN * gV_n)
                    * ((unitDistVec*permeability)/(dist))
                    * (cellDataI.densityLiquidBelowPlume() * gravity_.two_norm() * cellDataI.minGasPlumeDist()
                    + cellDataI.densityLiquidInPlume() * gravity_.two_norm() * (positionZ-cellDataI.minGasPlumeDist()));
                }
                else if(veModelJ < 2 && positionZ <= cellDataJ.minGasPlumeDist())
                {
                    entries[rhs] += volume * faceArea / perimeter * (lambdaW * gV_w + lambdaN * gV_n)
                    * ((unitDistVec*permeability)/(dist))
                    * cellDataJ.densityLiquidBelowPlume() * gravity_.two_norm() * positionZ;
                }
                else if(veModelJ == 0 && positionZ > cellDataJ.minGasPlumeDist()
                    && positionZ <= cellDataJ.gasPlumeDist())
                {
                    entries[rhs] += volume * faceArea / perimeter * (lambdaW * gV_w + lambdaN * gV_n)
                    * ((unitDistVec*permeability)/(dist))
                    * (cellDataJ.densityLiquidBelowPlume() * gravity_.two_norm() * cellDataJ.minGasPlumeDist()
                    + cellDataJ.densityLiquidInPlume() * gravity_.two_norm()
                    * (positionZ-cellDataJ.minGasPlumeDist()));
                }
                else if(veModelJ == 0 && positionZ > cellDataJ.gasPlumeDist())
                {
                    entries[rhs] += volume * faceArea / perimeter * (lambdaW * gV_w + lambdaN * gV_n)
                    * ((unitDistVec*permeability)/(dist))
                    * (cellDataJ.densityLiquidBelowPlume() * gravity_.two_norm() * cellDataJ.minGasPlumeDist()
                    + cellDataJ.densityLiquidInPlume() * gravity_.two_norm()
                    * (cellDataJ.gasPlumeDist()-cellDataJ.minGasPlumeDist())
                    + cellDataJ.density(nPhaseIdx) * gravity_.two_norm() * (positionZ-cellDataJ.gasPlumeDist()));
                }
                else if(veModelJ == 1 && positionZ > cellDataJ.minGasPlumeDist())
                {
                    entries[rhs] += volume * faceArea / perimeter * (lambdaW * gV_w + lambdaN * gV_n)
                    * ((unitDistVec*permeability)/(dist))
                    * (cellDataJ.densityLiquidBelowPlume() * gravity_.two_norm() * cellDataJ.minGasPlumeDist()
                    + cellDataJ.densityLiquidInPlume() * gravity_.two_norm()
                    * (positionZ-cellDataJ.minGasPlumeDist()));
                }
            }

            // calculate capillary pressure gradient
            Scalar capillaryPressureI = cellDataI.capillaryPressure();
            Scalar capillaryPressureJ = cellDataJ.capillaryPressure();
            if(veModelI < 2)
                capillaryPressureI = problem_.pressureModel().reconstCapillaryPressure(positionZ, elementI);
            else if(veModelJ < 2)
                capillaryPressureJ = problem_.pressureModel().reconstCapillaryPressure(positionZ, neighbor);

            Scalar pCGradient = (capillaryPressureI - capillaryPressureJ) / dist;

            // include capillary pressure fluxes
            switch (pressureType)
            {
                case pw:
                {
                    //add capillary pressure term to right hand side
                    entries[rhs] += lambdaN * dV_n * fabs(permeability * unitOuterNormal) * pCGradient * faceArea;
                    if(enableVolumeIntegral)
                        entries[rhs]-= lambdaN * gV_n * (permeability * unitDistVec) * pCGradient * volume * faceArea / perimeter;
                    break;
                }
                case pn:
                {
                    //add capillary pressure term to right hand side
                    entries[rhs] -= lambdaW * dV_w * fabs(permeability * unitOuterNormal) * pCGradient * faceArea;
                    if(enableVolumeIntegral)
                        entries[rhs]+= lambdaW * gV_w * (permeability * unitDistVec) * pCGradient * volume * faceArea / perimeter;
                    break;
                }
            }
        }   // end !first
    }
    else
    {
        ParentType::getFlux(entries, intersection, cellDataI, first);
    }
    return;
}
}//end namespace Dumux
#endif
