// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
#ifndef DUMUX_VARIABLECLASS_ADAPTIVEVE_2P2CVE_HH
#define DUMUX_VARIABLECLASS_ADAPTIVEVE_2P2CVE_HH

#include <dune/grid/utility/persistentcontainer.hh>
#include <dumux/decoupled/2pve/variableclassadaptive2pvefulld.hh>

/**
 * @file
 * @brief  Base class holding the variables for sequential models.
 */

namespace Dumux
{
/*!
 * \ingroup IMPET
 */
//! Base class holding the variables and discretized data for sequential models.
/*!
 * Stores global information and variables that are common for all sequential models and also functions needed to access these variables.
 * Can be directly used for a single phase model.
 *
 * @tparam TypeTag The Type Tag
 *
 */
template<class TypeTag>
class VariableClassAdaptive2P2CVE: public VariableClassAdaptive2PVE<TypeTag>
{
private:
    using ParentType = VariableClassAdaptive2PVE<TypeTag>;

    using Problem = GetPropType<TypeTag, Properties::Problem>;
    using GridView = typename GetPropType<TypeTag, Properties::GridGeometry>::GridView;
    using CellData = GetPropType<TypeTag, Properties::CellData>;
    using AdaptedValues = typename CellData::AdaptedValues;

    using Grid = GetPropType<TypeTag, Properties::Grid>;
    using LevelGridView = typename Grid::LevelGridView;
    using Element = typename GridView::Traits::template Codim<0>::Entity;
    using PersistentContainer = Dune::PersistentContainer<Grid, AdaptedValues>;

private:
    const Grid& grid_;

public:
    //! Constructs an adaptive VariableClass object
    /**
     * In addition to providing a storage object for cell-centered Methods, this class provides
     * mapping functionality to adapt the grid.
     *
     *  @param gridView a DUNE gridview object corresponding to diffusion and transport equation
     */
    VariableClassAdaptive2P2CVE(const GridView& gridView) :
        ParentType(gridView), grid_(gridView.grid())
    {}


    /*!
     * Reconstruct missing primary variables (where elements are created/deleted)
     *
     * To reconstruct the solution in father elements, problem properties might
     * need to be accessed.
     * Starting from the lowest level, the old solution is mapped on the new grid:
     * Where coarsened, new cells get information from old father element.
     * Where refined, a new solution is reconstructed from the old father cell,
     * and then a new son is created. That is then stored into the general data
     * structure (CellData).
     *
     * @param problem The current problem
     */
    void reconstructPrimVars(Problem& problem)
    {
        ParentType::reconstructPrimVars(problem);

        problem.pressureModel().adaptPressure();
    }

};
}
#endif
