// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup SequentialTwoPTwoCModel
 * \brief Finite volume 2p2c pressure model
 */
#ifndef DUMUX_FVPRESSURE2P2CVE_HH
#define DUMUX_FVPRESSURE2P2CVE_HH

// dumux environment
#include <dumux/porousmediumflow/2p2c/sequential/fvpressure.hh>
#include <dumux/material/fluidstates/pseudo1p2c.hh>

namespace Dumux {
/*!
 * \ingroup SequentialTwoPTwoCModel
 * \brief The finite volume model for the solution of the compositional pressure equation.
 *
 * Provides a Finite Volume implementation for the pressure equation of a compressible
 * system with two components. An IMPES-like method is used for the sequential
 * solution of the problem.  Diffusion is neglected, capillarity can be regarded.
 * Isothermal conditions and local thermodynamic
 * equilibrium are assumed.  Gravity is included.
 * \f[
         c_{total}\frac{\partial p}{\partial t} + \sum_{\kappa} \frac{\partial v_{total}}{\partial C^{\kappa}}
         \nabla \cdot \left( \sum_{\alpha} X^{\kappa}_{\alpha} \varrho_{\alpha} \bf{v}_{\alpha}\right)
          = \sum_{\kappa} \frac{\partial v_{total}}{\partial C^{\kappa}} q^{\kappa},
 *  \f]
 *  where \f$\bf{v}_{\alpha} = - \lambda_{\alpha} \bf{K} \left(\nabla p_{\alpha} + \rho_{\alpha} \bf{g} \right) \f$.
 *  \f$ c_{total} \f$ represents the total compressibility, for constant porosity this yields
 *  \f$ - \frac{\partial V_{total}}{\partial p_{\alpha}} \f$,
 *  \f$p_{\alpha} \f$ denotes the phase pressure, \f$ \bf{K} \f$ the absolute permeability,
 *  \f$ \lambda_{\alpha} \f$ the phase mobility,
 *  \f$ \rho_{\alpha} \f$ the phase density and \f$ \bf{g} \f$ the gravity constant and
 *  \f$ C^{\kappa} \f$ the total Component concentration.
 * See paper SPE 99619 or "Analysis of a Compositional Model for Fluid
 * Flow in Porous Media" by Chen, Qin and Ewing for derivation.
 *
 * The pressure base class FVPressure assembles the matrix and right-hand-side vector and solves for the pressure vector,
 * whereas this class provides the actual entries for the matrix and RHS vector.
 * The partial derivatives of the actual fluid volume \f$ v_{total} \f$ are gained by using a secant method.
 *
 */
template<class TypeTag> class FVPressure2P2CVE
: public FVPressure2P2C<TypeTag>
{
    //the model implementation
    using Implementation = GetPropType<TypeTag, Properties::PressureModel>;
    using ParentType = FVPressure2P2C<TypeTag>;

    using GridView = typename GetPropType<TypeTag, Properties::GridGeometry>::GridView;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using Problem = GetPropType<TypeTag, Properties::Problem>;

    using SpatialParams = GetPropType<TypeTag, Properties::SpatialParams>;
    using MaterialLaw = GetPropType<TypeTag, Properties::MaterialLaw>;

    using Indices = GetPropType<TypeTag, Properties::Indices>;
    using BoundaryTypes = GetPropType<TypeTag, Properties::SequentialBoundaryTypes>;

    using FluidSystem = GetPropType<TypeTag, Properties::FluidSystem>;
    using FluidState = GetPropType<TypeTag, Properties::FluidState>;
    using FluidState1p2c = PseudoOnePTwoCFluidState<Scalar, FluidSystem>;

    using CellData = GetPropType<TypeTag, Properties::CellData>;
    enum
    {
        dim = GridView::dimension, dimWorld = GridView::dimensionworld
    };
    enum
    {
        pw = Indices::pressureW,
        pn = Indices::pressureN,
        pGlobal = Indices::pressureGlobal
    };
    enum
    {
        wPhaseIdx = Indices::wPhaseIdx, nPhaseIdx = Indices::nPhaseIdx,
        wCompIdx = Indices::wPhaseIdx, nCompIdx = Indices::nPhaseIdx,
        contiWEqIdx = Indices::contiWEqIdx, contiNEqIdx = Indices::contiNEqIdx,
        numPhases = getPropValue<TypeTag, Properties::NumPhases>(),
        numComponents = getPropValue<TypeTag, Properties::NumComponents>()
    };
    enum VEModel
    {
        sharpInterface,
        capillaryFringe
    };


    /*!
     * \brief Indices of matrix and rhs entries
     * During the assembling of the global system of equations get-functions are called
     * (getSource(), getFlux(), etc.), which return global matrix or right hand side entries
     * in a vector. These can be accessed using following indices:
     */
    enum
    {
        rhs = 1,//!<index for the right hand side entry
        matrix = 0//!<index for the global matrix entry

    };

    // using declarations to abbreviate several dune classes...
    using Element = typename GridView::Traits::template Codim<0>::Entity;
    using Intersection = typename GridView::Intersection;

    // convenience shortcuts for Vectors/Matrices
    using GlobalPosition = Dune::FieldVector<Scalar, dimWorld>;
    using DimMatrix = Dune::FieldMatrix<Scalar, dim, dim>;
    using PhaseVector = Dune::FieldVector<Scalar, numPhases>;
    using ComponentVector = Dune::FieldVector<Scalar, numComponents>;
    using PrimaryVariables = GetPropType<TypeTag, Properties::PrimaryVariables>;

    // the typenames used for the stiffness matrix and solution vector
    using Matrix = GetPropType<TypeTag, Properties::PressureCoefficientMatrix>;

protected:
    //! @copydoc FVPressure::EntryType
    using EntryType = Dune::FieldVector<Scalar, 2>;

    Problem& problem()
    {
        return problem_;
    }
    const Problem& problem() const
    {
        return problem_;
    }

public:

    void volumeDerivatives(const GlobalPosition& globalPos, const Element& element);

    void getFlux(EntryType& entries, const Intersection& intersection, const CellData& cellDataI, const bool first);

    void getFluxOnBoundary(EntryType& entries, const Intersection& intersection, const CellData& cellDataI, const bool first);

    void initialMaterialLaws(bool compositional);

    //updates secondary variables for one cell and stores in the variables object
    void updateMaterialLawsInElement(const Element& element, bool postTimeStep);

    /*!\brief Returns the reconstructed phase pressure, assuming gasPlumeDist is known
     *
     * \param phaseIdx Index of a fluid phase
     */
    Scalar reconstPressure(const Scalar height, const int phaseIdx, const Element& element) const
    {
        const int eIdxGlobal = problem_.variables().index(element);
        const CellData& cellData = problem_.variables().cellData(eIdxGlobal);
        const int veModel = cellData.veModel();
        const Scalar coarsePressureW = cellData.pressure(wPhaseIdx);
        const Scalar densityWBelowPlume = cellData.densityLiquidBelowPlume();
        const Scalar densityWInPlume = cellData.densityLiquidInPlume();
        const Scalar densityNInPlume = cellData.density(nPhaseIdx);
        const Scalar gasPlumeDist = cellData.gasPlumeDist();
        const Scalar minGasPlumeDist = cellData.minGasPlumeDist();
        const Scalar gravity = problem_.gravity().two_norm();

        Scalar reconstPressure[numPhases];
        if(veModel == sharpInterface)
        {
            if(height <= minGasPlumeDist)
            {
                reconstPressure[wPhaseIdx] = coarsePressureW - densityWBelowPlume * gravity * height;
                reconstPressure[nPhaseIdx] = reconstPressure[wPhaseIdx];
            }
            else if(height > minGasPlumeDist && height <= gasPlumeDist)
            {
                reconstPressure[wPhaseIdx] = coarsePressureW - densityWBelowPlume * gravity * minGasPlumeDist
                - densityWInPlume * gravity * (height - minGasPlumeDist);
                reconstPressure[nPhaseIdx] = reconstPressure[wPhaseIdx];
            }
            else if(height > gasPlumeDist)
            {
                reconstPressure[wPhaseIdx] = coarsePressureW - densityWBelowPlume * gravity * minGasPlumeDist
                - densityWInPlume * gravity * (gasPlumeDist - minGasPlumeDist) - densityNInPlume * gravity * (height - gasPlumeDist);
                reconstPressure[nPhaseIdx]= reconstPressure[wPhaseIdx];
            }
        }
        else if(veModel == capillaryFringe)
        {
            if(height <= minGasPlumeDist)
            {
                reconstPressure[wPhaseIdx] = coarsePressureW - densityWBelowPlume * gravity * height;
                reconstPressure[nPhaseIdx] = reconstPressure[wPhaseIdx] + entryP_;
            }
            else if(height > minGasPlumeDist && height <= gasPlumeDist)
            {
                reconstPressure[wPhaseIdx] = coarsePressureW - densityWBelowPlume * gravity * minGasPlumeDist
                - densityWInPlume * gravity * (height - minGasPlumeDist);
                reconstPressure[nPhaseIdx] = reconstPressure[wPhaseIdx] + entryP_;
            }
            else if(height > gasPlumeDist)
            {
                reconstPressure[wPhaseIdx] = coarsePressureW - densityWBelowPlume * gravity * minGasPlumeDist
                - densityWInPlume * gravity * (height - minGasPlumeDist);
                reconstPressure[nPhaseIdx] = coarsePressureW - densityWBelowPlume * gravity * minGasPlumeDist
                - densityWInPlume * gravity * (gasPlumeDist - minGasPlumeDist) + entryP_ - densityNInPlume * gravity * (height - gasPlumeDist);
            }
        }
        else //reconstruct phase pressures for no ve model
        {
            reconstPressure[wPhaseIdx] = coarsePressureW;
            reconstPressure[nPhaseIdx] = cellData.pressure(nPhaseIdx);
        }

        return reconstPressure[phaseIdx];
    }

    /*!\brief Returns the reconstructed capillary phase pressure, assuming gasPlumeDist is known
     *
     * \param phaseIdx Index of a fluid phase
     */
    Scalar reconstCapillaryPressure(const Scalar height, const Element& element) const
    {
        const int eIdxGlobal = problem_.variables().index(element);
        const CellData& cellData = problem_.variables().cellData(eIdxGlobal);
        const int veModel = cellData.veModel();
        const Scalar densityW = cellData.densityLiquidInPlume();
        const Scalar densityN = cellData.density(nPhaseIdx);
        const Scalar gasPlumeDist = cellData.gasPlumeDist();

        Scalar reconstCapillaryPressure;
        if(veModel == sharpInterface)
        {
            reconstCapillaryPressure = 0.0;
        }
        else if (veModel == capillaryFringe)
        {
            if(height <= gasPlumeDist)
            {
                reconstCapillaryPressure = entryP_;
            }
            else if (height > gasPlumeDist)
            {
                reconstCapillaryPressure = densityW * problem_.gravity().two_norm() * (height-gasPlumeDist)
                + entryP_ - densityN * problem_.gravity().two_norm() * (height-gasPlumeDist);
            }
        }
        else //reconstruct phase pressures for no ve model
        {
            reconstCapillaryPressure = cellData.capillaryPressure();
        }

        return reconstCapillaryPressure;
    }

    /*!\brief Returns the reconstructed phase saturation, assuming gasPlumeDist is known
     *
     * \param phaseIdx Index of a fluid phase
     */
    Scalar reconstSaturation(const Scalar height, const int phaseIdx, const Element& element) const
    {
        const int eIdxGlobal = problem_.variables().index(element);
        const CellData& cellData = problem_.variables().cellData(eIdxGlobal);
        const int veModel = cellData.veModel();
        const Scalar gasPlumeDist = cellData.gasPlumeDist();
        const Scalar minGasPlumeDist = cellData.minGasPlumeDist();
        //set residual saturations according to some criterium
        const Scalar resSatW = problem_.spatialParams().materialLawParams(element).swr();
        const Scalar resSatN = problem_.spatialParams().materialLawParams(element).snr();

        Scalar reconstSaturation[numPhases];
        if (veModel == sharpInterface) //reconstruct phase saturation for sharp interface ve model
        {
            if(height <= minGasPlumeDist)
            {
                reconstSaturation[wPhaseIdx] = 1.0;
                reconstSaturation[nPhaseIdx] = 1.0 - reconstSaturation[wPhaseIdx];
            }
            else if(height > minGasPlumeDist && height <= gasPlumeDist)
            {
                reconstSaturation[wPhaseIdx] = 1.0 - resSatN;
                reconstSaturation[nPhaseIdx] = 1.0 - reconstSaturation[wPhaseIdx];
            }
            else if(height > gasPlumeDist)
            {
                reconstSaturation[wPhaseIdx] = resSatW;
                reconstSaturation[nPhaseIdx] = 1.0 - reconstSaturation[wPhaseIdx];
            }
        }
        else if (veModel == capillaryFringe) //reconstruct phase saturation for capillary fringe model
        {
            if(height <= minGasPlumeDist)
            {
                reconstSaturation[wPhaseIdx] = 1.0;
                reconstSaturation[nPhaseIdx] = 1.0 - reconstSaturation[wPhaseIdx];
            }
            else if(height > minGasPlumeDist && height <= gasPlumeDist)
            {
                reconstSaturation[wPhaseIdx] = 1.0 - resSatN;
                reconstSaturation[nPhaseIdx] = 1.0 - reconstSaturation[wPhaseIdx];
            }
            else if(height > gasPlumeDist)
            {
                const Scalar densityW = cellData.densityLiquidInPlume();
                const Scalar densityN = cellData.density(nPhaseIdx);

                reconstSaturation[wPhaseIdx] = std::pow(((height - gasPlumeDist) * (densityW - densityN)
                * problem_.gravity().two_norm() + entryP_), (-lambda_))
                * std::pow(entryP_, lambda_) * (1.0 - resSatW - resSatN) + resSatW;
                reconstSaturation[nPhaseIdx] = 1.0 - reconstSaturation[wPhaseIdx];
            }
        }
        else //reconstruct phase saturation for no ve model
        {
            reconstSaturation[wPhaseIdx] = cellData.saturation(wPhaseIdx);
            reconstSaturation[nPhaseIdx] = cellData.saturation(nPhaseIdx);
        }

        return reconstSaturation[phaseIdx];
    }

    /*! \brief Calculates wetting saturation integral over height given two bounds
     */
    Scalar saturationIntegral(const Scalar lowerBound,
                              const Scalar upperBound,
                              const Element& element,
                              const std::vector<Scalar> distances,
                              const FluidState& fluidStatePlume) const
    {
        const int eIdxGlobal = problem_.variables().index(element);
        const CellData& cellData = problem_.variables().cellData(eIdxGlobal);
        const int veModel = cellData.veModel();
        const Scalar gasPlumeDist = distances[0];
        const Scalar minGasPlumeDist = distances[1];
        const Scalar resSatW = problem_.spatialParams().materialLawParams(element).swr();
        const Scalar resSatN = problem_.spatialParams().materialLawParams(element).snr();

        //different integration areas:
        //1. from bottom to minGasPlumeDist, 2. from minGasPlumeDist to gasPlumeDist, 3. from gasPlumeDist to top
        Scalar integral = 0.0;

        if(lowerBound < minGasPlumeDist)
        {
            const Scalar upperPartBound = std::min(minGasPlumeDist, upperBound);
            integral += upperPartBound - lowerBound;
        }
        if(upperBound > minGasPlumeDist && lowerBound < gasPlumeDist )
        {
            const Scalar lowerpartBound = std::max(minGasPlumeDist, lowerBound);
            const Scalar upperPartBound = std::min(gasPlumeDist, upperBound);
            integral += (upperPartBound - lowerpartBound)*(1.0-resSatN);
        }
        if(upperBound > gasPlumeDist)
        {
            const Scalar lowerPartBound = std::max(gasPlumeDist, lowerBound);
            if(veModel == sharpInterface)
            {
                integral += (upperBound - lowerPartBound)*resSatW;
            }
            else if(veModel == capillaryFringe)
            {
                const Scalar densityW = fluidStatePlume.density(wPhaseIdx);
                const Scalar densityN = fluidStatePlume.density(nPhaseIdx);

                integral += (std::pow(entryP_, lambda_) * (1 - resSatW - resSatN)) /
                ((1 - lambda_) * (densityW - densityN) * problem_.gravity().two_norm()) *
                (std::pow(((upperBound - gasPlumeDist) * (densityW - densityN) *
                problem_.gravity().two_norm() + entryP_), (1 - lambda_))
                - std::pow(((lowerPartBound - gasPlumeDist) * (densityW - densityN) *
                problem_.gravity().two_norm() + entryP_), (1 - lambda_)))
                + resSatW * (upperBound - lowerPartBound);
            }
        }

        return integral;
    }

    /*! \brief Calculates wetting saturation integral over height given two bounds,
     * assuming gasPlumeDist is known
     */
    Scalar saturationIntegral(const Scalar lowerBound,
                              const Scalar upperBound,
                              const Element& element) const
    {
        const int eIdxGlobal = problem_.variables().index(element);
        const CellData& cellData = problem_.variables().cellData(eIdxGlobal);
        const int veModel = cellData.veModel();

        const Scalar gasPlumeDist = cellData.gasPlumeDist();
        const Scalar minGasPlumeDist = cellData.minGasPlumeDist();
        const Scalar resSatW = problem_.spatialParams().materialLawParams(element).swr();
        const Scalar resSatN = problem_.spatialParams().materialLawParams(element).snr();

        //different integration areas:
        //1. from bottom to minGasPlumeDist, 2. from minGasPlumeDist to gasPlumeDist, 3. from gasPlumeDist to top
        Scalar integral = 0.0;

        if(lowerBound < minGasPlumeDist)
        {
            const Scalar upperPartBound = std::min(minGasPlumeDist, upperBound);
            integral += upperPartBound - lowerBound;
        }
        if(upperBound > minGasPlumeDist && lowerBound < gasPlumeDist )
        {
            const Scalar lowerpartBound = std::max(minGasPlumeDist, lowerBound);
            const Scalar upperPartBound = std::min(gasPlumeDist, upperBound);
            integral += (upperPartBound - lowerpartBound)*(1.0-resSatN);
        }
        if(upperBound > gasPlumeDist)
        {
            const Scalar lowerPartBound = std::max(gasPlumeDist, lowerBound);
            if(veModel == sharpInterface)
            {
                integral += (upperBound - lowerPartBound)*resSatW;
            }
            else if(veModel == capillaryFringe)
            {
                const Scalar densityW = cellData.densityLiquidInPlume();
                const Scalar densityN = cellData.density(nPhaseIdx);

                integral += (std::pow(entryP_, lambda_) * (1 - resSatW - resSatN)) /
                ((1 - lambda_) * (densityW - densityN) * problem_.gravity().two_norm()) *
                (std::pow(((upperBound - gasPlumeDist) * (densityW - densityN) *
                problem_.gravity().two_norm() + entryP_), (1 - lambda_))
                - std::pow(((lowerPartBound - gasPlumeDist) * (densityW - densityN) *
                problem_.gravity().two_norm() + entryP_), (1 - lambda_)))
                + resSatW * (upperBound - lowerPartBound);
            }
        }

        return integral;
    }

    /*! \brief Determines fluid state inside plume
     */
    FluidState fluidStatePlume(const Element& element, const PhaseVector columnRefPressures) const
    {
        const GlobalPosition& globalPos = element.geometry().center();

        const Scalar resSatW = problem_.spatialParams().materialLawParams(element).swr();
        const Scalar porosity = problem_.spatialParams().porosity(element);
        const Scalar temperature = problem_.temperatureAtPos(globalPos);

        FluidState fluidStatePlume;
        CompositionalFlash<Scalar, FluidSystem> flashSolver;
        flashSolver.saturationFlash2p2c(fluidStatePlume,
                                        resSatW,
                                        columnRefPressures,
                                        porosity,
                                        temperature);

        return fluidStatePlume;
    }

    /*! \brief Determines fluid state inside plume, assuming gasPlumeDist is set
     */
    FluidState fluidStatePlume(const Element& element) const
    {
        const int eIdxGlobal = problem_.variables().index(element);
        const CellData& cellData = problem_.variables().cellData(eIdxGlobal);
        const GlobalPosition& globalPos = element.geometry().center();

        const Scalar resSatW = problem_.spatialParams().materialLawParams(element).swr();
        const Scalar porosity = problem_.spatialParams().porosity(element);
        const Scalar temperature = problem_.temperatureAtPos(globalPos);
        const Scalar minGasPlumeDist = cellData.minGasPlumeDist();
        const Scalar aquiferHeight = problem_.aquiferHeight();

        PhaseVector columnRefPressures;
        columnRefPressures[wPhaseIdx] = reconstPressure(minGasPlumeDist, wPhaseIdx, element);
        columnRefPressures[nPhaseIdx] = reconstPressure(aquiferHeight, nPhaseIdx, element);

        FluidState fluidStatePlume;
        CompositionalFlash<Scalar, FluidSystem> flashSolver;
        flashSolver.saturationFlash2p2c(fluidStatePlume,
                                        resSatW,
                                        columnRefPressures,
                                        porosity,
                                        temperature);

        return fluidStatePlume;
    }

    /*! \brief Determines fluid state below plume
     */
    FluidState1p2c fluidStateBelowPlume(const Element& element, const PhaseVector columnRefPressures) const
    {
        const GlobalPosition& globalPos = element.geometry().center();

        const int presentPhaseIdx = wPhaseIdx;
        const Scalar feedMassFractionWater = 1.0;
        const Scalar temperature = problem_.temperatureAtPos(globalPos);

        FluidState1p2c fluidStateBelowPlume;
        CompositionalFlash<Scalar, FluidSystem> flashSolver;
        flashSolver.concentrationFlash1p2c(fluidStateBelowPlume,
                                           feedMassFractionWater,
                                           columnRefPressures,
                                           presentPhaseIdx,
                                           temperature);

        return fluidStateBelowPlume;
    }

    /*! \brief Determines fluid state below plume, assuming gasPlumeDist is set
     */
    FluidState1p2c fluidStateBelowPlume(const Element& element) const
    {
        const int eIdxGlobal = problem_.variables().index(element);
        const CellData& cellData = problem_.variables().cellData(eIdxGlobal);
        const GlobalPosition& globalPos = element.geometry().center();

        const int presentPhaseIdx = wPhaseIdx;
        const Scalar feedMassFractionWater = 1.0;
        const Scalar temperature = problem_.temperatureAtPos(globalPos);

        PhaseVector columnRefPressures;
        columnRefPressures[wPhaseIdx] = cellData.pressure(wPhaseIdx);
        columnRefPressures[nPhaseIdx] = cellData.pressure(nPhaseIdx);// is not actually relevant

        FluidState1p2c fluidStateBelowPlume;
        CompositionalFlash<Scalar, FluidSystem> flashSolver;
        flashSolver.concentrationFlash1p2c(fluidStateBelowPlume,
                                           feedMassFractionWater,
                                           columnRefPressures,
                                           presentPhaseIdx,
                                           temperature);

        return fluidStateBelowPlume;
    }

    /*! \brief Completes fluid state, only pressure has to be set before
     */
    void completeFluidState(FluidState& fluidState,
                            const Element& element,
                            const ComponentVector totalConcentration,
                            const std::vector<Scalar> distances,
                            const FluidState& fluidStatePlume)
    {
        const int eIdxGlobal = problem_.variables().index(element);
        CellData& cellData = problem_.variables().cellData(eIdxGlobal);
        const GlobalPosition& globalPos = element.geometry().center();
        fluidState.setTemperature(problem().temperatureAtPos(globalPos));

        PhaseVector columnRefPressures;
        columnRefPressures[wPhaseIdx] = fluidState.pressure(wPhaseIdx);
        columnRefPressures[nPhaseIdx] = fluidState.pressure(nPhaseIdx);
        FluidState1p2c fluidStateBelowPlume = this->fluidStateBelowPlume(element, columnRefPressures);

        const Scalar densityWBelowPlume = fluidStateBelowPlume.density(wPhaseIdx);
        const Scalar densityWInPlume = fluidStatePlume.density(wPhaseIdx);
        const Scalar densityNInPlume = fluidStatePlume.density(nPhaseIdx);

        const Scalar molarDensityWBelowPlume = fluidStateBelowPlume.molarDensity(wPhaseIdx);
        const Scalar molarDensityWInPlume = fluidStatePlume.molarDensity(wPhaseIdx);
        const Scalar molarDensityNInPlume = fluidStatePlume.molarDensity(nPhaseIdx);

        const Scalar massFractionWNBelowPlume = fluidStateBelowPlume.massFraction(wPhaseIdx, nCompIdx);
        const Scalar massFractionWWInPlume = fluidStatePlume.massFraction(wPhaseIdx, wCompIdx);
        const Scalar massFractionNWInPlume = fluidStatePlume.massFraction(nPhaseIdx, wCompIdx);

        const Scalar moleFractionWNBelowPlume = fluidStateBelowPlume.moleFraction(wPhaseIdx, nCompIdx);
        const Scalar moleFractionWNInPlume = fluidStatePlume.moleFraction(wPhaseIdx, nCompIdx);
        const Scalar moleFractionNNInPlume = fluidStatePlume.moleFraction(nPhaseIdx, nCompIdx);

        //determine known saturation integrals
        const Scalar aquiferHeight = problem_.aquiferHeight();
        const Scalar minGasPlumeDist = distances[1];
        const Scalar plumeIntegral =
            saturationIntegral(minGasPlumeDist, aquiferHeight, element, distances, fluidStatePlume);

        // determine epsilon height due to volume mismatch
        const Scalar porosity = problem_.spatialParams().porosity(element);
        const Scalar volumeLiquidPhaseBelowPlume = (totalConcentration[wCompIdx] * aquiferHeight
            + plumeIntegral * porosity * (densityNInPlume * massFractionNWInPlume - densityWInPlume * massFractionWWInPlume)
            - (aquiferHeight - minGasPlumeDist) * porosity * densityNInPlume * massFractionNWInPlume)
            / densityWBelowPlume;
        const Scalar volumeAllPhases = (aquiferHeight - minGasPlumeDist) * porosity + volumeLiquidPhaseBelowPlume;
        const Scalar epsilonHeight = aquiferHeight - (volumeAllPhases/porosity);
        cellData.setEpsilonHeight(epsilonHeight);

        const Scalar fullIntegral =
            saturationIntegral(epsilonHeight, aquiferHeight, element, distances, fluidStatePlume);

        const Scalar saturationW = fullIntegral / (aquiferHeight - epsilonHeight);
        fluidState.setSaturation(wPhaseIdx, saturationW);
        fluidState.setSaturation(nPhaseIdx, 1.0 - saturationW);

        fluidState.setDensity(wPhaseIdx, ((minGasPlumeDist - epsilonHeight) * densityWBelowPlume + densityWInPlume * plumeIntegral) / fullIntegral);
        fluidState.setDensity(nPhaseIdx, densityNInPlume);
        fluidState.setMolarDensity(wPhaseIdx, ((minGasPlumeDist - epsilonHeight) * molarDensityWBelowPlume
        + molarDensityWInPlume * plumeIntegral) / fullIntegral);
        fluidState.setMolarDensity(nPhaseIdx, molarDensityNInPlume);

        cellData.setDensityLiquidInPlume(densityWInPlume);
        cellData.setDensityLiquidBelowPlume(densityWBelowPlume);

        cellData.setMassFractionLiquidInPlume(wCompIdx, massFractionWWInPlume);
        cellData.setMassFractionLiquidInPlume(nCompIdx, 1. - massFractionWWInPlume);
        cellData.setMassFractionLiquidBelowPlume(nCompIdx, massFractionWNBelowPlume);
        cellData.setMassFractionLiquidBelowPlume(wCompIdx, 1. - massFractionWNBelowPlume);

        const Scalar moleFractionWN = ((minGasPlumeDist - epsilonHeight) * molarDensityWBelowPlume * moleFractionWNBelowPlume
        + molarDensityWInPlume * moleFractionWNInPlume * plumeIntegral) /
        ((minGasPlumeDist - epsilonHeight) * molarDensityWBelowPlume + molarDensityWInPlume * plumeIntegral);
        fluidState.setMoleFraction(wPhaseIdx, nCompIdx, moleFractionWN);
        fluidState.setMoleFraction(wPhaseIdx, wCompIdx, 1. - moleFractionWN);
        fluidState.setMoleFraction(nPhaseIdx, nCompIdx, moleFractionNNInPlume);
        fluidState.setMoleFraction(nPhaseIdx, wCompIdx, 1. - moleFractionNNInPlume);

        cellData.setViscosityInPlume(wPhaseIdx, FluidSystem::viscosity(fluidStatePlume, wPhaseIdx));
        cellData.setViscosityInPlume(nPhaseIdx, FluidSystem::viscosity(fluidStatePlume, nPhaseIdx));
        fluidState.setViscosity(wPhaseIdx, FluidSystem::viscosity(fluidState, wPhaseIdx));
        fluidState.setViscosity(nPhaseIdx, FluidSystem::viscosity(fluidState, nPhaseIdx));
    }

    /*! \brief Calculates gasPlumeDist, distance of gas plume from bottom
     *
     * Stores minGasPlumeDist for all grid cells
     */
    std::vector<Scalar> gasPlumeDistances(const Element& element,
                                          const Scalar totalGasConcentration,
                                          const FluidState& fluidStatePlume) const
    {
        const int eIdxGlobal = problem_.variables().index(element);
        const CellData& cellData = problem_.variables().cellData(eIdxGlobal);
        const int veModel = cellData.veModel();
        Scalar minGasPlumeDist = cellData.minGasPlumeDist();
        const Scalar aquiferHeight = problem_.aquiferHeight();

        if(minGasPlumeDist > aquiferHeight)
            minGasPlumeDist = aquiferHeight;

        const Scalar resSatW = problem_.spatialParams().materialLawParams(element).swr();
        const Scalar resSatN = problem_.spatialParams().materialLawParams(element).snr();
        const Scalar porosity = problem_.spatialParams().porosity(element);

        const Scalar X_l_gComp = fluidStatePlume.massFraction(wPhaseIdx, nCompIdx);
        const Scalar X_g_gComp = fluidStatePlume.massFraction(nPhaseIdx, nCompIdx);
        const Scalar densityW = fluidStatePlume.density(wPhaseIdx);
        const Scalar densityN = fluidStatePlume.density(nPhaseIdx);

        Scalar gasPlumeDist = 0.0;

        if(totalGasConcentration < 1e-12)
            gasPlumeDist = aquiferHeight;
        else if (veModel == sharpInterface) //calculate gasPlumeDist for sharp interface ve model
        {
            const Scalar minGasPlumeIntegral = (aquiferHeight - minGasPlumeDist) *
            (porosity * (1-resSatW) * densityN * X_g_gComp + porosity * resSatW * densityW * X_l_gComp);
            //calculate gasPlumeDist for gasPlumeDist > minGasPlumeDist
            if (totalGasConcentration * aquiferHeight < minGasPlumeIntegral - 1e-6)
            {
                gasPlumeDist = (totalGasConcentration * aquiferHeight + porosity * densityN * X_g_gComp *
                (minGasPlumeDist * resSatN - aquiferHeight * (1-resSatW))
                + porosity * densityW * X_l_gComp * (minGasPlumeDist * (1-resSatN) - aquiferHeight * resSatW)) /
                (porosity * (1-resSatW-resSatN) * (densityW * X_l_gComp - densityN * X_g_gComp));
            }
            //calculate gasPlumeDist for gasPlumeDist < minGasPlumeDist and calculate new minGasPlumeDist
            else if (totalGasConcentration * aquiferHeight > minGasPlumeIntegral + 1e-6)
            {
                gasPlumeDist = aquiferHeight - (aquiferHeight * totalGasConcentration /
                ((porosity * (1-resSatW) * densityN * X_g_gComp) + (porosity * resSatW * densityW * X_l_gComp)));
                minGasPlumeDist = gasPlumeDist;
            }
            //calculate gasPlumeDist for gasPlumeDist = minGasPlumeDist
            else
            {
                gasPlumeDist = minGasPlumeDist;
            }
        }

        else if (veModel == capillaryFringe) //calculate gasPlumeDist for capillary fringe model
        {
            const Scalar minGasPlumeIntegral =
            porosity * densityN * X_g_gComp * (aquiferHeight - minGasPlumeDist)
            + porosity * (densityW * X_l_gComp - densityN * X_g_gComp) *
            (1.0 / (1.0 - lambda_) * std::pow(((aquiferHeight - minGasPlumeDist) * (densityW - densityN) * gravity_.two_norm()
            + entryP_),(1.0 - lambda_))
            * (1.0 - resSatW - resSatN) * std::pow(entryP_, lambda_) / ((densityW - densityN) * gravity_.two_norm())
            + resSatW * (aquiferHeight - minGasPlumeDist)
            - entryP_ * (1.0 - resSatW - resSatN) / ((1.0 - lambda_) * (densityW - densityN) * gravity_.two_norm()));

            const Scalar residualIntegral = (aquiferHeight - minGasPlumeDist) * porosity * (resSatN * densityN * X_g_gComp
            + (1 - resSatN) * densityW * X_l_gComp);

            Scalar residual = 1.0;
            // calculate gasPlumeDist for gasPlumeDist > minGasPlumeDist
            // check if gas concentration is high enough to sustain minGasPlumeDist
            if (totalGasConcentration * aquiferHeight < minGasPlumeIntegral - 1e-6
                && totalGasConcentration * aquiferHeight > residualIntegral)
            {
                //GasPlumeDist>0 (always in this case)
                gasPlumeDist = minGasPlumeDist; //XiStart
                //solve equation for
                int count = 0;
                for (;count < 100; count++)
                {
                    residual =
                    porosity * densityN * X_g_gComp * (aquiferHeight - gasPlumeDist)
                    + porosity * (densityW * X_l_gComp - densityN * X_g_gComp) *
                    (1.0 / (1.0 - lambda_) * std::pow(((aquiferHeight - gasPlumeDist) * (densityW - densityN) * gravity_.two_norm()
                    + entryP_),(1.0 - lambda_))
                    * (1.0 - resSatW - resSatN) * std::pow(entryP_, lambda_) / ((densityW - densityN) * gravity_.two_norm())
                    + resSatW * (aquiferHeight - gasPlumeDist)
                    - entryP_ * (1.0 - resSatW - resSatN) / ((1.0 - lambda_) * (densityW - densityN) * gravity_.two_norm()))
                    + (gasPlumeDist - minGasPlumeDist) * porosity *
                    (resSatN * densityN * X_g_gComp + (1 - resSatN) * densityW * X_l_gComp)
                    - totalGasConcentration * aquiferHeight;

                    if (fabs(residual) < 1e-10)
                    {
                        break;
                    }

                    const Scalar derivation =
                    - porosity * densityN * X_g_gComp
                    + porosity * (densityW * X_l_gComp - densityN * X_g_gComp) *
                    (std::pow(((aquiferHeight - gasPlumeDist) * (densityW - densityN) * gravity_.two_norm() + entryP_), -lambda_)
                    * (resSatN + resSatW - 1.0) * std::pow(entryP_, lambda_) - resSatW)
                    + porosity * (resSatN * densityN * X_g_gComp + (1 - resSatN) * densityW * X_l_gComp);

                    gasPlumeDist = gasPlumeDist - residual / (derivation);

                    if (count == 99)
                        std::cout
                        << "Iterating gasPlumeDist not successfull, count is at maximum, exit iteration. "
                        << "Iteration crashed in following routine: "
                        << "gasPlumeDist > minGasPlumeDist, with: "
                        << "totalGasConcentration = " << totalGasConcentration
                        << ", minGasPlumeIntegral = " << minGasPlumeIntegral
                        << ", residual = " << residual
                        << ", derivation = " << derivation
                        << ", minGasPlumeDist = " << minGasPlumeDist
                        << " and gasPlumeDist: " << gasPlumeDist << "."
                        << std::endl;
                }
            }
            // calculate gasPlumeDist for gasPlumeDist < minGasPlumeDist (or unphysical minGasPlumeDist)
            // and calculate new minGasPlumeDist
            else if (totalGasConcentration * aquiferHeight > minGasPlumeIntegral + 1e-6
                || (totalGasConcentration * aquiferHeight < minGasPlumeIntegral - 1e-6 &&
                totalGasConcentration * aquiferHeight < residualIntegral))
            {
                //check if GasPlumeDist>0, GasPlumeDist=0, GasPlumeDist<0
                const Scalar fullIntegral =
                porosity * densityN * X_g_gComp * aquiferHeight
                + porosity * (densityW * X_l_gComp - densityN * X_g_gComp) *
                (1.0 / (1.0 - lambda_) * std::pow((aquiferHeight * (densityW - densityN) * gravity_.two_norm() +
                entryP_),(1.0 - lambda_))
                * (1.0 - resSatW - resSatN) * std::pow(entryP_, lambda_) / ((densityW - densityN) * gravity_.two_norm())
                + resSatW * aquiferHeight
                - entryP_ * (1.0 - resSatW - resSatN) / ((1.0 - lambda_) * (densityW - densityN) * gravity_.two_norm()));
                //GasPlumeDist>0
                if (totalGasConcentration * aquiferHeight < fullIntegral - 1e-6)
                {
                    gasPlumeDist = minGasPlumeDist; //XiStart
                    //solve equation for
                    int count = 0;
                    for (; count < 100; count++)
                    {
                        residual =
                        porosity * densityN * X_g_gComp * (aquiferHeight - gasPlumeDist)
                        + porosity * (densityW * X_l_gComp - densityN * X_g_gComp) *
                        (1.0 / (1.0 - lambda_) * std::pow(((aquiferHeight - gasPlumeDist) * (densityW - densityN) * gravity_.two_norm() +
                        entryP_),(1.0 - lambda_))
                        * (1.0 - resSatW - resSatN) * std::pow(entryP_, lambda_) / ((densityW - densityN) * gravity_.two_norm())
                        + resSatW * (aquiferHeight - gasPlumeDist)
                        - entryP_ * (1.0 - resSatW - resSatN) / ((1.0 - lambda_) * (densityW - densityN) * gravity_.two_norm()))
                        - totalGasConcentration * aquiferHeight;

                        if (fabs(residual) < 1e-12)
                        {
                            break;
                        }

                        const Scalar derivation =
                        - porosity * densityN * X_g_gComp
                        + porosity * (densityW * X_l_gComp - densityN * X_g_gComp) *
                        (std::pow(((aquiferHeight - gasPlumeDist) * (densityW - densityN) * gravity_.two_norm() + entryP_), -lambda_)
                        * (- 1.0 + resSatW + resSatN) * std::pow(entryP_, lambda_) - resSatW);

                        gasPlumeDist = gasPlumeDist - residual / (derivation);

                        if (count == 99)
                            std::cout << "Iterating gasPlumeDist not successfull, count is at maximum, exit iteration. "
                            << "Iteration crashed in following routine: "
                            << "gasPlumeDist < minGasPlumeDist, GasPlumeDist > 0, with: "
                            << "totalGasConcentration = " << totalGasConcentration
                            << ", minGasPlumeIntegral = " << minGasPlumeIntegral
                            << ", fullIntegral = " << fullIntegral
                            << ", residual = " << residual
                            << ", derivation = " << derivation
                            << ", minGasPlumeDist = " << minGasPlumeDist
                            << " and gasPlumeDist: " << gasPlumeDist << "."
                            << std::endl;
                    }
                }
                //GasPlumeDist<0
                else if (totalGasConcentration * aquiferHeight > fullIntegral + 1e-6)
                {
                    gasPlumeDist = 0.0; //XiStart
                    //solve equation
                    int count = 0;
                    for (; count < 100; count++)
                    {
                        residual =
                        porosity * densityN * X_g_gComp * aquiferHeight
                        + porosity * (densityW * X_l_gComp - densityN * X_g_gComp) *
                        (1.0 / (1.0 - lambda_) * std::pow(((aquiferHeight - gasPlumeDist) * (densityW - densityN) * gravity_.two_norm() + entryP_), (1.0 - lambda_))
                        * (1.0 - resSatW - resSatN)* std::pow(entryP_, lambda_) / ((densityW - densityN) * gravity_.two_norm())
                        + resSatW * aquiferHeight
                        - 1.0 / (1.0 - lambda_) * std::pow((entryP_ - gasPlumeDist * (densityW - densityN)
                        * gravity_.two_norm()),(1.0 - lambda_)) * (1.0 - resSatW - resSatN) * std::pow(entryP_, lambda_)
                        / ((densityW - densityN) * gravity_.two_norm()))
                        - totalGasConcentration * aquiferHeight;

                        if (fabs(residual) < 1e-10)
                        {
                            break;
                        }

                        const Scalar derivation =
                        porosity * (densityW * X_l_gComp - densityN * X_g_gComp) *
                        (std::pow(((aquiferHeight - gasPlumeDist) * (densityW - densityN) * gravity_.two_norm() + entryP_), -lambda_)
                        * (- 1.0 + resSatN + resSatW) * std::pow(entryP_, lambda_)
                        + std::pow((entryP_ - gasPlumeDist * (densityW - densityN) * gravity_.two_norm()), -lambda_)
                        * (1.0 - resSatN - resSatW) * std::pow(entryP_, lambda_));

                        gasPlumeDist = gasPlumeDist - residual / (derivation);

                        if (count == 99)
                            std::cout << "Iterating gasPlumeDist not successfull, count is at maximum, exit iteration. "
                            << "Iteration crashed in following routine: "
                            << "gasPlumeDist < minGasPlumeDist, GasPlumeDist < 0, with: "
                            << "totalGasConcentration = " << totalGasConcentration
                            << ", minGasPlumeIntegral = " << minGasPlumeIntegral
                            << ", fullIntegral = " << fullIntegral
                            << ", residual = " << residual
                            << ", derivation = " << derivation
                            << ", minGasPlumeDist = " << minGasPlumeDist
                            << " and gasPlumeDist: " << gasPlumeDist << "."
                            << std::endl;
                    }
                }
                //GasPlumeDist=0
                else
                {
                    gasPlumeDist = 0.0;
                }
                minGasPlumeDist = std::max(0.0, gasPlumeDist);
            }
            //calculate gasPlumeDist for gasPlumeDist = minGasPlumeDist
            else
            {
                gasPlumeDist = minGasPlumeDist;
            }
        }
        std::vector<Scalar> distances(2);
        distances[0] = gasPlumeDist;
        distances[1] = minGasPlumeDist;
        return distances;
    }

    /*! \brief Calculates relative permeability as numerical integral of fine-scale permeabilities over z,
     * assuming gasPlumeDist is known
     *
     */
    Scalar relPermeabilityIntegral(const Scalar lowerBound,
                                          const Scalar upperBound,
                                          const Element& element,
                                          int phaseIdx) const
    {
        const int eIdxGlobal = problem_.variables().index(element);
        const CellData& cellData = problem_.variables().cellData(eIdxGlobal);
        const int veModel = cellData.veModel();
        const Scalar gasPlumeDist = cellData.gasPlumeDist();

        Scalar relPermIntegral = 0.0;

        // Attention! This introduces a hard-coded lense
//         if(lowerBound < gasPlumeDist && phaseIdx == wPhaseIdx)
//         {
//             const Scalar upperPartBound = std::min(gasPlumeDist, upperBound);
//             relPermIntegral += upperPartBound - lowerBound;
//         }
//         if(upperBound > gasPlumeDist)
//         {
            const Scalar lowerPartBound = lowerBound;
            if(veModel == sharpInterface)
            {
                //set residual saturations according to some criterium
                const Scalar resSatW = problem_.spatialParams().materialLawParams(element).swr();
                switch (phaseIdx)
                {
                    case wPhaseIdx:
                    {
                        relPermIntegral += (upperBound - lowerPartBound)
                        * MaterialLaw::krw(problem_.spatialParams().materialLawParams(element), resSatW);
                    }
                    break;
                    case nPhaseIdx:
                    {
                        relPermIntegral += (upperBound - lowerPartBound)
                        * MaterialLaw::krn(problem_.spatialParams().materialLawParams(element), resSatW);
                    }
                    break;
                }
            }
            else if(veModel == capillaryFringe) //numerical integration
            {
                const int reconstruction = getParam<int>("Grid.Reconstruction");
                const int intervalNumber = std::pow(2, reconstruction);//TODO: find better solution, too slow
                const Scalar deltaZ = (upperBound - lowerPartBound)/intervalNumber;

                const Scalar saturationStart = reconstSaturation(lowerPartBound, wPhaseIdx, element);
                const Scalar saturationEnd = reconstSaturation(upperBound, wPhaseIdx, element);
                const Scalar permeabilityLens = getParam<Scalar>("SpatialParams.PermeabilityLens");

                GlobalPosition globalPos = element.geometry().center();
                if (globalPos[0] > 30 && globalPos[0] < 40)//lense1
                {
                    Scalar permeabilityStart = 2.24e-14;
                    Scalar permeabilityEnd = 2.24e-14;
                    switch (phaseIdx)
                    {
                        case wPhaseIdx:
                        {
                            relPermIntegral += deltaZ / 2.0 * ((MaterialLaw::krw(problem_.spatialParams().materialLawParams(element), saturationStart)
                            * permeabilityStart)
                            + (MaterialLaw::krw(problem_.spatialParams().materialLawParams(element), saturationEnd)*permeabilityEnd));
                            for (int count = 1; count < intervalNumber; count++)
                            {
                                Scalar permeability = 2.24e-14;
                                if((deltaZ * count + lowerPartBound) > 10 && (deltaZ * count + lowerPartBound) < 20)
                                    permeability = permeabilityLens;
                                Scalar saturation = reconstSaturation(deltaZ * count + lowerPartBound, wPhaseIdx, element);
                                relPermIntegral += deltaZ * MaterialLaw::krw(problem_.spatialParams().materialLawParams(element), saturation)*permeability;
                            }
                            relPermIntegral = relPermIntegral/1.49408e-14;
                            break;
                        }
                        case nPhaseIdx:
                        {
                            relPermIntegral += deltaZ / 2.0 * ((MaterialLaw::krn(problem_.spatialParams().materialLawParams(element), saturationStart)
                            * permeabilityStart)
                            + (MaterialLaw::krn(problem_.spatialParams().materialLawParams(element), saturationEnd)*permeabilityEnd));
                            for (int count = 1; count < intervalNumber; count++)
                            {
                                Scalar permeability = 2.24e-14;
                                if((deltaZ * count + lowerPartBound) > 10 && (deltaZ * count + lowerPartBound) < 20)
                                    permeability = permeabilityLens;
                                Scalar saturation = reconstSaturation(deltaZ * count + lowerPartBound, wPhaseIdx, element);
                                relPermIntegral += deltaZ * MaterialLaw::krn(problem_.spatialParams().materialLawParams(element), saturation)*permeability;
                            }
                            relPermIntegral = relPermIntegral/1.49408e-14;
                            break;
                        }
                    }
                }
                else if (globalPos[0] > 120 && globalPos[0] < 140)//lense2
                {
                    Scalar permeabilityStart = 2.24e-14;
                    Scalar permeabilityEnd = permeabilityLens;
                    switch (phaseIdx)
                    {
                        case wPhaseIdx:
                        {
                            relPermIntegral += deltaZ / 2.0 * ((MaterialLaw::krw(problem_.spatialParams().materialLawParams(element), saturationStart)
                            * permeabilityStart)
                            + (MaterialLaw::krw(problem_.spatialParams().materialLawParams(element), saturationEnd)*permeabilityEnd));
                            for (int count = 1; count < intervalNumber; count++)
                            {
                                Scalar permeability = 2.24e-14;
                                if((deltaZ * count + lowerPartBound) > 20)
                                    permeability = permeabilityLens;
                                Scalar saturation = reconstSaturation(deltaZ * count + lowerPartBound, wPhaseIdx, element);
                                relPermIntegral += deltaZ * MaterialLaw::krw(problem_.spatialParams().materialLawParams(element), saturation)*permeability;
                            }
                            relPermIntegral = relPermIntegral/1.49408e-14;
                            break;
                        }
                        case nPhaseIdx:
                        {
                            relPermIntegral += deltaZ / 2.0 * ((MaterialLaw::krn(problem_.spatialParams().materialLawParams(element), saturationStart)
                            * permeabilityStart)
                            + (MaterialLaw::krn(problem_.spatialParams().materialLawParams(element), saturationEnd)*permeabilityEnd));
                            for (int count = 1; count < intervalNumber; count++)
                            {
                                Scalar permeability = 2.24e-14;
                                if((deltaZ * count + lowerPartBound) > 20)
                                    permeability = permeabilityLens;
                                Scalar saturation = reconstSaturation(deltaZ * count + lowerPartBound, wPhaseIdx, element);
                                relPermIntegral += deltaZ * MaterialLaw::krn(problem_.spatialParams().materialLawParams(element), saturation)*permeability;
                            }
                            relPermIntegral = relPermIntegral/1.49408e-14;
                            break;
                        }
                    }
                }
                else//no lense
                {
                switch (phaseIdx)
                {
                    case wPhaseIdx:
                    {
                        relPermIntegral += deltaZ / 2.0 * (MaterialLaw::krw(problem_.spatialParams().materialLawParams(element), saturationStart)
                        + MaterialLaw::krw(problem_.spatialParams().materialLawParams(element), saturationEnd));
                        for (int count = 1; count < intervalNumber; count++)
                        {
                            Scalar saturation = reconstSaturation(deltaZ * count + lowerPartBound, wPhaseIdx, element);
                            relPermIntegral += deltaZ * MaterialLaw::krw(problem_.spatialParams().materialLawParams(element), saturation);
                        }
                        break;
                    }
                    case nPhaseIdx:
                    {
                        relPermIntegral += deltaZ / 2.0 * (MaterialLaw::krn(problem_.spatialParams().materialLawParams(element), saturationStart)
                        + MaterialLaw::krn(problem_.spatialParams().materialLawParams(element), saturationEnd));
                        for (int count = 1; count < intervalNumber; count++)
                        {
                            Scalar saturation = reconstSaturation(deltaZ * count + lowerPartBound, wPhaseIdx, element);
                            relPermIntegral += deltaZ * MaterialLaw::krn(problem_.spatialParams().materialLawParams(element), saturation);
                        }
                        break;
                    }
                }
                }
            }
//         }
        return relPermIntegral;
    }

    /*! \brief Calculates concentration integral over height given two bounds assuming gasPlumeDist is known
     */
    ComponentVector concentrationIntegral(const Scalar lowerBound,
                                          const Scalar upperBound,
                                          const Element& element) const
    {
        const int eIdxGlobal = problem_.variables().index(element);
        const CellData& cellData = problem_.variables().cellData(eIdxGlobal);

        const Scalar minGasPlumeDist = cellData.minGasPlumeDist();

        //different integration areas:
        //1. from bottom to minGasPlumeDist, 2. from minGasPlumeDist to top
        ComponentVector integral(0);

        if(lowerBound < minGasPlumeDist)
        {
            const Scalar upperPartBound = std::min(minGasPlumeDist, upperBound);
            integral[wCompIdx] += cellData.densityLiquidBelowPlume() * cellData.massFractionLiquidBelowPlume(wCompIdx)
                                  * (upperPartBound - lowerBound);
            integral[wCompIdx] -= cellData.densityLiquidBelowPlume() * cellData.massFractionLiquidBelowPlume(wCompIdx)
                                  * cellData.epsilonHeight() * (upperPartBound - lowerBound)/minGasPlumeDist;
                                  // add mass that is hidden in volume mismatch (only liquid phase below plume)
                                  // epsilonHeight is positive for negative volume mismatch
            integral[nCompIdx] += cellData.densityLiquidBelowPlume() * cellData.massFractionLiquidBelowPlume(nCompIdx)
                                  * (upperPartBound - lowerBound);
            integral[nCompIdx] -= cellData.densityLiquidBelowPlume() * cellData.massFractionLiquidBelowPlume(nCompIdx)
                                  * cellData.epsilonHeight() * (upperPartBound - lowerBound)/minGasPlumeDist;
                                  // add mass that is hidden in volume mismatch (only liquid phase below plume)
                                  // epsilonHeight is positive for negative volume mismatch
        }
        if(upperBound > minGasPlumeDist)
        {
            const Scalar lowerpartBound = std::max(minGasPlumeDist, lowerBound);
            integral[wCompIdx] += cellData.density(nPhaseIdx) * cellData.massFraction(nPhaseIdx, wCompIdx)
                                  * (upperBound - lowerpartBound);
            integral[wCompIdx] += (cellData.densityLiquidInPlume() * cellData.massFractionLiquidInPlume(wCompIdx)
                                  - cellData.density(nPhaseIdx) * cellData.massFraction(nPhaseIdx, wCompIdx))
                                  * saturationIntegral(lowerpartBound, upperBound, element);
            integral[nCompIdx] += cellData.density(nPhaseIdx) * cellData.massFraction(nPhaseIdx, nCompIdx)
                                  * (upperBound - lowerpartBound);
            integral[nCompIdx] += (cellData.densityLiquidInPlume() * cellData.massFractionLiquidInPlume(nCompIdx)
                                  - cellData.density(nPhaseIdx) * cellData.massFraction(nPhaseIdx, nCompIdx))
                                  * saturationIntegral(lowerpartBound, upperBound, element);
        }

        const Scalar porosity = problem_.spatialParams().porosity(element);
        integral *= porosity;

        return integral;
    }

    /*! \brief Calculates concentration by integrating over aquifer height given a target initial gasPlumeDist
    */
    const ComponentVector concentrationInitial(const Scalar gasPlumeDist,
                                               const PhaseVector columnRefPressures,
                                               const FluidState& fluidStatePlume,
                                               const Element& element) const
    {
        const int eIdxGlobal = problem_.variables().index(element);
        const CellData& cellData = problem_.variables().cellData(eIdxGlobal);
        const int veModel = cellData.veModel();

        const Scalar minGasPlumeDist = gasPlumeDist; // residual part could be included in initial conditions in future
        const Scalar lowerBound = 0.0; // we integrate from bottom of aquifer
        const Scalar upperBound = problem_.aquiferHeight(); // we integrate to top of aquifer

        const Scalar resSatW = problem_.spatialParams().materialLawParams(element).swr();
        const Scalar resSatN = problem_.spatialParams().materialLawParams(element).snr();

        const Scalar densityW = fluidStatePlume.density(wPhaseIdx);
        const Scalar densityN = fluidStatePlume.density(nPhaseIdx);

        //different integration areas:
        //1. from bottom to minGasPlumeDist, 2. from minGasPlumeDist to gasPlumeDist, 3. from gasPlumeDist to top
        Scalar satWIntegral = 0.0;

        if(lowerBound < minGasPlumeDist)
        {
            const Scalar upperPartBound = std::min(minGasPlumeDist, upperBound);
            satWIntegral += upperPartBound - lowerBound;
        }
        if(upperBound > minGasPlumeDist && lowerBound < gasPlumeDist )
        {
            const Scalar lowerpartBound = std::max(minGasPlumeDist, lowerBound);
            const Scalar upperPartBound = std::min(gasPlumeDist, upperBound);
            satWIntegral += (upperPartBound - lowerpartBound)*(1.0-resSatN);
        }
        if(upperBound > gasPlumeDist)
        {
            const Scalar lowerPartBound = std::max(gasPlumeDist, lowerBound);
            if(veModel == sharpInterface)
            {
                satWIntegral += (upperBound - lowerPartBound)*resSatW;
            }
            else if(veModel == capillaryFringe)
            {
                satWIntegral += (std::pow(entryP_, lambda_) * (1 - resSatW - resSatN)) /
                ((1 - lambda_) * (densityW - densityN) * problem_.gravity().two_norm()) *
                (std::pow(((upperBound - gasPlumeDist) * (densityW - densityN) *
                problem_.gravity().two_norm() + entryP_), (1 - lambda_))
                - std::pow(((lowerPartBound - gasPlumeDist) * (densityW - densityN) *
                problem_.gravity().two_norm() + entryP_), (1 - lambda_)))
                + resSatW * (upperBound - lowerPartBound);
            }
        }

        ComponentVector integral(0);
        const Scalar porosity = problem_.spatialParams().porosity(element);
        integral[wCompIdx] = porosity*satWIntegral/(upperBound-lowerBound)*densityW; // neglecting dissolution
        integral[nCompIdx] = porosity*(1-satWIntegral/(upperBound-lowerBound))*densityN; // neglecting dissolution
        return integral;
    }

    /*! \brief Calculates averaged liquid phase density given two bounds assuming gasPlumeDist is known
     */
    Scalar densityLiquidAverage(const Scalar lowerBound,
                                const Scalar upperBound,
                                const Element& element) const
    {
        const int eIdxGlobal = problem_.variables().index(element);
        const CellData& cellData = problem_.variables().cellData(eIdxGlobal);

        const Scalar minGasPlumeDist = cellData.minGasPlumeDist();

        //different integration areas:
        //1. from bottom to minGasPlumeDist, 2. from minGasPlumeDist to top
        Scalar density(0);

        if(lowerBound < minGasPlumeDist)
        {
            const Scalar upperPartBound = std::min(minGasPlumeDist, upperBound);
            density += cellData.densityLiquidBelowPlume() * (upperPartBound - lowerBound);
        }
        if(upperBound > minGasPlumeDist)
        {
            const Scalar lowerpartBound = std::max(minGasPlumeDist, lowerBound);
            density += cellData.densityLiquidInPlume() * saturationIntegral(lowerpartBound, upperBound, element);
        }

        density /= saturationIntegral(lowerBound, upperBound, element);

        return density;
    }

    /*! \brief Calculates averaged liquid mass fractions given two bounds assuming gasPlumeDist is known
     */
    ComponentVector massFractionLiquidAverage(const Scalar lowerBound,
                                                                      const Scalar upperBound,
                                                                      const Element& element) const
    {
        const int eIdxGlobal = problem_.variables().index(element);
        const CellData& cellData = problem_.variables().cellData(eIdxGlobal);

        const Scalar minGasPlumeDist = cellData.minGasPlumeDist();

        //different integration areas:
        //1. from bottom to minGasPlumeDist, 2. from minGasPlumeDist to top
        ComponentVector massFraction(0);
        Scalar denominator(0);

        if(lowerBound < minGasPlumeDist)
        {
            const Scalar upperPartBound = std::min(minGasPlumeDist, upperBound);
            massFraction[wCompIdx] += cellData.densityLiquidBelowPlume() * cellData.massFractionLiquidBelowPlume(wCompIdx)
            * (upperPartBound - lowerBound);
            denominator += cellData.densityLiquidBelowPlume() * (upperPartBound - lowerBound);
        }
        if(upperBound > minGasPlumeDist)
        {
            const Scalar lowerpartBound = std::max(minGasPlumeDist, lowerBound);
            massFraction[wCompIdx] += cellData.densityLiquidInPlume() * cellData.massFractionLiquidInPlume(wCompIdx)
            * saturationIntegral(lowerpartBound, upperBound, element);
            denominator += cellData.densityLiquidInPlume() * saturationIntegral(lowerpartBound, upperBound, element);
        }

        massFraction[wCompIdx] /= denominator;

        massFraction[nCompIdx] = 1.0 - massFraction[wCompIdx];

        return massFraction;
    }

    /*!
     * \brief Constructs a FVPressure2P2CVE object
     * \param problem a problem class object
     */
    FVPressure2P2CVE(Problem& problem) : FVPressure2P2C<TypeTag>(problem), problem_(problem), gravity_(problem.gravity())
    {
        ErrorTermFactor_ = getParam<Scalar>("Impet.ErrorTermFactor");
        ErrorTermLowerBound_ = getParam<Scalar>("Impet.ErrorTermLowerBound", 0.2);
        ErrorTermUpperBound_ = getParam<Scalar>("Impet.ErrorTermUpperBound");

        lambda_ = getParam<Scalar>("SpatialParams.Lambda");
        entryP_ = getParam<Scalar>("SpatialParams.EntryPressure");

        enableVolumeIntegral = getParam<bool>("Impet.EnableVolumeIntegral");
        regulateBoundaryPermeability = getPropValue<TypeTag, Properties::RegulateBoundaryPermeability>();
        if(regulateBoundaryPermeability)
        {
            minimalBoundaryPermeability = getParam<Scalar>("SpatialParams.MinBoundaryPermeability");
            Dune::dinfo << " Warning: Regulating Boundary Permeability requires correct subface indices on reference elements!"
                        << std::endl;
        }
    }

protected:
    Problem& problem_;
    const GlobalPosition& gravity_;
    bool enableVolumeIntegral; //!< Enables the volume integral of the pressure equation
    bool regulateBoundaryPermeability; //!< Enables regulation of permeability in the direction of a Dirichlet Boundary Condition
    Scalar minimalBoundaryPermeability; //!< Minimal limit for the boundary permeability
    Scalar ErrorTermFactor_; //!< Handling of error term: relaxation factor
    Scalar ErrorTermLowerBound_; //!< Handling of error term: lower bound for error dampening
    Scalar ErrorTermUpperBound_; //!< Handling of error term: upper bound for error dampening
    Scalar lambda_;
    Scalar entryP_;
    //! gives kind of pressure used (\f$ 0 = p_w \f$, \f$ 1 = p_n \f$, \f$ 2 = p_{global} \f$)
    static constexpr int pressureType = getPropValue<TypeTag, Properties::PressureFormulation>();
private:
    //! Returns the implementation of the problem (i.e. static polymorphism)
    Implementation &asImp_()
    {   return *static_cast<Implementation *>(this);}

    //! \copydoc IMPETProblem::asImp_()
    const Implementation &asImp_() const
    {   return *static_cast<const Implementation *>(this);}
};

/*!
 * \brief Get flux at an interface between two cells
 *
 * for first == true, the flux is calculated in traditional fractional-flow forn as in FVPressure2P.
 * for first == false, the flux thorugh \f$ \gamma \f$  is calculated via a volume balance formulation
 *  \f[ - A_{\gamma} \mathbf{n}^T_{\gamma} \mathbf{K}  \sum_{\alpha} \varrho_{\alpha} \lambda_{\alpha}
 *    \mathbf{d}_{ij}  \left( \frac{p_{\alpha,j}^t - p^{t}_{\alpha,i}}{\Delta x} + \varrho_{\alpha} \mathbf{g}^T \mathbf{d}_{ij} \right)
 *               \sum_{\kappa} X^{\kappa}_{\alpha} \frac{\partial v_{t}}{\partial C^{\kappa}}
 *   + V_i \frac{A_{\gamma}}{U_i} \mathbf{d}^T \mathbf{K} \sum_{\alpha} \varrho_{\alpha} \lambda_{\alpha}
 *    \mathbf{d}_{ij}  \left( \frac{p_{\alpha,j}^t - p^{t}_{\alpha,i}}{\Delta x} + \varrho_{\alpha} \mathbf{g}^T \mathbf{d}_{ij} \right)
 *         \sum_{\kappa} X^{\kappa}_{\alpha}
 *         \frac{\frac{\partial v_{t,j}}{\partial C^{\kappa}_j}-\frac{\partial v_{t,i}}{\partial C^{\kappa}_i}}{\Delta x} \f]
 * This includes a boundary integral and a volume integral, because
 *  \f$ \frac{\partial v_{t,i}}{\partial C^{\kappa}_i} \f$ is not constant.
 * Here, \f$ \mathbf{d}_{ij} \f$ is the normalized vector connecting the cell centers, and \f$ \mathbf{n}_{\gamma} \f$
 * represents the normal of the face \f$ \gamma \f$.
 * \param entries The Matrix and RHS entries
 * \param intersection Intersection between cell I and J
 * \param cellDataI Data of cell I
 * \param first Flag if pressure field is unknown
 */
template<class TypeTag>
void FVPressure2P2CVE<TypeTag>::getFlux(Dune::FieldVector<Scalar, 2>& entries,
                                      const Intersection& intersection,
                                      const CellData& cellDataI,
                                      const bool first)
{
    if (cellDataI.veModel() == sharpInterface || cellDataI.veModel() == capillaryFringe)
    {
        entries = 0.;
        auto elementI = intersection.inside();
        int eIdxGlobalI = problem().variables().index(elementI);

        // get global coordinate of cell center
        const GlobalPosition& globalPos = elementI.geometry().center();

        // cell volume & perimeter, assume linear map here
        Scalar volume = elementI.geometry().volume();
        Scalar perimeter = cellDataI.perimeter();
        //#warning perimeter hack 2D!
        //    perimeter = intersection.geometry().volume()*2;

        const GlobalPosition& gravity_ = problem().gravity();

        // get absolute permeability
        DimMatrix permeabilityI(problem().spatialParams().intrinsicPermeability(elementI));

        // get mobilities and fractional flow factors
        Scalar fractionalWI=0, fractionalNWI=0;
        if (first)
        {
            fractionalWI = cellDataI.mobility(wPhaseIdx)
            / (cellDataI.mobility(wPhaseIdx)+ cellDataI.mobility(nPhaseIdx));
            fractionalNWI = cellDataI.mobility(nPhaseIdx)
            / (cellDataI.mobility(wPhaseIdx)+ cellDataI.mobility(nPhaseIdx));
        }

        // get normal vector
        const GlobalPosition& unitOuterNormal = intersection.centerUnitOuterNormal();

        // get face volume
        Scalar faceArea = intersection.geometry().volume();

        // access neighbor
        auto neighbor = intersection.outside();
        int eIdxGlobalJ = problem().variables().index(neighbor);
        CellData& cellDataJ = problem().variables().cellData(eIdxGlobalJ);

        // gemotry info of neighbor
        const GlobalPosition& globalPosNeighbor = neighbor.geometry().center();

        // distance vector between barycenters
        GlobalPosition distVec = globalPosNeighbor - globalPos;

        // compute distance between cell centers
        Scalar dist = distVec.two_norm();

        GlobalPosition unitDistVec(distVec);
        unitDistVec /= dist;

        DimMatrix permeabilityJ
        = problem().spatialParams().intrinsicPermeability(neighbor);

        // compute vectorized permeabilities
        DimMatrix meanPermeability(0);
        harmonicMeanMatrix(meanPermeability, permeabilityI, permeabilityJ);

        Dune::FieldVector<Scalar, dim> permeability(0);
        meanPermeability.mv(unitDistVec, permeability);

        // get average density for gravity flux
        Scalar rhoMeanWInPlume = 0.5 * (cellDataI.densityLiquidInPlume() + cellDataJ.densityLiquidInPlume());
        Scalar rhoMeanWBelowPlume = 0.5 * (cellDataI.densityLiquidBelowPlume() + cellDataJ.densityLiquidBelowPlume());
        Scalar rhoMeanNW = 0.5 * (cellDataI.density(nPhaseIdx) + cellDataJ.density(nPhaseIdx));

        // reset potential gradients
        Scalar potentialWInPlume = 0;
        Scalar potentialWBelowPlume = 0;
        Scalar potentialNW = 0;

        if (first)     // if we are at the very first iteration we can't calculate phase potentials
        {
            // get fractional flow factors in neigbor
            Scalar fractionalWJ = cellDataJ.mobility(wPhaseIdx)
            / (cellDataJ.mobility(wPhaseIdx)+ cellDataJ.mobility(nPhaseIdx));
            Scalar fractionalNWJ = cellDataJ.mobility(nPhaseIdx)
            / (cellDataJ.mobility(wPhaseIdx)+ cellDataJ.mobility(nPhaseIdx));

            // perform central weighting
            Scalar lambda = (cellDataI.mobility(wPhaseIdx) + cellDataJ.mobility(wPhaseIdx)) * 0.5
            + (cellDataI.mobility(nPhaseIdx) + cellDataJ.mobility(nPhaseIdx)) * 0.5;

            entries[0] = fabs(lambda*faceArea*fabs(permeability*unitOuterNormal)/(dist));

            Scalar factor = (fractionalWI + fractionalWJ) * (rhoMeanWBelowPlume) * 0.5
            + (fractionalNWI + fractionalNWJ) * (rhoMeanNW) * 0.5;
            entries[1] = factor * lambda * faceArea * fabs(unitOuterNormal*permeability)
            * (gravity_ * unitDistVec);
        }
        else
        {
            // determine volume derivatives
            if (!cellDataJ.hasVolumeDerivatives())
                asImp_().volumeDerivatives(globalPosNeighbor, neighbor);

            Scalar dv_dC1 = (cellDataJ.dv(wCompIdx)
            + cellDataI.dv(wCompIdx)) / 2; // dV/dm1= dv/dC^1
            Scalar dv_dC2 = (cellDataJ.dv(nCompIdx)
            + cellDataI.dv(nCompIdx)) / 2;

            Scalar graddv_dC1 = (cellDataJ.dv(wCompIdx)
            - cellDataI.dv(wCompIdx)) / dist;
            Scalar graddv_dC2 = (cellDataJ.dv(nCompIdx)
            - cellDataI.dv(nCompIdx)) / dist;

            //                    potentialW = problem().variables().potentialWetting(eIdxGlobalI, isIndex);
            //                    potentialNW = problem().variables().potentialNonwetting(eIdxGlobalI, isIndex);
            //
            //                    densityW = (potentialW > 0.) ? densityWI : densityWJ;
            //                    densityNW = (potentialNW > 0.) ? densityNWI : densityNWJ;
            //
            //                    densityW = (potentialW == 0.) ? rhoMeanW : densityW;
            //                    densityNW = (potentialNW == 0.) ? rhoMeanNW : densityNW;
            //jochen: central weighting for gravity term
            Scalar densityWInPlume = rhoMeanWInPlume;
            Scalar densityWBelowPlume = rhoMeanWBelowPlume;
            Scalar densityNW = rhoMeanNW;

            const Scalar pressureWInPlumeI = reconstPressure(problem_.aquiferHeight(), wPhaseIdx, elementI);
            const Scalar pressureWInPlumeJ = reconstPressure(problem_.aquiferHeight(), wPhaseIdx, neighbor);

            potentialWInPlume = (pressureWInPlumeI - pressureWInPlumeJ)/dist;
            potentialWBelowPlume = (cellDataI.pressure(wPhaseIdx) - cellDataJ.pressure(wPhaseIdx))/dist;
            potentialNW = (cellDataI.pressure(nPhaseIdx) - cellDataJ.pressure(nPhaseIdx))/dist;

            potentialWInPlume += densityWInPlume * (unitDistVec * gravity_);
            potentialWBelowPlume += densityWBelowPlume * (unitDistVec * gravity_);
            potentialNW += densityNW * (unitDistVec * gravity_);

            // initialize convenience shortcuts
            Scalar lambdaWInPlume(0.), lambdaWBelowPlume(0.), lambdaN(0.);
            Scalar dV_w_inPlume(0.), dV_w_belowPlume(0.), dV_n(0.);        // dV_a = \sum_k \rho_a * dv/dC^k * X^k_a
            Scalar gV_w_inPlume(0.), gV_w_belowPlume(0.), gV_n(0.);        // multipaper eq(3.3) line 3 analogon dV_w


            //do the upwinding of the mobility depending on the phase potentials
            const CellData* upwindWCellDataInPlume(0);
            const CellData* upwindWCellDataBelowPlume(0);
            const CellData* upwindNCellData(0);
            if (potentialWInPlume > 0.)
                upwindWCellDataInPlume = &cellDataI;
            else if (potentialWInPlume < 0.)
                upwindWCellDataInPlume = &cellDataJ;
            else
            {
                if(cellDataI.isUpwindCell(intersection.indexInInside(), contiWEqIdx))
                    upwindWCellDataInPlume = &cellDataI;
                else if(cellDataJ.isUpwindCell(intersection.indexInOutside(), contiWEqIdx))
                    upwindWCellDataInPlume = &cellDataJ;
                //else
                //  upwinding is not done!
            }

            if (potentialWBelowPlume > 0.)
                upwindWCellDataBelowPlume = &cellDataI;
            else if (potentialWBelowPlume < 0.)
                upwindWCellDataBelowPlume = &cellDataJ;
            else
            {
                if(cellDataI.isUpwindCell(intersection.indexInInside(), contiWEqIdx))
                    upwindWCellDataBelowPlume = &cellDataI;
                else if(cellDataJ.isUpwindCell(intersection.indexInOutside(), contiWEqIdx))
                    upwindWCellDataBelowPlume = &cellDataJ;
                //else
                //  upwinding is not done!
            }

            if (potentialNW > 0.)
                upwindNCellData = &cellDataI;
            else if (potentialNW < 0.)
                upwindNCellData = &cellDataJ;
            else
            {
                if(cellDataI.isUpwindCell(intersection.indexInInside(), contiNEqIdx))
                    upwindNCellData = &cellDataI;
                else if(cellDataJ.isUpwindCell(intersection.indexInOutside(), contiNEqIdx))
                    upwindNCellData = &cellDataJ;
                //else
                //  upwinding is not done!
            }

            //perform upwinding if desired
            if(!upwindWCellDataInPlume or (cellDataI.wasRefined() && cellDataJ.wasRefined() && elementI.father() == neighbor.father()))
            {
                if (cellDataI.wasRefined() && cellDataJ.wasRefined())
                {
                    problem().variables().cellData(eIdxGlobalI).setUpwindCell(intersection.indexInInside(), contiWEqIdx, false);
                    cellDataJ.setUpwindCell(intersection.indexInOutside(), contiWEqIdx, false);
                }

                Scalar averagedMassFractionInPlume[2];
                averagedMassFractionInPlume[wCompIdx]
                = harmonicMean(cellDataI.massFractionLiquidInPlume(wCompIdx), cellDataJ.massFractionLiquidInPlume(wCompIdx));
                averagedMassFractionInPlume[nCompIdx]
                = harmonicMean(cellDataI.massFractionLiquidInPlume(nCompIdx), cellDataJ.massFractionLiquidInPlume(nCompIdx));
                Scalar averageDensityInPlume = harmonicMean(cellDataI.densityLiquidInPlume(), cellDataJ.densityLiquidInPlume());

                //compute means
                dV_w_inPlume = dv_dC1 * averagedMassFractionInPlume[wCompIdx] + dv_dC2 * averagedMassFractionInPlume[nCompIdx];
                dV_w_inPlume *= averageDensityInPlume;
                gV_w_inPlume = graddv_dC1 * averagedMassFractionInPlume[wCompIdx] + graddv_dC2 * averagedMassFractionInPlume[nCompIdx];
                gV_w_inPlume *= averageDensityInPlume;
                lambdaWInPlume = harmonicMean(cellDataI.mobilityInPlume(wPhaseIdx), cellDataJ.mobilityInPlume(wPhaseIdx));
            }
            else //perform upwinding
            {
                dV_w_inPlume = (dv_dC1 * upwindWCellDataInPlume->massFractionLiquidInPlume(wCompIdx)
                + dv_dC2 * upwindWCellDataInPlume->massFractionLiquidInPlume(nCompIdx));
                lambdaWInPlume = upwindWCellDataInPlume->mobilityInPlume(wPhaseIdx);
                gV_w_inPlume = (graddv_dC1 * upwindWCellDataInPlume->massFractionLiquidInPlume(wCompIdx)
                + graddv_dC2 * upwindWCellDataInPlume->massFractionLiquidInPlume(nCompIdx));
                dV_w_inPlume *= upwindWCellDataInPlume->densityLiquidInPlume();
                gV_w_inPlume *= upwindWCellDataInPlume->densityLiquidInPlume();
            }

            if (!upwindWCellDataBelowPlume or (cellDataI.wasRefined() && cellDataJ.wasRefined() && elementI.father() == neighbor.father()))
            {
                Scalar averagedMassFractionBelowPlume[2];
                averagedMassFractionBelowPlume[wCompIdx]
                = harmonicMean(cellDataI.massFraction(wPhaseIdx, wCompIdx), cellDataJ.massFraction(wPhaseIdx, wCompIdx));
                averagedMassFractionBelowPlume[nCompIdx]
                = harmonicMean(cellDataI.massFraction(wPhaseIdx, nCompIdx), cellDataJ.massFraction(wPhaseIdx, nCompIdx));
                Scalar averageDensityBelowPlume = harmonicMean(cellDataI.densityLiquidBelowPlume(), cellDataJ.densityLiquidBelowPlume());

                //compute means
                dV_w_belowPlume = dv_dC1 * averagedMassFractionBelowPlume[wCompIdx] + dv_dC2 * averagedMassFractionBelowPlume[nCompIdx];
                dV_w_belowPlume *= averageDensityBelowPlume;
                gV_w_belowPlume = graddv_dC1 * averagedMassFractionBelowPlume[wCompIdx] + graddv_dC2 * averagedMassFractionBelowPlume[nCompIdx];
                gV_w_belowPlume *= averageDensityBelowPlume;
                lambdaWBelowPlume = harmonicMean(cellDataI.mobilityLiquidBelowPlume(), cellDataJ.mobilityLiquidBelowPlume());
            }
            else //perform upwinding
            {
                dV_w_belowPlume = (dv_dC1 * upwindWCellDataBelowPlume->massFractionLiquidBelowPlume(wCompIdx)
                + dv_dC2 * upwindWCellDataBelowPlume->massFractionLiquidBelowPlume(nCompIdx));
                lambdaWBelowPlume = upwindWCellDataBelowPlume->mobilityLiquidBelowPlume();
                gV_w_belowPlume = (graddv_dC1 * upwindWCellDataBelowPlume->massFractionLiquidBelowPlume(wCompIdx)
                + graddv_dC2 * upwindWCellDataBelowPlume->massFractionLiquidBelowPlume(nCompIdx));
                dV_w_belowPlume *= upwindWCellDataBelowPlume->densityLiquidBelowPlume();
                gV_w_belowPlume *= upwindWCellDataBelowPlume->densityLiquidBelowPlume();
            }

            if(!upwindNCellData or (cellDataI.wasRefined() && cellDataJ.wasRefined()))
            {
                if (cellDataI.wasRefined() && cellDataJ.wasRefined())
                {
                    problem().variables().cellData(eIdxGlobalI).setUpwindCell(intersection.indexInInside(), contiNEqIdx, false);
                    cellDataJ.setUpwindCell(intersection.indexInOutside(), contiNEqIdx, false);
                }

                Scalar averagedMassFraction[2];
                averagedMassFraction[wCompIdx]
                = harmonicMean(cellDataI.massFraction(nPhaseIdx, wCompIdx), cellDataJ.massFraction(nPhaseIdx, wCompIdx));
                averagedMassFraction[nCompIdx]
                = harmonicMean(cellDataI.massFraction(nPhaseIdx, nCompIdx), cellDataJ.massFraction(nPhaseIdx, nCompIdx));
                Scalar averageDensity = harmonicMean(cellDataI.density(nPhaseIdx), cellDataJ.density(nPhaseIdx));

                //compute means
                dV_n = dv_dC1 * averagedMassFraction[wCompIdx] + dv_dC2 * averagedMassFraction[nCompIdx];
                dV_n *= averageDensity;
                gV_n = graddv_dC1 * averagedMassFraction[wCompIdx] + graddv_dC2 * averagedMassFraction[nCompIdx];
                gV_n *= averageDensity;
                lambdaN = harmonicMean(cellDataI.mobilityInPlume(nPhaseIdx), cellDataJ.mobilityInPlume(nPhaseIdx));
            }
            else
            {
                dV_n = (dv_dC1 * upwindNCellData->massFraction(nPhaseIdx, wCompIdx)
                + dv_dC2 * upwindNCellData->massFraction(nPhaseIdx, nCompIdx));
                lambdaN = upwindNCellData->mobilityInPlume(nPhaseIdx);
                gV_n = (graddv_dC1 * upwindNCellData->massFraction(nPhaseIdx, wCompIdx)
                + graddv_dC2 * upwindNCellData->massFraction(nPhaseIdx, nCompIdx));
                dV_n *= upwindNCellData->density(nPhaseIdx);
                gV_n *= upwindNCellData->density(nPhaseIdx);
            }

            //calculate current matrix entry
            entries[matrix] = faceArea * (lambdaWInPlume * dV_w_inPlume + lambdaN * dV_n)
            * fabs((unitOuterNormal*permeability)/(dist));
            entries[matrix] += faceArea * lambdaWBelowPlume * dV_w_belowPlume
            * fabs((unitOuterNormal*permeability)/(dist));
            if(enableVolumeIntegral) {
                entries[matrix] -= volume * faceArea / perimeter * (lambdaWInPlume * gV_w_inPlume + lambdaN * gV_n)
                * ((unitDistVec*permeability)/(dist));     // = boundary integral - area integral
                entries[matrix] -= volume * faceArea / perimeter * lambdaWBelowPlume * gV_w_belowPlume
                * ((unitDistVec*permeability)/(dist));     // = boundary integral - area integral
            }

            //calculate right hand side due to gravity
            entries[rhs] = faceArea * (densityWInPlume * lambdaWInPlume * dV_w_inPlume + densityNW * lambdaN * dV_n)
            * fabs(unitOuterNormal * permeability);
            entries[rhs] += faceArea   * densityWBelowPlume * lambdaWBelowPlume * dV_w_belowPlume
            * fabs(unitOuterNormal * permeability);
            if(enableVolumeIntegral) {
                entries[rhs] -= volume * faceArea / perimeter * (densityWInPlume * lambdaWInPlume * gV_w_inPlume + densityNW * lambdaN * gV_n)
                * (unitDistVec * permeability);
                entries[rhs] -= volume * faceArea / perimeter * densityWBelowPlume * lambdaWBelowPlume * gV_w_belowPlume
                * (unitDistVec * permeability);
            }
            entries[rhs] *= (gravity_ * unitDistVec);

            // calculate right hand side due to liquid pressure reconstruction in gas plume
            entries[rhs] += faceArea * lambdaWInPlume * dV_w_inPlume * fabs((unitOuterNormal*permeability)/(dist))
            * gravity_.two_norm()
            * (((cellDataI.densityLiquidBelowPlume() - cellDataI.densityLiquidInPlume()) * cellDataI.minGasPlumeDist())
            - ((cellDataJ.densityLiquidBelowPlume() - cellDataJ.densityLiquidInPlume()) * cellDataJ.minGasPlumeDist())
            + ((cellDataI.densityLiquidInPlume() - cellDataJ.densityLiquidInPlume()) * problem_.aquiferHeight()));

            // calculate capillary pressure gradient
            Scalar pCGradient = (cellDataI.capillaryPressure() - cellDataJ.capillaryPressure()) / dist;

            // include capillary pressure fluxes
            switch (pressureType)
            {
                case pw:
                {
                    //add capillary pressure term to right hand side
                    entries[rhs] += lambdaN * dV_n * fabs(permeability * unitOuterNormal) * pCGradient * faceArea;
                    if(enableVolumeIntegral)
                        entries[rhs]-= lambdaN * gV_n * (permeability * unitDistVec) * pCGradient * volume * faceArea / perimeter;
                    break;
                }
                case pn:
                {
                    //add capillary pressure term to right hand side
                    entries[rhs] -= lambdaWInPlume * dV_w_inPlume * fabs(permeability * unitOuterNormal) * pCGradient * faceArea;
                    entries[rhs] -= lambdaWBelowPlume * dV_w_belowPlume * fabs(permeability * unitOuterNormal) * pCGradient * faceArea;
                    if(enableVolumeIntegral) {
                        entries[rhs]+= lambdaWInPlume * gV_w_inPlume * (permeability * unitDistVec) * pCGradient * volume * faceArea / perimeter;
                        entries[rhs]+= lambdaWBelowPlume * gV_w_belowPlume * (permeability * unitDistVec) * pCGradient * volume * faceArea / perimeter;
                    }
                    break;
                }
            }
        }   // end !first
    }
    else
    {
        ParentType::getFlux(entries, intersection, cellDataI, first);
    }
    return;
}

/*!
 * \brief Get flux on Boundary
 *
 * for first == true, the flux is calculated in traditional fractional-flow forn as in FVPressure2P.
 * for first == false, the flux thorugh \f$ \gamma \f$  is calculated via a volume balance formulation
 *  \f[ - A_{\gamma} \mathbf{n}^T_{\gamma} \mathbf{K} \sum_{\alpha} \varrho_{\alpha} \lambda_{\alpha} \mathbf{d}_{ij}
    \left( \frac{p_{\alpha,j}^t - p^{t}_{\alpha,i}}{\Delta x} + \varrho_{\alpha} \mathbf{g}^T \mathbf{d}_{ij} \right)
    \sum_{\kappa} \frac{\partial v_{t}}{\partial C^{\kappa}} X^{\kappa}_{\alpha} \;, \f]
 * where we skip the volume integral assuming  \f$ \frac{\partial v_{t,i}}{\partial C^{\kappa}_i} \f$
 * to be constant at the boundary.
 * Here, \f$ \mathbf{d}_{ij} \f$ is the normalized vector connecting the cell centers, and \f$ \mathbf{n}_{\gamma} \f$
 * represents the normal of the face \f$ \gamma \f$.
 *
 * If a Neumann BC is set, the given (mass-)flux is directly multiplied by the volume derivative and inserted.
 * \param entries The Matrix and RHS entries
 * \param intersection Intersection between cell I and J
 * \param cellDataI Data of cell I
 * \param first Flag if pressure field is unknown
 */
template<class TypeTag>
void FVPressure2P2CVE<TypeTag>::getFluxOnBoundary(Dune::FieldVector<Scalar, 2>& entries,
        const Intersection& intersection, const CellData& cellDataI, const bool first)
{
    if (cellDataI.veModel() == sharpInterface || cellDataI.veModel() == capillaryFringe)
    {
        entries = 0.;
        // get global coordinate of cell center
        auto element = intersection.inside();
        const GlobalPosition& globalPos = element.geometry().center();

        // get normal vector
        const GlobalPosition& unitOuterNormal = intersection.centerUnitOuterNormal();
        // get face volume
        Scalar faceArea = intersection.geometry().volume();

        // get volume derivatives inside the cell
        Scalar dv_dC1 = cellDataI.dv(wCompIdx);
        Scalar dv_dC2 = cellDataI.dv(nCompIdx);

        // center of face in global coordinates
        const GlobalPosition& globalPosFace = intersection.geometry().center();

        // geometrical information
        GlobalPosition distVec(globalPosFace - globalPos);
        Scalar dist = distVec.two_norm();
        GlobalPosition unitDistVec(distVec);
        unitDistVec /= dist;

        //get boundary condition for boundary face center
        BoundaryTypes bcType;
        problem().boundaryTypes(bcType, intersection);

        /**********         Dirichlet Boundary        *************/
        if (bcType.isDirichlet(Indices::pressureEqIdx))
        {
            //read boundary values
            PrimaryVariables primaryVariablesOnBoundary(NAN);
            problem().dirichlet(primaryVariablesOnBoundary, intersection);

            // prepare pressure boundary condition
            PhaseVector pressBC(0.);
            Scalar pressBound = primaryVariablesOnBoundary[Indices::pressureEqIdx];
            Scalar pcBound (0.);

            // get absolute permeability
            DimMatrix permeabilityI(problem().spatialParams().intrinsicPermeability(element));

            if(regulateBoundaryPermeability)
            {
                int axis = intersection.indexInInside() / 2;
                if(permeabilityI[axis][axis] < minimalBoundaryPermeability)
                    permeabilityI[axis][axis] = minimalBoundaryPermeability;
            }

            //permeability vector at boundary
            Dune::FieldVector<Scalar, dim> permeability(0);
            permeabilityI.mv(unitDistVec, permeability);

            //pressure at boundary is defined at the cell center, but needed at bottom for ve models
            Scalar tempRef = problem_.temperatureAtPos(globalPos);
            FluidState BCfluidState;
            BCfluidState.setPressure(wPhaseIdx, problem_.referencePressureAtPos(globalPos));
            BCfluidState.setPressure(nPhaseIdx, problem_.referencePressureAtPos(globalPos));
            BCfluidState.setTemperature(tempRef);
            BCfluidState.setMassFraction(wPhaseIdx, wCompIdx, 1.);
            BCfluidState.setMassFraction(nPhaseIdx, wCompIdx, 0.);
            Scalar gravity = problem_.gravity().two_norm();
            switch (pressureType)
            {
                case pw:
                {
                    pressBound = pressBound + (FluidSystem::density(BCfluidState, wPhaseIdx) * gravity * problem_.aquiferHeight()/2.0);
                    break;
                }
                case pn:
                {
                    pressBound = pressBound + (FluidSystem::density(BCfluidState, nPhaseIdx) * gravity * problem_.aquiferHeight()/2.0);
                    break;
                }
            }

            if (first)
            {

                Scalar fractionalWI=0, fractionalNWI=0;
                fractionalWI = cellDataI.mobility(wPhaseIdx)
                / (cellDataI.mobility(wPhaseIdx)+ cellDataI.mobility(nPhaseIdx));
                fractionalNWI = cellDataI.mobility(nPhaseIdx)
                / (cellDataI.mobility(wPhaseIdx)+ cellDataI.mobility(nPhaseIdx));

                Scalar lambda = cellDataI.mobility(wPhaseIdx)+cellDataI.mobility(nPhaseIdx);
                entries[matrix] += lambda * faceArea * fabs(permeability * unitOuterNormal) / (dist);
                Scalar pressBoundary = pressBound;
                entries[rhs] += lambda * faceArea * pressBoundary * fabs(permeability * unitOuterNormal) / (dist);
                Scalar rightentry = (fractionalWI * cellDataI.density(wPhaseIdx)
                + fractionalNWI * cellDataI.density(nPhaseIdx))
                * lambda * faceArea * fabs(unitOuterNormal * permeability)
                * ( unitDistVec * gravity_);
                entries[rhs] -= rightentry;
            }
            else    //not first
            {
                //assume no gas on boundary and neglect difference above/below plume for cell inside TODO: extend
                //iterate capillary pressure and gasPlumeDist (VE models)
                unsigned int maxiter = 6;
                pcBound = cellDataI.pressure(nPhaseIdx) - cellDataI.pressure(wPhaseIdx); // initial guess for pc from cell inside
                PhaseVector referencePressurePlume;

                Scalar densityWInPlume = 0.0;
                Scalar densityNInPlume = 0.0;
                Scalar densityWBelowPlume = 0.0;
                Scalar viscosityWInPlume = 0.0;
                //start iteration loop
                for (unsigned int iter = 0; iter < maxiter; iter++)
                {
                    switch (pressureType)
                    {
                        case pw:
                        {
                            pressBC[wPhaseIdx] = pressBound;
                            pressBC[nPhaseIdx] = pressBound + pcBound;
                            break;
                        }
                        case pn:
                        {
                            pressBC[wPhaseIdx] = pressBound - pcBound;
                            pressBC[nPhaseIdx] = pressBound;
                            break;
                        }
                    }

                    //calculate new pc
                    Scalar oldPcBound = pcBound;

                    if(iter == 0)
                        referencePressurePlume = pressBC;
                    const FluidState fluidStatePlume = this->fluidStatePlume(element, referencePressurePlume);
                    const FluidState1p2c fluidStateBelowPlume = this->fluidStateBelowPlume(element, pressBC);

                    Scalar gasPlumeDist = problem_.aquiferHeight();
                    Scalar minGasPlumeDist = problem_.aquiferHeight();

                    densityWBelowPlume = fluidStateBelowPlume.density(wPhaseIdx);
                    densityWInPlume = fluidStatePlume.density(wPhaseIdx);
                    densityNInPlume = fluidStatePlume.density(nPhaseIdx);
                    viscosityWInPlume = fluidStatePlume.viscosity(wPhaseIdx);

                    if (cellDataI.veModel() == sharpInterface)
                    {
                        pcBound = densityNInPlume * gravity * gasPlumeDist - densityWBelowPlume * gravity * minGasPlumeDist
                        - densityWInPlume * gravity * (gasPlumeDist - minGasPlumeDist);
                    }
                    else if (cellDataI.veModel() == capillaryFringe)
                    {
                        pcBound = densityNInPlume * gravity * gasPlumeDist - densityWBelowPlume * gravity * minGasPlumeDist
                        - densityWInPlume * gravity * (gasPlumeDist - minGasPlumeDist) + entryP_;
                    }

                    referencePressurePlume[wPhaseIdx] = pressBC[wPhaseIdx] - densityWBelowPlume * gravity * minGasPlumeDist;
                    referencePressurePlume[nPhaseIdx] = pressBC[wPhaseIdx] + pcBound
                                                        - densityNInPlume * gravity * problem_.aquiferHeight();

                    if ((fabs(oldPcBound-pcBound)<10 || !getPropValue<TypeTag, Properties::EnableCapillarity>()) && iter != 0)
                        break;
                }

                // determine fluid properties at the boundary
                Scalar lambdaWBound = 1./viscosityWInPlume;
                Scalar lambdaNWBound = 0.;

                Scalar densityWBound = densityWBelowPlume;
                Scalar densityNWBound = densityNInPlume;

                // get average density
                Scalar rhoMeanW = 0.5 * (cellDataI.density(wPhaseIdx) + densityWBound);
                Scalar rhoMeanNW = 0.5 * (cellDataI.density(nPhaseIdx) + densityNWBound);

                Scalar potentialW = 0;
                Scalar potentialNW = 0;
                if (!first)
                {
                    //calculate potential gradient
                    potentialW = (cellDataI.pressure(wPhaseIdx) - pressBC[wPhaseIdx])/dist;
                    potentialNW = (cellDataI.pressure(nPhaseIdx) - pressBC[nPhaseIdx])/dist;

                    potentialW += rhoMeanW * (unitDistVec * gravity_);
                    potentialNW += rhoMeanNW * (unitDistVec * gravity_);
                }   //end !first

                //do the upwinding of the mobility depending on the phase potentials
                Scalar lambdaW, lambdaNW;
                Scalar densityW(0.), densityNW(0.);
                Scalar dV_w, dV_n;     // no gV, because dv/dC assumed to be constant at the boundary
                // => no area integral, only boundary integral

                if (potentialW >= 0.)
                {
                    densityW = (Dune::FloatCmp::eq<Scalar, Dune::FloatCmp::absolute>(potentialW, 0.0, 1.0e-30)) ? rhoMeanW : cellDataI.density(wPhaseIdx);
                    dV_w = (dv_dC1 * cellDataI.massFraction(wPhaseIdx, wCompIdx)
                    + dv_dC2 * cellDataI.massFraction(wPhaseIdx, nCompIdx));
                    dV_w *= densityW;
                    lambdaW = (Dune::FloatCmp::eq<Scalar, Dune::FloatCmp::absolute>(potentialW, 0.0, 1.0e-30)) ? 0.5 * (cellDataI.mobility(wPhaseIdx) + lambdaWBound)
                    : cellDataI.mobility(wPhaseIdx);
                }
                else
                {
                    densityW = densityWBound;
                    dV_w = (dv_dC1 * 1.0 /*=BCfluidState.massFraction(wPhaseIdx, wCompIdx)*/
                    + dv_dC2 * 0.0 /*=BCfluidState.massFraction(wPhaseIdx, nCompIdx)*/);
                    dV_w *= densityW;
                    lambdaW = lambdaWBound;
                }
                if (potentialNW >= 0.)
                {
                    densityNW = (Dune::FloatCmp::eq<Scalar, Dune::FloatCmp::absolute>(potentialNW, 0.0, 1.0e-30)) ? rhoMeanNW : cellDataI.density(nPhaseIdx);
                    dV_n = (dv_dC1 * cellDataI.massFraction(nPhaseIdx, wCompIdx)
                    + dv_dC2 * cellDataI.massFraction(nPhaseIdx, nCompIdx));
                    dV_n *= densityNW;
                    lambdaNW = (Dune::FloatCmp::eq<Scalar, Dune::FloatCmp::absolute>(potentialNW, 0.0, 1.0e-30)) ? 0.5 * (cellDataI.mobility(nPhaseIdx) + lambdaNWBound)
                    : cellDataI.mobility(nPhaseIdx);
                }
                else
                {
                    densityNW = densityNWBound;
                    dV_n = (dv_dC1 * 0.0 /*=BCfluidState.massFraction(nPhaseIdx, wCompIdx)*/
                    + dv_dC2 * 1.0 /*=BCfluidState.massFraction(nPhaseIdx, nCompIdx)*/);
                    dV_n *= densityNW;
                    lambdaNW = lambdaNWBound;
                }

                //calculate current matrix entry
                Scalar entry = (lambdaW * dV_w + lambdaNW * dV_n)
                * (fabs(unitOuterNormal * permeability) / dist) * faceArea;

                //calculate right hand side
                Scalar rightEntry = (lambdaW * densityW * dV_w + lambdaNW * densityNW * dV_n)
                * fabs(unitOuterNormal * permeability) * (gravity_ * unitDistVec) * faceArea ;

                // include capillary pressure fluxes
                // calculate capillary pressure gradient
                Scalar pCGradient = (cellDataI.capillaryPressure() - pcBound) / dist;
                switch (pressureType)
                {
                    case pw:
                    {
                        //add capillary pressure term to right hand side
                        rightEntry += lambdaNW * dV_n * pCGradient * fabs(unitOuterNormal * permeability) * faceArea;
                        break;
                    }
                    case pn:
                    {
                        //add capillary pressure term to right hand side
                        rightEntry -= lambdaW * dV_w * pCGradient * fabs(unitOuterNormal * permeability) * faceArea;
                        break;
                    }
                }


                // set diagonal entry and right hand side entry
                entries[matrix] += entry;
                entries[rhs] += entry * pressBound;
                entries[rhs] -= rightEntry;
            }    //end of if(first) ... else{...
        }   // end dirichlet

        /**********************************
         * set neumann boundary condition
         **********************************/
        else if(bcType.isNeumann(Indices::pressureEqIdx))
        {
            PrimaryVariables J(NAN);
            problem().neumann(J, intersection);
            if (first)
            {
                J[contiWEqIdx] /= cellDataI.density(wPhaseIdx);
                J[contiNEqIdx] /= cellDataI.density(nPhaseIdx);
            }
            else
            {
                J[contiWEqIdx] *= dv_dC1;
                J[contiNEqIdx] *= dv_dC2;
            }

            entries[rhs] -= (J[contiWEqIdx] + J[contiNEqIdx]) * faceArea;
        }
        else
            DUNE_THROW(Dune::NotImplemented, "Boundary Condition neither Dirichlet nor Neumann!");
    }
    else
    {
        ParentType::getFluxOnBoundary(entries, intersection, cellDataI, first);
    }
    return;
}

/*!
 * \brief Partial derivatives of the volumes w.r.t. changes in total concentration and pressure
 *
 * This method calculates the volume derivatives via a secant method, where the
 * secants are gained in a pre-computational step via the transport equation and
 * the last TS size.
 * The partial derivatives w.r.t. mass are defined as
 * \f$ \frac{\partial v}{\partial C^{\kappa}} = \frac{\partial V}{\partial m^{\kappa}}\f$
 *
 * \param globalPos The global position of the current element
 * \param element The current element
 */
template<class TypeTag>
void FVPressure2P2CVE<TypeTag>::volumeDerivatives(const GlobalPosition& globalPos, const Element& element)
{
    // cell index
    int eIdxGlobal = problem_.variables().index(element);

    CellData& cellData = problem_.variables().cellData(eIdxGlobal);
    int veModel = cellData.veModel();

    // get cell temperature
    Scalar temperature_ = cellData.temperature(wPhaseIdx);

    // initialize an Fluid state and a flash solver
    FluidState updFluidState;
    CompositionalFlash<Scalar, FluidSystem> flashSolver;

    /**********************************
     * a) get necessary variables
     **********************************/
    //determine phase pressures for flash calculation
    PhaseVector pressure(0.);
    for(int phaseIdx = 0; phaseIdx< numPhases; phaseIdx++)
        pressure[phaseIdx] = cellData.pressure(phaseIdx);

    PhaseVector referencePressurePlume(0.);
    referencePressurePlume[wPhaseIdx] = reconstPressure(cellData.minGasPlumeDist(), wPhaseIdx, element);
    referencePressurePlume[nPhaseIdx] = reconstPressure(problem_.aquiferHeight(), nPhaseIdx, element);

    // mass of components inside the cell
    ComponentVector mass(0.);
    for(int compIdx = 0; compIdx< numComponents; compIdx++)
        mass[compIdx] = cellData.massConcentration(compIdx);

    // actual fluid volume
    // see Fritz 2011 (Dissertation) eq.3.76
    Scalar specificVolume(0.); // = \sum_{\alpha} \nu_{\alpha} / \rho_{\alpha}
    for(int phaseIdx = 0; phaseIdx< numPhases; phaseIdx++)
        specificVolume += cellData.phaseMassFraction(phaseIdx) / cellData.density(phaseIdx);
    Scalar volalt = mass.one_norm() * specificVolume;
    //    volalt = cellData.volumeError()+problem_.spatialParams().porosity(element);
    // = \sum_{\kappa} C^{\kappa} + \sum_{\alpha} \nu_{\alpha} / \rho_{\alpha}

    /**********************************
     * b) define increments
     **********************************/
    // increments for numerical derivatives
    ComponentVector massIncrement(0.);
    for(int compIdx = 0; compIdx< numComponents; compIdx++)
    {
        massIncrement[compIdx] = this->updateEstimate_[compIdx][eIdxGlobal];
        if(fabs(massIncrement[compIdx]) < 1e-8 * cellData.density(compIdx))
            massIncrement[compIdx] = 1e-8* cellData.density(compIdx);   // as phaseIdx = compIdx
    }
    Scalar& incp = this->incp_;

    /**********************************
     * c) Secant method for derivatives
     **********************************/

    // numerical derivative of fluid volume with respect to pressure
    PhaseVector p_(incp);
    p_ += pressure;
    PhaseVector pPlume_(incp);
    pPlume_ += referencePressurePlume;

    Scalar Z1 = mass[0] / mass.one_norm();
    Scalar totalGasConcentration = mass[nCompIdx];
    if ((veModel == sharpInterface || veModel == capillaryFringe) && totalGasConcentration > 1e-12)
    {
        const FluidState fluidStatePlume = this->fluidStatePlume(element, pPlume_);
        std::vector<Scalar> distances = gasPlumeDistances(element, totalGasConcentration, fluidStatePlume);
        updFluidState.setPressure(wPhaseIdx, p_[wPhaseIdx]);
        updFluidState.setPressure(nPhaseIdx, p_[nPhaseIdx]);

        this->completeFluidState(updFluidState, element, mass, distances, fluidStatePlume);
    }
    else
    {
        flashSolver.concentrationFlash2p2c(updFluidState, Z1,
                                       p_, problem_.spatialParams().porosity(element), temperature_);
    }

    specificVolume=0.; // = \sum_{\alpha} \nu_{\alpha} / \rho_{\alpha}
    for(int phaseIdx = 0; phaseIdx< numPhases; phaseIdx++)
        specificVolume += updFluidState.phaseMassFraction(phaseIdx) / updFluidState.density(phaseIdx);
    Scalar dv_dp = ((mass.one_norm() * specificVolume) - volalt) / incp;

    if (dv_dp>0 && !Dune::FloatCmp::eq<Scalar, Dune::FloatCmp::CmpStyle::absolute>(dv_dp, 0.0, 1e-15))
    {
        // dV_dp > 0 is unphysical: Try inverse increment for secant
        Dune::dinfo << "dv_dp larger 0 at Idx " << eIdxGlobal << " , try and invert secant"<< std::endl;

        p_ -= 2*incp;
        pPlume_ -= 2*incp;
        if ((veModel == sharpInterface || veModel == capillaryFringe) && totalGasConcentration > 1e-12)
        {
            const FluidState fluidStatePlume = this->fluidStatePlume(element, pPlume_);
            std::vector<Scalar> distances = gasPlumeDistances(element, totalGasConcentration, fluidStatePlume);
            updFluidState.setPressure(wPhaseIdx, p_[wPhaseIdx]);
            updFluidState.setPressure(nPhaseIdx, p_[nPhaseIdx]);

            this->completeFluidState(updFluidState, element, mass, distances, fluidStatePlume);
        }
        else
        {
            flashSolver.concentrationFlash2p2c(updFluidState, Z1,
                                               p_, problem_.spatialParams().porosity(element), temperature_);
        }

        specificVolume=0.; // = \sum_{\alpha} \nu_{\alpha} / \rho_{\alpha}
        for(int phaseIdx = 0; phaseIdx< numPhases; phaseIdx++)
            specificVolume += updFluidState.phaseMassFraction(phaseIdx) / updFluidState.density(phaseIdx);
        dv_dp = ((mass.one_norm() * specificVolume) - volalt) /incp;

        // dV_dp > 0 is unphysical: Try inverse increment for secant
        if (dv_dp>0)
        {
            Dune::dwarn << "dv_dp still larger 0 after inverting secant at idx"<< eIdxGlobal<< std::endl;
            p_ += 2*incp;
            pPlume_ += 2*incp;
            if ((veModel == sharpInterface || veModel == capillaryFringe) && totalGasConcentration > 1e-12)
            {
                const FluidState fluidStatePlume = this->fluidStatePlume(element, pPlume_);
                std::vector<Scalar> distances = gasPlumeDistances(element, totalGasConcentration, fluidStatePlume);
                updFluidState.setPressure(wPhaseIdx, p_[wPhaseIdx]);
                updFluidState.setPressure(nPhaseIdx, p_[nPhaseIdx]);

                this->completeFluidState(updFluidState, element, mass, distances, fluidStatePlume);
            }
            else
            {
                flashSolver.concentrationFlash2p2c(updFluidState, Z1,
                                                   p_, problem_.spatialParams().porosity(element), temperature_);
            }

            // neglect effects of phase split, only regard changes in phase densities
            specificVolume=0.; // = \sum_{\alpha} \nu_{\alpha} / \rho_{\alpha}
            for(int phaseIdx = 0; phaseIdx< numPhases; phaseIdx++)
                specificVolume += cellData.phaseMassFraction(phaseIdx) / updFluidState.density(phaseIdx);
            dv_dp = ((mass.one_norm() * specificVolume) - volalt) /incp;
            if (dv_dp>0)
            {
                std::cout << "dv_dp still larger 0 after both inverts at idx " << eIdxGlobal << std::endl;
                dv_dp = cellData.dv_dp();
            }
        }
    }
    cellData.dv_dp()=dv_dp;

    // numerical derivative of fluid volume with respect to mass of components
    for (int compIdx = 0; compIdx<numComponents; compIdx++)
    {
        mass[compIdx] +=  massIncrement[compIdx];
        Z1 = mass[0] / mass.one_norm();

        if ((veModel == sharpInterface || veModel == capillaryFringe) && mass[nCompIdx] > 1e-12)
        {
            const FluidState fluidStatePlume = this->fluidStatePlume(element, referencePressurePlume);
            std::vector<Scalar> distances = gasPlumeDistances(element, mass[nCompIdx], fluidStatePlume);
            updFluidState.setPressure(wPhaseIdx, pressure[wPhaseIdx]);
            updFluidState.setPressure(nPhaseIdx, pressure[nPhaseIdx]);

            this->completeFluidState(updFluidState, element, mass, distances, fluidStatePlume);
        }
        else
        {
            flashSolver.concentrationFlash2p2c(updFluidState, Z1,
                                               pressure, problem_.spatialParams().porosity(element), temperature_);
        }

        specificVolume=0.; // = \sum_{\alpha} \nu_{\alpha} / \rho_{\alpha}
        for(int phaseIdx = 0; phaseIdx< numPhases; phaseIdx++)
            specificVolume += updFluidState.phaseMassFraction(phaseIdx) / updFluidState.density(phaseIdx);

        cellData.dv(compIdx) = ((mass.one_norm() * specificVolume) - volalt) / massIncrement[compIdx];
        mass[compIdx] -= massIncrement[compIdx];

        //check routines if derivatives are meaningful
        using std::isnan;
        using std::isinf;
        if (isnan(cellData.dv(compIdx)) || isinf(cellData.dv(compIdx)) )
        {
            DUNE_THROW(Dune::MathError, "NAN/inf of dV_dm. If that happens in first timestep, try smaller firstDt!");
        }
    }
    cellData.volumeDerivativesAvailable(true);
}

/*!
 *  \brief initializes the fluid distribution and hereby the variables container
 *
 *  It differs from updateMaterialLaws() because there are two possible initial conditions:
 *  saturations and concentration.
 *  \param compositional flag that determines if compositional effects are regarded, i.e.
 *      a reasonable pressure field is known with which compositions can be calculated.
 */
template<class TypeTag>
void FVPressure2P2CVE<TypeTag>::initialMaterialLaws(bool compositional)
{
    // iterate through leaf grid an evaluate c0 at cell center
    for (const auto& element : elements(problem_.gridView()))
    {
        // assign an Index for convenience
        int eIdxGlobal = problem_.variables().index(element);

        // get global coordinate of cell center
        GlobalPosition globalPos = element.geometry().center();

        // get the temperature
        CellData& cellData = problem_.variables().cellData(eIdxGlobal);
        int veModel = cellData.veModel();

        // acess the fluid state and prepare for manipulation
        FluidState& fluidState = cellData.manipulateFluidState();

        typename Indices::BoundaryFormulation icFormulation;
        problem_.initialFormulation(icFormulation, element);            // get type of initial condition

        //iterate capillary pressure and saturation/concentration
        unsigned int maxiter = 6;
        Scalar pc = cellData.capillaryPressure(); //initial guess for pc
        PhaseVector pressure;
        PhaseVector referencePressurePlume;
        ComponentVector totalConcentration;
        std::vector<Scalar> distances(2);
        Scalar densityWInPlume;
        Scalar densityNInPlume;
        Scalar densityWBelowPlume;
        //start iteration loop
        for (unsigned int iter = 0; iter < maxiter; iter++)
        {
            switch (pressureType)
            {
                case pw:
                {
                    pressure[wPhaseIdx] = asImp_().pressure()[eIdxGlobal];
                    pressure[nPhaseIdx] = asImp_().pressure()[eIdxGlobal] + pc;
                    break;
                }
                case pn:
                {
                    pressure[wPhaseIdx] = asImp_().pressure()[eIdxGlobal] - pc;
                    pressure[nPhaseIdx] = asImp_().pressure()[eIdxGlobal];
                    break;
                }
            }

            if(!compositional) //means that we do the first approximate guess of pressures
            {
                // phase pressures are unknown, so start with an exemplary pressure
                Scalar exemplaryPressure = problem_.referencePressure(element);

                switch (pressureType)
                {
                    case pw:
                    {
                        pressure[wPhaseIdx] = exemplaryPressure;
                        pressure[nPhaseIdx] = exemplaryPressure + pc;
                        break;
                    }
                    case pn:
                    {
                        pressure[wPhaseIdx] = exemplaryPressure - pc;
                        pressure[nPhaseIdx] = exemplaryPressure;
                        break;
                    }
                }
            }

            //calculate new pc
            Scalar oldPc = pc;

            Scalar gasPlumeDist;
            Scalar minGasPlumeDist;

            if(iter == 0)
                referencePressurePlume = pressure;
            const FluidState fluidStatePlume = this->fluidStatePlume(element, referencePressurePlume);
            const FluidState1p2c fluidStateBelowPlume = this->fluidStateBelowPlume(element, pressure);

            densityWBelowPlume = fluidStateBelowPlume.density(wPhaseIdx);
            densityWInPlume = fluidStatePlume.density(wPhaseIdx);
            densityNInPlume = fluidStatePlume.density(nPhaseIdx);

            if (icFormulation == Indices::saturation)  // saturation initial condition
            {
                DUNE_THROW(Dune::NotImplemented, "Saturation initial condition currently not implemented for VE model");
            }
            else if (icFormulation == Indices::concentration && (veModel == sharpInterface || veModel == capillaryFringe))
                // concentration initial condition with VE gives a target gasPlumeDist
            {
                // calculate total concentrations based on target gasPlumeDist
                const Scalar gasPlumeDistTarget = problem_.initConcentration(element);
                const ComponentVector totalConcentrationInitial = concentrationInitial(gasPlumeDistTarget,
                                                                                       referencePressurePlume,
                                                                                       fluidStatePlume,
                                                                                       element);
                totalConcentration[wCompIdx] = totalConcentrationInitial[wCompIdx];
                totalConcentration[nCompIdx] = totalConcentrationInitial[nCompIdx];

                distances = gasPlumeDistances(element, totalConcentration[nPhaseIdx], fluidStatePlume);

                gasPlumeDist = distances[0];
                minGasPlumeDist = distances[1];
            }
            else // concentration initial condition without VE
            {
                Scalar Z0 = problem_.initConcentration(element);
                CompositionalFlash<Scalar, FluidSystem> flashSolver;

                flashSolver.concentrationFlash2p2c(fluidState, Z0, pressure,
                                                   problem_.spatialParams().porosity(element), problem_.temperatureAtPos(globalPos));
            }

            Scalar gravity = problem_.gravity().two_norm();

            if (veModel == sharpInterface)
            {
                pc = densityNInPlume * gravity * gasPlumeDist - densityWBelowPlume * gravity * minGasPlumeDist
                - densityWInPlume * gravity * (gasPlumeDist - minGasPlumeDist);
            }
            else if (veModel == capillaryFringe)
            {
                pc = densityNInPlume * gravity * gasPlumeDist - densityWBelowPlume * gravity * minGasPlumeDist
                - densityWInPlume * gravity * (gasPlumeDist - minGasPlumeDist) + entryP_;
            }
            else
            {
                pc = MaterialLaw::pc(problem_.spatialParams().materialLawParams(element),
                                     fluidState.saturation(wPhaseIdx));
            }

            referencePressurePlume[wPhaseIdx] = pressure[wPhaseIdx] - densityWBelowPlume * gravity * minGasPlumeDist;
            referencePressurePlume[nPhaseIdx] = pressure[wPhaseIdx] + pc - densityNInPlume * gravity * problem_.aquiferHeight();

            if ((fabs(oldPc-pc)<10 || !getPropValue<TypeTag, Properties::EnableCapillarity>()) && iter != 0)
                break;
        }

        //set pressures
        switch (pressureType)
        {
            case pw:
            {
                fluidState.setPressure(wPhaseIdx, pressure[wPhaseIdx]);
                fluidState.setPressure(nPhaseIdx, pressure[wPhaseIdx] + pc);
                break;
            }
            case pn:
            {
                fluidState.setPressure(wPhaseIdx, pressure[nPhaseIdx] - pc);
                fluidState.setPressure(nPhaseIdx, pressure[nPhaseIdx]);
                break;
            }
        }

        if(veModel == sharpInterface || veModel == capillaryFringe)
        {
            // set gasPlumeDistances and averaged secondary variables for VE models (for non-VE models set in flash)
            const FluidState fluidStatePlume = this->fluidStatePlume(element, referencePressurePlume);
            distances = gasPlumeDistances(element, totalConcentration[nPhaseIdx], fluidStatePlume);
            cellData.setGasPlumeDist(distances[0]);
            cellData.setMinGasPlumeDist(distances[1]);
            this->completeFluidState(fluidState, element, totalConcentration, distances, fluidStatePlume);

            // set mobilities
            cellData.setMobility(wPhaseIdx, relPermeabilityIntegral(0, problem_.aquiferHeight(), element, wPhaseIdx)
            / problem_.aquiferHeight() / cellData.viscosity(wPhaseIdx));
            cellData.setMobility(nPhaseIdx, relPermeabilityIntegral(0, problem_.aquiferHeight(), element, nPhaseIdx)
            / problem_.aquiferHeight() / cellData.viscosity(nPhaseIdx));

            cellData.setMobilityInPlume(wPhaseIdx, relPermeabilityIntegral(distances[1], problem_.aquiferHeight(), element, wPhaseIdx)
            / problem_.aquiferHeight() / cellData.viscosityInPlume(wPhaseIdx));
            cellData.setMobilityInPlume(nPhaseIdx, relPermeabilityIntegral(distances[1], problem_.aquiferHeight(), element, nPhaseIdx)
            / problem_.aquiferHeight() / cellData.viscosityInPlume(nPhaseIdx));

            cellData.setMobilityLiquidBelowPlume(relPermeabilityIntegral(0, distances[1], element, wPhaseIdx)
            / problem_.aquiferHeight() / cellData.viscosity(wPhaseIdx));
        }
        else
        {
            cellData.setMobility(wPhaseIdx, MaterialLaw::krw(problem_.spatialParams().materialLawParams(element),
                                                             fluidState.saturation(wPhaseIdx))
            / cellData.viscosity(wPhaseIdx));
            cellData.setMobility(nPhaseIdx, MaterialLaw::krn(problem_.spatialParams().materialLawParams(element),
                                                             fluidState.saturation(wPhaseIdx))
            / cellData.viscosity(nPhaseIdx));
        }

        cellData.calculateMassConcentration(problem_.spatialParams().porosity(element));

        problem_.transportModel().totalConcentration(wCompIdx,eIdxGlobal) = cellData.massConcentration(wCompIdx);
        problem_.transportModel().totalConcentration(nCompIdx,eIdxGlobal) = cellData.massConcentration(nCompIdx);

        // calculate perimeter used as weighting factor
        if(!compositional)
        {
            // run through all intersections with neighbors
            for (const auto& intersection : intersections(problem_.gridView(), element))
            {
                cellData.perimeter()
                += intersection.geometry().volume();
            }
            cellData.globalIdx() = eIdxGlobal;

            // set dv to zero to prevent output errors
            cellData.dv_dp() = 0.;
            cellData.dv(wPhaseIdx) = 0.;
            cellData.dv(nPhaseIdx) = 0.;
        }
        // all
        cellData.reset();
    }
    return;
}

/*!
 * \brief Updates secondary variables of one cell
 *
 * For each element, the secondary variables are updated according to the
 * primary variables. In case the method is called after the Transport,
 * i.e. at the end / post time step, CellData2p2c.reset() resets the volume
 * derivatives for the next time step.
 * \param element The element
 * \param postTimeStep Flag indicating if we have just completed a time step
 */
template<class TypeTag>
void FVPressure2P2CVE<TypeTag>::updateMaterialLawsInElement(const Element& element, bool postTimeStep)
{
    // get global coordinate of cell center
    GlobalPosition globalPos = element.geometry().center();

    // cell Index and cell data
    int eIdxGlobal = problem().variables().index(element);
    CellData& cellData = problem().variables().cellData(eIdxGlobal);
    int veModel = cellData.veModel();

    // acess the fluid state and prepare for manipulation
    FluidState& fluidState = cellData.manipulateFluidState();

    Scalar temperature = problem().temperatureAtPos(globalPos);

    // reset to calculate new timeStep if we are at the end of a time step
    if(postTimeStep)
        cellData.reset();

    // get the overall mass of component 1 Z1 = C^k / (C^1+C^2) [-]
    Scalar Z1 = cellData.massConcentration(wCompIdx)
            / (cellData.massConcentration(wCompIdx)
                    + cellData.massConcentration(nCompIdx));

    // make sure only physical quantities enter flash calculation
    if(Z1 < 0. || Z1 > 1.)
    {
        Dune::dgrave << "Feed mass fraction unphysical: Z1 = " << Z1
               << " at global Idx " << eIdxGlobal
               << " , because totalConcentration(wCompIdx) = "
               << cellData.totalConcentration(wCompIdx)
               << " and totalConcentration(nCompIdx) = "
               << cellData.totalConcentration(nCompIdx)<< std::endl;
        if(Z1 < 0.)
            {
            Z1 = 0.;
            cellData.setTotalConcentration(wCompIdx, 0.);
            problem().transportModel().totalConcentration(wCompIdx, eIdxGlobal) = 0.;
            Dune::dgrave << "Regularize totalConcentration(wCompIdx) = "
                << cellData.totalConcentration(wCompIdx)<< std::endl;
            }
        else
            {
            Z1 = 1.;
            cellData.setTotalConcentration(nCompIdx, 0.);
            problem().transportModel().totalConcentration(nCompIdx,eIdxGlobal) = 0.;
            Dune::dgrave << "Regularize totalConcentration(eIdxGlobal, nCompIdx) = "
                << cellData.totalConcentration(nCompIdx)<< std::endl;
            }
    }

    //iterate capillary pressure and saturation (no-VE model) or gasPlumeDist (VE models)
    unsigned int maxiter = 6;
    Scalar pc = cellData.capillaryPressure(); //initial guess for pc from last TS
    PhaseVector pressure;
    PhaseVector referencePressurePlume;
    referencePressurePlume[wPhaseIdx] = reconstPressure(cellData.minGasPlumeDist(), wPhaseIdx, element);
    referencePressurePlume[nPhaseIdx] = reconstPressure(problem_.aquiferHeight(), nPhaseIdx, element);
    PhaseVector totalConcentration;
    for(int compIdx = 0; compIdx< numComponents; compIdx++)
        totalConcentration[compIdx] = cellData.massConcentration(compIdx);
    std::vector<Scalar> distances(2);
    Scalar densityWInPlume = 0.0;
    Scalar densityNInPlume = 0.0;
    Scalar densityWBelowPlume = 0.0;
    //start iteration loop
    for (unsigned int iter = 0; iter < maxiter; iter++)
    {
        switch (pressureType)
        {
            case pw:
            {
                pressure[wPhaseIdx] = asImp_().pressure(eIdxGlobal);
                pressure[nPhaseIdx] = asImp_().pressure(eIdxGlobal) + pc;
                break;
            }
            case pn:
            {
                pressure[wPhaseIdx] = asImp_().pressure(eIdxGlobal) - pc;
                pressure[nPhaseIdx] = asImp_().pressure(eIdxGlobal);
                break;
            }
        }

        //calculate new pc
        Scalar oldPc = pc;
        if (veModel == sharpInterface || veModel == capillaryFringe)
        {
            const FluidState fluidStatePlume = this->fluidStatePlume(element, referencePressurePlume);
            const FluidState1p2c fluidStateBelowPlume = this->fluidStateBelowPlume(element, pressure);
            distances = gasPlumeDistances(element, totalConcentration[nCompIdx], fluidStatePlume);

            Scalar gasPlumeDist = distances[0];
            Scalar minGasPlumeDist = distances[1];

            densityWBelowPlume = fluidStateBelowPlume.density(wPhaseIdx);
            densityWInPlume = fluidStatePlume.density(wPhaseIdx);
            densityNInPlume = fluidStatePlume.density(nPhaseIdx);
            Scalar gravity = problem_.gravity().two_norm();

            if (veModel == sharpInterface)
            {
                pc = densityNInPlume * gravity * gasPlumeDist - densityWBelowPlume * gravity * minGasPlumeDist
                - densityWInPlume * gravity * (gasPlumeDist - minGasPlumeDist);
            }
            else if (veModel == capillaryFringe)
            {
                pc = densityNInPlume * gravity * gasPlumeDist - densityWBelowPlume * gravity * minGasPlumeDist
                - densityWInPlume * gravity * (gasPlumeDist - minGasPlumeDist) + entryP_;
            }

            referencePressurePlume[wPhaseIdx] = pressure[wPhaseIdx] - densityWBelowPlume * gravity * minGasPlumeDist;
            referencePressurePlume[nPhaseIdx] = pressure[wPhaseIdx] + pc - densityNInPlume * gravity * problem_.aquiferHeight();
        }
        else
        {
            CompositionalFlash<Scalar, FluidSystem> flashSolver;
            flashSolver.concentrationFlash2p2c(fluidState, Z1, pressure, problem().spatialParams().porosity(element), temperature);

            pc = MaterialLaw::pc(problem().spatialParams().materialLawParams(element), fluidState.saturation(wPhaseIdx));
        }

        if ((fabs(oldPc-pc)<10 || !getPropValue<TypeTag, Properties::EnableCapillarity>()) && iter != 0)
            break;
    }

    //set pressures
    switch (pressureType)
    {
        case pw:
        {
            fluidState.setPressure(wPhaseIdx, pressure[wPhaseIdx]);
            fluidState.setPressure(nPhaseIdx, pressure[wPhaseIdx] + pc);
            break;
        }
        case pn:
        {
            fluidState.setPressure(wPhaseIdx, pressure[nPhaseIdx] - pc);
            fluidState.setPressure(nPhaseIdx, pressure[nPhaseIdx]);
            break;
        }
    }

    // set gasPlumeDistances and averaged secondary variables for VE models (for non-VE models set in flash)
    if (veModel == sharpInterface || veModel == capillaryFringe)
    {
        const FluidState fluidStatePlume = this->fluidStatePlume(element, referencePressurePlume);
        distances = gasPlumeDistances(element, totalConcentration[nCompIdx], fluidStatePlume);
        cellData.setGasPlumeDist(distances[0]);
        cellData.setMinGasPlumeDist(distances[1]);
        this->completeFluidState(fluidState, element, totalConcentration, distances, fluidStatePlume);
    }

    // set mobilities
    if (veModel == sharpInterface || veModel == capillaryFringe)
    {
        cellData.setMobility(wPhaseIdx, relPermeabilityIntegral(0, problem_.aquiferHeight(), element, wPhaseIdx)
        / problem_.aquiferHeight() / cellData.viscosity(wPhaseIdx));
        cellData.setMobility(nPhaseIdx, relPermeabilityIntegral(0, problem_.aquiferHeight(), element, nPhaseIdx)
        / problem_.aquiferHeight() / cellData.viscosity(nPhaseIdx));

        cellData.setMobilityInPlume(wPhaseIdx, relPermeabilityIntegral(distances[1], problem_.aquiferHeight(), element, wPhaseIdx)
        / problem_.aquiferHeight() / cellData.viscosityInPlume(wPhaseIdx));
        cellData.setMobilityInPlume(nPhaseIdx, relPermeabilityIntegral(distances[1], problem_.aquiferHeight(), element, nPhaseIdx)
        / problem_.aquiferHeight() / cellData.viscosityInPlume(nPhaseIdx));

        cellData.setMobilityLiquidBelowPlume(relPermeabilityIntegral(0, distances[1], element, wPhaseIdx)
        / problem_.aquiferHeight() / cellData.viscosity(wPhaseIdx));
    }
    else
    {
        cellData.setMobility(wPhaseIdx, MaterialLaw::krw(problem().spatialParams().materialLawParams(element),
                                                         fluidState.saturation(wPhaseIdx))
        / cellData.viscosity(wPhaseIdx));
        cellData.setMobility(nPhaseIdx, MaterialLaw::krn(problem().spatialParams().materialLawParams(element),
                                                         fluidState.saturation(wPhaseIdx))
        / cellData.viscosity(nPhaseIdx));
    }

    // determine volume mismatch between actual fluid volume and pore volume
    Scalar sumConc = (cellData.totalConcentration(wCompIdx) + cellData.totalConcentration(nCompIdx));
    Scalar massw = sumConc * fluidState.phaseMassFraction(wPhaseIdx);
    Scalar massn = sumConc * fluidState.phaseMassFraction(nPhaseIdx);

    if (Dune::FloatCmp::eq<Scalar>((cellData.density(wPhaseIdx)*cellData.density(nPhaseIdx)), 0))
        DUNE_THROW(Dune::MathError, "Sequential2p2c::postProcessUpdate: try to divide by 0 density");
    Scalar vol = massw / cellData.density(wPhaseIdx) + massn / cellData.density(nPhaseIdx);
    if (Dune::FloatCmp::ne<Scalar, Dune::FloatCmp::absolute>(problem().timeManager().timeStepSize(), 0.0, 1.0e-30))
    {
        cellData.volumeError()=(vol - problem().spatialParams().porosity(element));

        using std::isnan;
        if (isnan(cellData.volumeError()))
        {
            DUNE_THROW(Dune::MathError, "Sequential2p2c::postProcessUpdate:\n"
                    << "volErr[" << eIdxGlobal << "] isnan: vol = " << vol
                    << ", massw = " << massw << ", rho_l = " << cellData.density(wPhaseIdx)
                    << ", massn = " << massn << ", rho_g = " << cellData.density(nPhaseIdx)
                    << ", poro = " << problem().spatialParams().porosity(element)
                    << ", dt = " << problem().timeManager().timeStepSize());
        }
    }
    else
        cellData.volumeError()=0.;
    return;
}

}//end namespace Dumux
#endif
