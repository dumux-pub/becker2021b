# dune-common
# releases/2.7 # aa689abba532f40db8f5663fa379ea77211c1953 # 2020-11-10 13:36:21 +0000 # Christian Engwer
git clone https://gitlab.dune-project.org/core/dune-common.git
cd dune-common
git checkout releases/2.7
git reset --hard aa689abba532f40db8f5663fa379ea77211c1953
cd ..

# dune-istl
# releases/2.7 # 375e9b4b0d14961d496588b98468083c82d50fe8 # 2020-08-11 10:57:38 +0000 # René Heß
git clone https://gitlab.dune-project.org/core/dune-istl.git
cd dune-istl
git checkout releases/2.7
git reset --hard 375e9b4b0d14961d496588b98468083c82d50fe8
cd ..

# dune-geometry
# releases/2.7 # 2a66558feebde80a5454845495f21a9643b3c76b # 2020-08-11 11:45:36 +0000 # René Heß
git clone https://gitlab.dune-project.org/core/dune-geometry.git
cd dune-geometry
git checkout releases/2.7
git reset --hard 2a66558feebde80a5454845495f21a9643b3c76b
cd ..

# dune-uggrid
# releases/2.7 # d214484ccef1a474fa283ad8f9c0e39873e5f34c # 2020-10-29 20:25:05 +0100 # Christoph Grüninger
git clone https://gitlab.dune-project.org/staging/dune-uggrid
cd dune-uggrid
git checkout releases/2.7
git reset --hard d214484ccef1a474fa283ad8f9c0e39873e5f34c
cd ..

# dune-grid
# releases/2.7 # 680d9846d07a9a0f37f05363f32cf2e4bd2abed6 # 2020-08-12 08:04:25 +0000 # René Heß
git clone https://gitlab.dune-project.org/core/dune-grid.git
cd dune-grid
git checkout releases/2.7
git reset --hard 680d9846d07a9a0f37f05363f32cf2e4bd2abed6
cd ..

# dune-localfunctions
# releases/2.7 # 9b18e6835e4e72e743910814461e5530812f22f9 # 2020-08-11 11:42:55 +0000 # René Heß
git clone https://gitlab.dune-project.org/core/dune-localfunctions.git
cd dune-localfunctions
git checkout releases/2.7
git reset --hard 9b18e6835e4e72e743910814461e5530812f22f9
cd ..

# dune-alugrid
# releases/2.7 # 178a69b69eca8bf3e31ddce0fd8c990fee4931ae # 2020-08-17 21:21:32 +0200 # Robert K
git clone https://gitlab.dune-project.org/extensions/dune-alugrid
cd dune-alugrid
git checkout releases/2.7
git reset --hard 178a69b69eca8bf3e31ddce0fd8c990fee4931ae
cd ..

# dumux
# master # 2ae441225f3b22eb2adeafaef171ccbb9c515c51 # 2020-11-16 14:55:02 +0100 # Timo Koch
git clone https://git.iws.uni-stuttgart.de/dumux-repositories/dumux.git
cd dumux
git checkout master
git reset --hard 2ae441225f3b22eb2adeafaef171ccbb9c515c51
wget -q https://git.iws.uni-stuttgart.de/dumux-pub/becker2021b/raw/master/compositionalflash.patch
git apply compositionalflash.patch
cd ..

# becker2021b
# master
git clone https://git.iws.uni-stuttgart.de/dumux-pub/becker2021b.git
cd becker2021b
git checkout master
cd ..

### Run dunecontrol
./dune-common/bin/dunecontrol --opts=./dumux/cmake.opts all
