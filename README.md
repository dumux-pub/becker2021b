## Summary

This is the DuMuX module containing the code for producing the results
submitted for:

B. Becker, B. Guo, B. Flemisch, R. Helmig<br>
An adaptive hybrid vertical equilibrium/full multidimensional model for compositional multiphase flow<br>
2021

## Content

The content of this DUNE module was extracted from the module `dumux-ve`.
In particular, the following subfolders of `dumux-ve` have been extracted:

*  `appl/energyStorage/sequential/storage2p2c/`,
*  `test/decoupled/2p2cve/storage2p2cve/`,
*  `test/modelcoupling/2p2cve2p2cfullmono/storagemultidim/`.

Additionally, all headers in `dumux-ve` that are required to build the
executables from the sources

*  `appl/energyStorage/sequential/storage2p2c/test_storage.cc`: program using a full multidimensional model for calculating reference solutions,
*  `test/decoupled/2p2cve/storage2p2cve/test_storage2p2cve.cc`: program using a full compositional vertical equilibrium model,
*  `test/modelcoupling/2p2cve2p2cfullmono/storagemultidim/test_storagemultidim.cc`: program using the multiphysics model coupling compositional vertical equilibrium and compositional full multidimensions.

For building and running the executables,
please go to the build folders corresponding to the sources listed above.

## Installation

You can build the module just like any other DUNE
module by using `dunecontrol`.
The easiest way to install this module is to create a new folder and to execute
the file [installbecker2021b.sh](https://git.iws.uni-stuttgart.de/dumux-pub/becker2021b/raw/master/installbecker2021b.sh)
in this folder.
You can copy the following to a terminal:
```bash
mkdir -p becker2021b && cd becker2021b
wget -q https://git.iws.uni-stuttgart.de/dumux-pub/becker2021b/raw/master/installbecker2021b.sh
chmod +x installbecker2021b.sh && ./installbecker2021b.sh
```
For more detailed informations on installation, have a look at the
[DuMuX installation guide](https://dumux.org/installation/)
or use the [DuMuX handbook](https://dumux.org/docs/).

## Installation with Docker 

Create a new folder in your favourite location and change into it

```bash
mkdir DUMUX
cd DUMUX
```

Download the container startup script
```bash
wget https://git.iws.uni-stuttgart.de/dumux-pub/becker2021b/-/raw/master/docker_becker2021b.sh
```
Open the Docker Container
```bash
bash docker_becker2021b.sh open
```

After the script has run successfully, you may build all executables

```bash
cd becker2021b/build-cmake
make build_tests
```

and you can run them individually. They are located in the build-cmake folder according to the following paths:

*  `appl/energyStorage/sequential/storage2p2c/`,
*  `test/decoupled/2p2cve/storage2p2cve/`,
*  `test/modelcoupling/2p2cve2p2cfullmono/storagemultidim/`.

It can be executed with an input file, e.g.

```
cd appl/energyStorage/sequential/storage2p2c
./test_storage test_storage.input
```


## Dependencies on other DUNE modules

| module | branch | commit hash |
|:-------|:-------|:------------|
| dune-common | releases/2.7 | aa689abba532f40db8f5663fa379ea77211c1953 |
| dune-istl | releases/2.7 | 375e9b4b0d14961d496588b98468083c82d50fe8 |
| dune-geometry | releases/2.7 | 2a66558feebde80a5454845495f21a9643b3c76b |
| dune-uggrid | releases/2.7 | d214484ccef1a474fa283ad8f9c0e39873e5f34c |
| dune-grid | releases/2.7 | 680d9846d07a9a0f37f05363f32cf2e4bd2abed6 |
| dune-localfunctions | releases/2.7 | 9b18e6835e4e72e743910814461e5530812f22f9 |
| dune-alugrid | releases/2.7 | 178a69b69eca8bf3e31ddce0fd8c990fee4931ae |
| dumux | master | 2ae441225f3b22eb2adeafaef171ccbb9c515c51 |

In addition, the linear solver SuperLU has to be installed.

The module has been tested successfully with GCC 10.3.0 and CMake version 3.16.3.
