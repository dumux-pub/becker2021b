// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup SequentialTwoPTwoCTests
 * \brief test problem for the sequential 2p2c model
 */
#ifndef TEST_STORAGEPROBLEM_HH
#define TEST_STORAGEPROBLEM_HH

#include <dune/grid/yaspgrid.hh>
#include <dune/grid/uggrid.hh>
#include <dumux/io/grid/gridmanager.hh>

#include <dumux/porousmediumflow/2p2c/sequential/problem.hh>
#include <dumux/porousmediumflow/2p2c/sequential/fvpressure.hh>
#include <dumux/porousmediumflow/2p2c/sequential/fvtransport.hh>

// fluid properties
#include <dumux/material/fluidsystems/h2och4.hh>
#include <dumux/material/fluidsystems/h2oh2.hh>
#include <dumux/material/fluidsystems/h2oh2tabulated.hh>
#include <dumux/material/components/h2tables.hh>
#include <dumux/material/fluidsystems/h2oair.hh>


#include "test_storagespatialparams.hh"

using namespace std;
namespace Dumux
{
template<class TypeTag>
class TestStorageProblem;

//////////
// Specify the properties
//////////
namespace Properties
{
// Create new type tags
namespace TTag {
    struct TestStorage { using InheritsFrom = std::tuple<TestStorageSpatialParams, SequentialTwoPTwoC>; };
} // end namespace TTag

// Set the grid type
template<class TypeTag>
struct Grid<TypeTag, TTag::TestStorage> { using type = Dune::UGGrid<3>; };

// Set the problem property
template<class TypeTag>
struct Problem<TypeTag, TTag::TestStorage> { using type = TestStorageProblem<TypeTag>; };

//Set the transport model
template<class TypeTag>
struct TransportModel<TypeTag, TTag::TestStorage> { using type = FVTransport2P2C<TypeTag>; };

//Set the pressure model
template<class TypeTag>
struct PressureModel<TypeTag, TTag::TestStorage> { using type = FVPressure2P2C<TypeTag>; };

// Select fluid system
template<class TypeTag>
struct FluidSystem<TypeTag, TTag::TestStorage>
{
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using type = FluidSystems::H2OH2Tabulated<Scalar, H2Tables::H2Tables>;
};

template<class TypeTag>
struct PressureFormulation<TypeTag, TTag::TestStorage> { static constexpr int value = GetPropType<TypeTag, Properties::Indices>::pressureW; };

template<class TypeTag>
struct EnableCapillarity<TypeTag, TTag::TestStorage> { static constexpr bool value = true; };
template<class TypeTag>
struct BoundaryMobility<TypeTag, TTag::TestStorage> { static constexpr int value = GetPropType<TypeTag, Properties::Indices>::satDependent; };

template<class TypeTag>
struct LinearSolver<TypeTag, TTag::TestStorage> { using type = SuperLUBackend; };
}

/*!
 * \ingroup IMPETtests
 *
 * \brief test problem for the sequential 2p2c model
 *
 * The domain is box shaped (3D). All sides are closed (Neumann 0 boundary)
 * except the top and bottom boundaries (Dirichlet). A Gas (Nitrogen)
 * is injected over a vertical well in the center of the domain.
 *
 * To run the simulation execute the following line in shell:
 * <tt>./test_dec2p2c -parameterFile ./test_dec2p2c.input</tt>
 */
template<class TypeTag>
class TestStorageProblem: public IMPETProblem2P2C<TypeTag>
{
using ParentType = IMPETProblem2P2C<TypeTag>;
using GridView = typename GetPropType<TypeTag, Properties::GridGeometry>::GridView;
using Grid = typename GridView::Grid;
using TimeManager = GetPropType<TypeTag, Properties::TimeManager>;
using Indices = typename GetPropType<TypeTag, Properties::ModelTraits>::Indices;

using FluidSystem = GetPropType<TypeTag, Properties::FluidSystem>;
using WettingPhase = typename GetPropType<TypeTag, Properties::FluidSystem>::H2O;
using NonWettingPhase = typename GetPropType<TypeTag, Properties::FluidSystem>::H2;

using CellData = GetPropType<TypeTag, Properties::CellData>;
using BoundaryTypes = GetPropType<TypeTag, Properties::SequentialBoundaryTypes>;
using PrimaryVariables = GetPropType<TypeTag, Properties::PrimaryVariables>;

using SpatialParams = GetPropType<TypeTag, Properties::SpatialParams>;
using MaterialLaw = GetPropType<TypeTag, Properties::MaterialLaw>;

enum
{
    dim = GridView::dimension, dimWorld = GridView::dimensionworld
};

enum
{
    wPhaseIdx = Indices::wPhaseIdx,
    nPhaseIdx = Indices::nPhaseIdx,
    wCompIdx = Indices::wCompIdx,
    nCompIdx = Indices::nCompIdx
};

enum
{
    numPhases = FluidSystem::numPhases,
    numComponents = FluidSystem::numComponents
};

using Scalar = GetPropType<TypeTag, Properties::Scalar>;

using Element = typename GridView::Traits::template Codim<0>::Entity;
using Intersection = typename GridView::Intersection;
using GlobalPosition = typename Element::Geometry::GlobalCoordinate;

using CellArray = std::array<unsigned int, dimWorld>;
using DimMatrix = Dune::FieldMatrix<Scalar, dim, dim>;
using PhaseVector = Dune::FieldVector<Scalar, numPhases>;
using ComponentVector = Dune::FieldVector<Scalar, numComponents>;
using FluidState = GetPropType<TypeTag, Properties::FluidState>;

public:
TestStorageProblem(TimeManager& timeManager, Grid& grid) :
ParentType(timeManager, grid)
{
    // initialize the tables of the fluid system
    FluidSystem::init(280.0, 400.0, 121, 1e5, 1000e5, 1000);

    int outputInterval = 0;
    if (hasParam("Problem.OutputInterval"))
    {
        outputInterval = getParam<int>("Problem.OutputInterval");
    }
    this->setOutputInterval(outputInterval);

    Scalar outputTimeInterval = 1e6;
    if (hasParam("Problem.OutputTimeInterval"))
    {
        outputTimeInterval = getParam<Scalar>("Problem.OutputTimeInterval");
    }
    this->setOutputTimeInterval(outputTimeInterval);

    name_ = getParam<std::string>("Problem.Name");

    // store pointer to all elements in a multimap, elements belonging to the same column
    // have the same key, starting with key = 1 for first column
    // TODO: only works for equidistant grids
    int columnIndex = 0;
    for (const auto& element : Dune::elements(this->gridView()))
    {
        // identify column number
        GlobalPosition globalPos = element.geometry().center();
        CellArray numberOfCellsX = getParam<CellArray>("Grid.Cells");
        double deltaX = this->bBoxMax()[0]/numberOfCellsX[0];

        columnIndex = round((globalPos[0] + (deltaX/2.0))/deltaX);

        mapColumns_.insert(std::make_pair(columnIndex, element));
        dummy_ = element;
    }

    //calculate segregation time
    Scalar height = this->bBoxMax()[dim-1];
    Scalar porosity = this->spatialParams().porosity(dummy_);
    GlobalPosition globalPos = dummy_.geometry().center();
    Scalar pRef = referencePressureAtPos(globalPos);
    Scalar tempRef = temperatureAtPos(globalPos);
    viscosityW_ = WettingPhase::liquidViscosity(tempRef, pRef);
    Scalar permeability = this->spatialParams().intrinsicPermeability(dummy_)[dim-1][dim-1];
    Scalar gravity = this->gravity().two_norm();
    Scalar densityW = WettingPhase::liquidDensity(tempRef, pRef);
    Scalar densityN = NonWettingPhase::gasDensity(tempRef, pRef);
    segTime_ = (height*porosity*viscosityW_)/(permeability*gravity*(densityW-densityN));
    std::cout << "segTime " << segTime_ << std::endl;

    veModel_ = getParam<int>("VE.VEModel");

    mass_ = 0.0;
    isExtracting_ = true;

    //reset all output
    system("rm *.out");
}

/*!
 * \name Problem parameters
 */
// \{

//! The problem name.
/*! This is used as a prefix for files generated by the simulation.
*/
std::string name() const
{
    return name_;
}
//!  Returns true if a restart file should be written.
/* The default behaviour is to write no restart file.
 */
bool shouldWriteRestartFile() const
{
    return false;
}

//! Returns the temperature within the domain.
/*! This problem assumes a temperature of 10 degrees Celsius.
 * \param globalPos The global Position
 */
Scalar temperatureAtPos(const GlobalPosition& globalPos) const
{
    return 326.0;
}

// \}
//! Returns the reference pressure.
 /*This pressure is used in order to calculate the material properties
 * at the beginning of the initialization routine. It should lie within
 * a reasonable pressure range for the current problem.
 * \param globalPos The global Position
 */
Scalar referencePressureAtPos(const GlobalPosition& globalPos) const
{
    return 1.7e7;
}

//! Type of boundary condition.
/*! Defines the type the boundary condition for the conservation equation,
 *  e.g. Dirichlet or Neumann. The Pressure equation is acessed via
 *  Indices::pressureEqIdx, while the transport (or mass conservation -)
 *  equations are reached with Indices::contiWEqIdx and Indices::contiNEqIdx
 *
 * \param bcTypes The boundary types for the conservation equations
 * \param globalPos The global Position
 */
void boundaryTypesAtPos(BoundaryTypes &bcTypes, const GlobalPosition& globalPos) const
{
    static const Scalar angle = M_PI*(getParam<std::vector<Scalar>>("Grid.Angular1")[1])/180.0/2.0;
    static const Scalar length = getParam<std::vector<Scalar>>("Grid.Radial0")[1]+getParam<Scalar>("Grid.WellRadius");
    static const Scalar xCoord = cos(angle)*cos(angle)*length;
    static const Scalar yCoord = sin(angle)*cos(angle)*length;
    if (globalPos[0] > xCoord - eps_ && globalPos[0] < xCoord + eps_
        && globalPos[1] > yCoord - eps_ && globalPos[1] < yCoord + eps_)
    {
        bcTypes.setAllDirichlet();
    }
    // all other boundaries
    else
    {
        bcTypes.setAllNeumann();
    }
}

//! Flag for the type of Dirichlet conditions
/*! The Dirichlet BCs can be specified by a given concentration (mass of
 * a component per total mass inside the control volume) or by means
 * of a saturation.
 *
 * \param bcFormulation The boundary formulation for the conservation equations.
 * \param intersection The intersection on the boundary.
 */
void boundaryFormulation(typename Indices::BoundaryFormulation &bcFormulation, const Intersection& intersection) const
{
    bcFormulation = Indices::concentration;
}
//! Values for dirichlet boundary condition \f$ [Pa] \f$ for pressure and \f$ \frac{mass}{totalmass} \f$ or \f$ S_{\alpha} \f$ for transport.
/*! In case of a dirichlet BC, values for all primary variables have to be set. In the sequential 2p2c model, a pressure
 * is required for the pressure equation and component fractions for the transport equations. Although one BC for the two
 * transport equations can be deduced by the other, it is seperately defined for consistency reasons with other models.
 * Depending on the boundary Formulation, either saturation or total mass fractions can be defined.
 *
 * \param bcValues Vector holding values for all equations (pressure and transport).
 * \param globalPos The global Position
 */
void dirichletAtPos(PrimaryVariables &bcValues ,const GlobalPosition& globalPos) const
{
    Scalar pRef = referencePressureAtPos(globalPos);
    Scalar temp = temperatureAtPos(globalPos);
    bcValues[Indices::pressureEqIdx] = (pRef + (this->bBoxMax()[dim-1] - globalPos[dim-1])
    * FluidSystem::H2O::liquidDensity(temp, pRef)
    * this->gravity().two_norm());

    bcValues[Indices::contiWEqIdx] = 1.;
    bcValues[Indices::contiNEqIdx] = 1.- bcValues[Indices::contiWEqIdx];
}

/*!
 * \brief Evaluate the boundary conditions for a neumann
 *        boundary segment.
 *
 * \param values The neumann values for the conservation equations [kg / (m^2 *s )]
 * \param intersection The boundary intersection
 *
 * For this method, the \a values parameter stores the mass flux
 * in normal direction of each phase. Negative values mean influx.
 */
void neumann(PrimaryVariables &neumannValues,
             const Intersection &intersection) const
{
    this->setZero(neumannValues);

    const GlobalPosition &globalPos = intersection.geometry().center();
    if (isWell(globalPos))
    {
        static const Scalar cylcesDev = getParam<double>("BoundaryConditions.CyclesDev");
        static const Scalar injectionDurationDev = getParam<double>("BoundaryConditions.InjectionDurationDev");
        static const Scalar idleDurationDev = getParam<double>("BoundaryConditions.IdleDurationDev");
        static const Scalar developmentDuration = cylcesDev*(injectionDurationDev+idleDurationDev);
        static const Scalar wellHeight = getParam<double>("BoundaryConditions.WellHeight");
        const Scalar time = this->timeManager().time() + this->timeManager().timeStepSize();
        if(time <= developmentDuration && globalPos[dim-1] > wellHeight)
        {
            const int cycleNumber = std::floor(time/(injectionDurationDev+idleDurationDev));
            const Scalar localTimeInCycle = time - cycleNumber*(injectionDurationDev+idleDurationDev);
            if(localTimeInCycle < injectionDurationDev)
                neumannValues[Indices::contiNEqIdx] = getParam<double>("BoundaryConditions.InjectionRateDev");
        }
        else if(this->timeManager().time() + this->timeManager().timeStepSize() > developmentDuration
            && globalPos[dim-1] > wellHeight)
        {
            static const Scalar injectionDurationOp = getParam<double>("BoundaryConditions.InjectionDurationOp");
            static const Scalar extractionDurationOp = getParam<double>("BoundaryConditions.ExtractionDurationOp");
            const int cycleNumberOp = std::floor((time-developmentDuration)/(injectionDurationOp+extractionDurationOp));
            const Scalar localTimeInCycle = time-developmentDuration-cycleNumberOp*(injectionDurationOp+extractionDurationOp);
            if(localTimeInCycle < extractionDurationOp)
                neumannValues[Indices::contiNEqIdx] = getParam<double>("BoundaryConditions.ExtractionRateOp");
            else
                neumannValues[Indices::contiNEqIdx] = getParam<double>("BoundaryConditions.InjectionRateOp");
        }
    }
}

//! Source of mass \f$ [\frac{kg}{m^3 \cdot s}] \f$
/*! Evaluate the source term for all phases within a given
 *  volume. The method returns the mass generated (positive) or
 *  annihilated (negative) per volume unit.
 *  Both pressure and transport module regard the neumann-bc values
 *  with the tranport (mass conservation -) equation indices. So the first entry
 *  of the vector is superflous.
 *
 * \param sourceValues Vector holding values for all equations (pressure and transport).
 * \param globalPos The global Position
 */
void sourceAtPos(PrimaryVariables &sourceValues, const GlobalPosition& globalPos) const
{
    this->setZero(sourceValues);
}

//! Flag for the type of initial conditions
/*! The problem can be initialized by a given concentration (mass of
 * a component per total mass inside the control volume) or by means
 * of a saturation.
 */
void initialFormulation(typename Indices::BoundaryFormulation &initialFormulation, const Element& element) const
{
    initialFormulation = Indices::concentration;
}

//! Sat initial condition (dimensionless)
/*! The problem is initialized with the following saturation.
 */
Scalar initConcentrationAtPos(const GlobalPosition& globalPos) const
{
    const Scalar gasPlumeDistGlobal = getParam<Scalar>("Initial.gasPlumeDistGlobal");
    if(globalPos[dim-1] > gasPlumeDistGlobal)
    {
        const Scalar lambda = 2.0; // as in spatial params
        const Scalar entryP = 1e5; // as in spatial params
        const Scalar resSatW = 0.0; // as in spatial params
        const Scalar resSatN = 0.01; // as in spatial params
        const Scalar porosity = 0.1308; // as in spatial params
        const Scalar pRef = referencePressureAtPos(globalPos);
        const Scalar temp = temperatureAtPos(globalPos);
        const Scalar densityW = FluidSystem::H2O::liquidDensity(temp, pRef);
        const Scalar densityN = FluidSystem::H2::gasDensity(temp, pRef);
        const Scalar gravity = this->gravity().two_norm();
        const Scalar satW = std::pow((globalPos[dim-1] - gasPlumeDistGlobal)*(densityW-densityN)*gravity + entryP, -lambda)
        * std::pow(entryP, lambda) * (1-resSatW-resSatN) + resSatW;
        const Scalar totalConcW = porosity*satW*densityW;
        const Scalar totalConcG = porosity*(1-satW)*densityN;
        if(totalConcW < 1e-8 || totalConcG < 1e-8)
        {
            std::cout << " totalConcW " << totalConcW << " totalConcG " << totalConcG << std::endl;
        }
        return totalConcW/(totalConcW + totalConcG);
    }
    else
        return 1;
}

void postTimeStep()
{
    ParentType::postTimeStep();

    if (getParam<bool>("Output.PlotProfiles"))
    {
        const CellArray numberOfCells = getParam<CellArray>("Grid.Cells");
        const int timeStepIdx = this->timeManager().timeStepIndex();
        bool plotThisColumn = false;
        //iterate over grid columns
        for (int columnIndex = 1; columnIndex <= numberOfCells[0]; ++columnIndex)
        {
            //only plot profiles for specific columns and time steps
            if ((columnIndex == 20 && (timeStepIdx == 799 || timeStepIdx == 1215 || this->timeManager().willBeFinished()))
                || (columnIndex == 200 && (timeStepIdx == 799 || timeStepIdx == 1215 || this->timeManager().willBeFinished())))
            {
                plotThisColumn = true;
            }

            if(plotThisColumn)
            {
                //iterate over all cells in one grid column,
                //calculate averageConcColumn, collect sat and relPerm
                Scalar volumeColumn = 0.0;
                Scalar averageTotalConcColumn = 0.0;
                PhaseVector pRefColumn;// reference pressures in column
                pRefColumn[wPhaseIdx] = 0.0;
                pRefColumn[nPhaseIdx] = 1e100;
                Scalar columnMinPressureW = 1e100;
                typename std::map<int, Element>::iterator it = mapColumns_.lower_bound(columnIndex);
                for (; it != mapColumns_.upper_bound(columnIndex); ++it)
                {
                    const GlobalPosition globalPos = (it->second).geometry().center();
                    const Scalar volume = (it->second).geometry().volume();
                    const int globalIdxI = this->variables().index(it->second);
                    const CellData& cellData = this->variables().cellData(globalIdxI);

                    Scalar positionZ = globalPos[dim - 1];
                    Scalar satW = cellData.saturation(wPhaseIdx);
                    Scalar relPermW = cellData.mobility(wPhaseIdx) * cellData.viscosity(wPhaseIdx);

                    //write data to sat-plots
                    std::ostringstream oss;
                    oss << "satProfile_" << columnIndex << "-" << timeStepIdx << ".out";
                    std::string fileName = oss.str();
                    outputFile_.open(fileName, std::ios::app);
                    outputFile_ << positionZ << " " << satW << std::endl;
                    outputFile_.close();

                    //write data to relPerm-plots
                    oss.str("");
                    oss << "relPermProfile_" << columnIndex << "-" << timeStepIdx << ".out";
                    fileName = oss.str();
                    outputFile_.open(fileName, std::ios::app);
                    outputFile_ << positionZ << " " << relPermW << std::endl;
                    outputFile_.close();

                    volumeColumn += volume;

                    averageTotalConcColumn += cellData.totalConcentration(nCompIdx) * volume;
                    if(cellData.saturation(nPhaseIdx) > 1e-8)
                        pRefColumn[wPhaseIdx] = std::max(pRefColumn[wPhaseIdx], cellData.pressure(wPhaseIdx));
                    pRefColumn[nPhaseIdx] = std::min(pRefColumn[nPhaseIdx], cellData.pressure(nPhaseIdx));
                    columnMinPressureW = std::min(columnMinPressureW, cellData.pressure(wPhaseIdx));
                }
                averageTotalConcColumn /= volumeColumn;
                if(pRefColumn[wPhaseIdx] < 1e-8)
                    pRefColumn[wPhaseIdx] = columnMinPressureW;
                Scalar gasPlumeDist = calculateGasPlumeDist(averageTotalConcColumn, pRefColumn);

                //collect data for sat and relPerm VE profiles
                static const int steps = 100;
                static const Scalar domainHeight = this->bBoxMax()[dim - 1];
                static const Scalar deltaZ = domainHeight/steps;
                for (int i = 0; i <= steps; i++)
                {
                    Scalar positionZ2 = i*deltaZ;
                    Scalar reconstSatW = reconstSaturation(positionZ2, gasPlumeDist, pRefColumn);
                    Scalar reconstRelPermW = MaterialLaw::krw(this->spatialParams().materialLawParams(dummy_), reconstSatW);

                    //write data to reconstSat-plots
                    std::ostringstream oss;
                    oss << "reconstSatProfile_" << columnIndex << "-" << timeStepIdx << ".out";
                    std::string fileName = oss.str();
                    outputFile_.open(fileName, std::ios::app);
                    outputFile_ << positionZ2 << " " << reconstSatW << std::endl;
                    outputFile_.close();

                    //write data to reconstRelPerm-plots
                    oss.str("");
                    oss << "reconstRelPermProfile_" << columnIndex << "-" << timeStepIdx << ".out";
                    fileName = oss.str();
                    outputFile_.open(fileName, std::ios::app);
                    outputFile_ << positionZ2 << " " << reconstRelPermW << std::endl;
                    outputFile_.close();
                }

                plotThisColumn = false;
            }
        }

    }

    if (getParam<bool>("Output.PlotVECriteriaForColumns"))
    {
        const CellArray numberOfCells = getParam<CellArray>("Grid.Cells");
        bool plotThisColumn = false;
        //iterate over grid columns
        for (int columnIndex = 1; columnIndex <= numberOfCells[0]; ++columnIndex)
        {
            //only plot VE criteria over time for specific columns
            if (columnIndex == 20 || columnIndex == 200)
            {
                plotThisColumn = true;
            }

            if (plotThisColumn)
            {
                //iterate over all cells in one grid column and calculate averageTotalConcColumn and refPressure
                Scalar volumeColumn = 0;
                Scalar averageTotalConcColumn = 0;
                PhaseVector pRefColumn;// reference pressures in column
                pRefColumn[wPhaseIdx] = 0.0;
                pRefColumn[nPhaseIdx] = 1e100;
                Scalar columnMinPressureW = 1e100;
                typename std::map<int, Element>::iterator it = mapColumns_.lower_bound(columnIndex);
                for (; it != mapColumns_.upper_bound(columnIndex); ++it)
                {
                    const Scalar volume = (it->second).geometry().volume();
                    const int globalIdxI = this->variables().index(it->second);
                    const CellData& cellData = this->variables().cellData(globalIdxI);

                    volumeColumn += volume;

                    averageTotalConcColumn += cellData.totalConcentration(nCompIdx) * volume;
                    if(cellData.saturation(nPhaseIdx) > 1e-8)
                        pRefColumn[wPhaseIdx] = std::max(pRefColumn[wPhaseIdx], cellData.pressure(wPhaseIdx));
                    pRefColumn[nPhaseIdx] = std::min(pRefColumn[nPhaseIdx], cellData.pressure(nPhaseIdx));
                    columnMinPressureW = std::min(columnMinPressureW, cellData.pressure(wPhaseIdx));
                }
                averageTotalConcColumn /= volumeColumn;
                if(pRefColumn[wPhaseIdx] < 1e-8)
                    pRefColumn[wPhaseIdx] = columnMinPressureW;
                Scalar gasPlumeDist = calculateGasPlumeDist(averageTotalConcColumn, pRefColumn);

                //iterate over all cells in one grid column and calculate VE criteria
                Scalar satCrit = 0;
                Scalar relPermCrit = 0;
                static const Scalar domainHeight = this->bBoxMax()[dim - 1];
                static const Scalar deltaZ = domainHeight/numberOfCells[dim-1];
                it = mapColumns_.lower_bound(columnIndex);
                for (; it != mapColumns_.upper_bound(columnIndex); ++it)
                {
                    const GlobalPosition globalPos = (it->second).geometry().center();
                    const Scalar lowerBound = globalPos[dim-1] - deltaZ/2.0;
                    const Scalar upperBound = globalPos[dim-1] + deltaZ/2.0;
                    const int globalIdxI = this->variables().index(it->second);
                    const Scalar saturationW = this->variables().cellData(globalIdxI).saturation(wPhaseIdx);
                    satCrit += calculateSatCriterionIntegral(lowerBound, upperBound, saturationW, gasPlumeDist, pRefColumn);
                    relPermCrit += calculateKrwCriterionIntegral(lowerBound, upperBound, saturationW, gasPlumeDist, pRefColumn);
                }

                satCrit /= (domainHeight-gasPlumeDist);
                relPermCrit /= (domainHeight-gasPlumeDist);

                if(gasPlumeDist > domainHeight - 1e-8)
                {
                    satCrit = 0;
                    relPermCrit = 0;
                }

                //write data to sat-criterion plots
                std::ostringstream oss;
                oss << "satCritColumn_" << columnIndex << ".out";
                std::string fileName = oss.str();
                outputFile_.open(fileName, std::ios::app);
                outputFile_ << this->timeManager().time() + this->timeManager().timeStepSize()/segTime_
                            << " " << satCrit << std::endl;
                outputFile_.close();

                //write data to relPerm-criterion plots
                oss.str("");
                oss << "relPermCritColumn_" << columnIndex << ".out";
                fileName = oss.str();
                outputFile_.open(fileName, std::ios::app);
                outputFile_ << this->timeManager().time() + this->timeManager().timeStepSize()/segTime_
                            << " " << relPermCrit << std::endl;
                outputFile_.close();
            }
            plotThisColumn = false;
        }
    }

    if (getParam<bool>("Output.PlotVECriteriaOverDomain"))
    {
        bool plotCriteria = false;
        const int timeStepIdx = this->timeManager().timeStepIndex();
        //only plot VE criteria over domain for specific timeStepIdx
        if (timeStepIdx == 799 || timeStepIdx == 1215 || this->timeManager().willBeFinished())
        {
            plotCriteria = true;
        }

        if(plotCriteria)
        {
            const CellArray numberOfCells = getParam<CellArray>("Grid.Cells");
            //iterate over grid columns
            for (int columnIndex = 1; columnIndex <= numberOfCells[0]; ++columnIndex)
            {
                //iterate over all cells in one grid column and calculate averageTotalConcColumn and refPressure
                Scalar volumeColumn = 0;
                Scalar averageTotalConcColumn = 0;
                PhaseVector pRefColumn;// reference pressures in column
                pRefColumn[wPhaseIdx] = 0.0;
                pRefColumn[nPhaseIdx] = 1e100;
                Scalar columnMinPressureW = 1e100;
                typename std::map<int, Element>::iterator it = mapColumns_.lower_bound(columnIndex);
                for (; it != mapColumns_.upper_bound(columnIndex); ++it)
                {
                    const Scalar volume = (it->second).geometry().volume();
                    const int globalIdxI = this->variables().index(it->second);
                    const CellData& cellData = this->variables().cellData(globalIdxI);

                    volumeColumn += volume;

                    averageTotalConcColumn += cellData.totalConcentration(nCompIdx) * volume;
                    if(cellData.saturation(nPhaseIdx) > 1e-8)
                        pRefColumn[wPhaseIdx] = std::max(pRefColumn[wPhaseIdx], cellData.pressure(wPhaseIdx));
                    pRefColumn[nPhaseIdx] = std::min(pRefColumn[nPhaseIdx], cellData.pressure(nPhaseIdx));
                    columnMinPressureW = std::min(columnMinPressureW, cellData.pressure(wPhaseIdx));
                }
                averageTotalConcColumn /= volumeColumn;
                if(pRefColumn[wPhaseIdx] < 1e-8)
                    pRefColumn[wPhaseIdx] = columnMinPressureW;
                Scalar gasPlumeDist = calculateGasPlumeDist(averageTotalConcColumn, pRefColumn);

                //iterate over all cells in one grid column and calculate VE criteria
                Scalar positionX;
                Scalar satCrit = 0;
                Scalar relPermCrit = 0;
                static const Scalar domainHeight = this->bBoxMax()[dim - 1];
                static const Scalar deltaZ = domainHeight/numberOfCells[dim-1];
                it = mapColumns_.lower_bound(columnIndex);
                for (; it != mapColumns_.upper_bound(columnIndex); ++it)
                {
                    const GlobalPosition globalPos = (it->second).geometry().center();
                    positionX = globalPos[0];
                    const Scalar lowerBound = globalPos[dim-1] - deltaZ/2.0;
                    const Scalar upperBound = globalPos[dim-1] + deltaZ/2.0;
                    const int globalIdxI = this->variables().index(it->second);
                    const Scalar saturationW = this->variables().cellData(globalIdxI).saturation(wPhaseIdx);
                    satCrit += calculateSatCriterionIntegral(lowerBound, upperBound, saturationW, gasPlumeDist, pRefColumn);
                    relPermCrit += calculateKrwCriterionIntegral(lowerBound, upperBound, saturationW, gasPlumeDist, pRefColumn);
                }

                satCrit /= (domainHeight-gasPlumeDist);
                relPermCrit /= (domainHeight-gasPlumeDist);

                if(gasPlumeDist > domainHeight - 1e-8)
                {
                    satCrit = 0;
                    relPermCrit = 0;
                }

                //write data to satCrit-plots
                std::ostringstream oss;
                oss << "satCrit_" << timeStepIdx << ".out";
                std::string fileName = oss.str();
                outputFile_.open(fileName, std::ios::app);
                outputFile_ << positionX << " " << satCrit << std::endl;
                outputFile_.close();

                //write data to relPermCrit-plots
                oss.str("");
                oss << "relPermCrit_" << timeStepIdx << ".out";
                fileName = oss.str();
                outputFile_.open(fileName, std::ios::app);
                outputFile_ << positionX << " " << relPermCrit << std::endl;
                outputFile_.close();
            }
        }
    }

    if (getParam<bool>("Output.PlotDissolvedMass"))
    {
        bool plotCriteria = false;
        const int timeStepIdx = this->timeManager().timeStepIndex();
        //only plot VE criteria over domain for specific timeStepIdx
        if (timeStepIdx % 10 == 0)
        {
            plotCriteria = true;
        }

        if(plotCriteria)
        {
            Scalar totalMassGas = 0.0;
            Scalar totalMassDissolvedGas = 0.0;
            Scalar totalMassDissolvedGasInPlume = 0.0;
            const CellArray numberOfCells = getParam<CellArray>("Grid.Cells");
            //iterate over grid columns
            for (int columnIndex = 1; columnIndex <= numberOfCells[0]; ++columnIndex)
            {
                //iterate over all cells in one grid column and calculate mass of dissolved gas
                typename std::map<int, Element>::iterator it = mapColumns_.lower_bound(columnIndex);
                for (; it != mapColumns_.upper_bound(columnIndex); ++it)
                {
                    const Scalar volume = (it->second).geometry().volume();
                    const int eIdxGlobal = this->variables().index(it->second);
                    CellData& cellData = this->variables().cellData(eIdxGlobal);

                    totalMassGas += volume * cellData.totalConcentration(nCompIdx);
                    totalMassDissolvedGas += cellData.saturation(wPhaseIdx) * volume
                    * this->spatialParams().porosity(it->second) * cellData.density(wPhaseIdx)
                    * cellData.massFraction(wPhaseIdx, nCompIdx);

                    //identify gas plume cells
                    if (cellData.saturation(wPhaseIdx) < 1.0 - eps_)
                    {
                        totalMassDissolvedGasInPlume += cellData.saturation(wPhaseIdx) * volume
                        * this->spatialParams().porosity(it->second) * cellData.density(wPhaseIdx)
                        * cellData.massFraction(wPhaseIdx, nCompIdx);
                    }
                }
            }

            //write data to mass-plots
            std::ostringstream oss;
            oss << "massDissolvedGas" << ".out";
            std::string fileName = oss.str();
            outputFile_.open(fileName, std::ios::app);
            outputFile_ << this->timeManager().time() + this->timeManager().timeStepSize()
                        << " " << totalMassDissolvedGas <<  " " <<  totalMassDissolvedGas/totalMassGas*100
                        << " " <<  totalMassDissolvedGasInPlume << " " << totalMassDissolvedGasInPlume/totalMassDissolvedGas*100 << std::endl;
            outputFile_.close();
        }
    }

    if (getParam<bool>("Output.CheckMassConservativity"))
    {
        //check mass conservativity:
        Scalar totalMassW = 0.0;
        Scalar totalMassN = 0.0;
        Scalar boundaryFluxW = 0.0;
        Scalar boundaryFluxN = 0.0;
        GlobalPosition globalPos = dummy_.geometry().center();
        Scalar pRef = referencePressureAtPos(globalPos);
        Scalar tempRef = temperatureAtPos(globalPos);
        Scalar porosity = this->spatialParams().porosity(dummy_);
        Scalar satW = 1.0;//Attention: change according to initial conditions
        static Scalar totalMassWExpected = satW * this->bBoxMax()[0] * this->bBoxMax()[dim - 1]
        * porosity * WettingPhase::liquidDensity(tempRef, pRef);
        static Scalar totalMassNExpected = (1.0-satW) * this->bBoxMax()[0] * this->bBoxMax()[dim - 1]
        * porosity * NonWettingPhase::gasDensity(tempRef, pRef);
        for (const auto& element : elements(this->gridView()))
        {
            int eIdxGlobal = this->variables().index(element);
            GlobalPosition globalPos = element.geometry().center();
            CellData& cellData = this->variables().cellData(eIdxGlobal);
            Scalar pRef = referencePressureAtPos(globalPos);
            Scalar temp = temperatureAtPos(globalPos);
            Scalar massW = cellData.saturation(wPhaseIdx) * element.geometry().volume()
            * this->spatialParams().porosity(element) * WettingPhase::liquidDensity(temp, pRef);
            Scalar massN = cellData.saturation(nPhaseIdx) * element.geometry().volume()
            * this->spatialParams().porosity(element) * NonWettingPhase::gasDensity(temp, pRef);
            totalMassW += massW;
            totalMassN += massN;

            // calculate flow of gas over boundary
            for (const auto& intersection : intersections(this->gridView(), element))
            {
                if (intersection.boundary())
                {
                    int isIndex = intersection.indexInInside();
                    GlobalPosition unitOuterNormal = intersection.centerUnitOuterNormal();
                    Scalar faceArea = intersection.geometry().volume();

                    boundaryFluxW -= cellData.fluxData().velocity(wPhaseIdx, isIndex)
                    * unitOuterNormal * faceArea * WettingPhase::liquidDensity(temp, pRef);
                    boundaryFluxN -= cellData.fluxData().velocity(nPhaseIdx, isIndex)
                    * unitOuterNormal * faceArea * NonWettingPhase::gasDensity(temp, pRef);

                    BoundaryTypes bcTypes;
                    this->boundaryTypes(bcTypes, intersection);
                }
            }
        }
        Scalar timeStepSize = this->timeManager().timeStepSize();
        totalMassWExpected += boundaryFluxW * timeStepSize;
        totalMassNExpected += boundaryFluxN * timeStepSize;
        std::cout << "Error in mass W: " << totalMassWExpected - totalMassW << ". ";
        std::cout << "Error in mass N: " << totalMassNExpected - totalMassN << ". ";
    }

    if (hasParam("BoundaryConditions.MassCycled"))
    {
        // calculate injected/extracted mass and operate storage after development phase
        static const Scalar cyclesDev = getParam<double>("BoundaryConditions.CyclesDev");
        static const Scalar injectionDurationDev = getParam<double>("BoundaryConditions.InjectionDurationDev");
        static const Scalar idleDurationDev = getParam<double>("BoundaryConditions.IdleDurationDev");
        static const Scalar developmentDuration = cyclesDev*(injectionDurationDev+idleDurationDev);
        if(this->timeManager().time() + this->timeManager().timeStepSize() > developmentDuration)
        {
            for (const auto& element : elements(this->gridView()))
            {
                for (const auto& intersection : intersections(this->gridView(), element))
                {
                    BoundaryTypes bcType;
                    this->boundaryTypes(bcType, intersection);
                    if (bcType.isNeumann(Indices::contiNEqIdx))
                    {
                        PrimaryVariables neumannValues;
                        this->neumann(neumannValues, intersection);
                        Scalar faceArea = intersection.geometry().volume();
                        mass_ += neumannValues[Indices::contiNEqIdx] * faceArea * this->timeManager().timeStepSize();
                    }
                }
            }
            if(mass_ > getParam<double>("BoundaryConditions.MassCycled"))
            {
                mass_ = 0.0;
                isExtracting_ = false;
            }
            else if(mass_ < -getParam<double>("BoundaryConditions.MassCycled"))
            {
                mass_ = 0.0;
                isExtracting_ = true;
            }
            std::cout << "Mass is " << mass_ << " and we are currently extracting? " << isExtracting_ << std::endl;
        }
    }

    if (hasParam("Output.PlotOperationOverTime"))
    {
        Scalar injectionRate = 0.0;
        static Scalar mass = 0.0;
        Scalar wellPressure = 0.0;
        Scalar gasPressureAtWell = 0.0;
        Scalar waterInGasAtWell = 0.0;
        Scalar saturationAtWell = 0.0;
        for (const auto& element : elements(this->gridView()))
        {
            for (const auto& intersection : intersections(this->gridView(), element))
            {
                BoundaryTypes bcType;
                this->boundaryTypes(bcType, intersection);
                if (bcType.isNeumann(Indices::contiNEqIdx))
                {
                    PrimaryVariables neumannValues;
                    this->neumann(neumannValues, intersection);
                    Scalar faceArea = intersection.geometry().volume();

                    injectionRate += neumannValues[Indices::contiNEqIdx] * faceArea;
                    mass += injectionRate * this->timeManager().timeStepSize();

                    const GlobalPosition &globalPos = intersection.geometry().center();
                    static const Scalar wellHeight = getParam<double>("BoundaryConditions.WellHeight");
                    static const Scalar cellNumberZ = getParam<double>("Grid.Cells2");
                    static const Scalar aquiferHeight = getParam<std::vector<Scalar>>("Grid.Axial2")[1];
                    static const Scalar deltaZ = aquiferHeight/cellNumberZ;
                    // plot bottom well pressure
                    if(isWell(globalPos) && globalPos[dim-1] > wellHeight && globalPos[dim-1] < wellHeight+deltaZ)
                    {
                        const auto element = intersection.inside();
                        const int eIdxGlobal = this->variables().index(element);
                        const CellData& cellData = this->variables().cellData(eIdxGlobal);
                        const GlobalPosition globalPos = element.geometry().center();
                        static const Scalar cellNumberX = getParam<std::vector<Scalar>>("Grid.Radial0")[1];
                        static const Scalar aquiferLength = getParam<double>("Grid.Cells0");

                        const DimMatrix permeabilityMatrix = this->spatialParams().intrinsicPermeability(element);
                        GlobalPosition distVec = intersection.geometry().center() - globalPos;
                        distVec = distVec/distVec.two_norm();
                        Dune::FieldVector<Scalar, dim> permeability(0);
                        permeabilityMatrix.mv(distVec, permeability);
                        GlobalPosition unitOuterNormal = intersection.centerUnitOuterNormal();
                        double deltaX = aquiferLength/cellNumberX;

                        const Scalar densityN = cellData.density(nPhaseIdx);
                        const Scalar mobilityN = cellData.mobility(nPhaseIdx);
                        gasPressureAtWell = cellData.pressure(nPhaseIdx);
                        waterInGasAtWell = cellData.massFraction(nPhaseIdx, wCompIdx);
                        saturationAtWell = cellData.saturation(nPhaseIdx);

                        static const Scalar angle = getParam<std::vector<Scalar>>("Grid.Angular1")[1];
                        static const Scalar wellRadius = getParam<Scalar>("Grid.WellRadius");
                        static const Scalar wellFaceArea = 2*M_PI*wellRadius*(angle/360)*deltaZ;
                        const Scalar faceArea = intersection.geometry().volume();

                        const Scalar flowN = neumannValues[Indices::contiNEqIdx] * faceArea;
                        wellPressure = gasPressureAtWell-flowN/
                                       (mobilityN*densityN*unitOuterNormal*permeability*wellFaceArea)*(0.5*deltaX);
                    }
                }
            }
        }

        //write data to plot
        std::ostringstream oss;
        oss << "operationOverTime" << ".out";
        std::string fileName = oss.str();
        outputFile_.open(fileName, std::ios::app);
        outputFile_ << this->timeManager().time() + this->timeManager().timeStepSize()
        << " " << injectionRate
        << " " << mass
        << " " << wellPressure
        << " " << gasPressureAtWell
        << " " << waterInGasAtWell
        << " " << saturationAtWell << std::endl;
        outputFile_.close();
    }
}

const bool isWell(const GlobalPosition& globalPos) const
{
    const Scalar angle = M_PI*(getParam<std::vector<Scalar>>("Grid.Angular1")[1])/180.0/2.0;
    const Scalar xCoord = cos(angle)*cos(angle)*getParam<Scalar>("Grid.WellRadius");
    const Scalar yCoord = sin(angle)*cos(angle)*getParam<Scalar>("Grid.WellRadius");
    if (globalPos[0] > xCoord - eps_ && globalPos[0] < xCoord + eps_
        && globalPos[1] > yCoord - eps_ && globalPos[1] < yCoord + eps_)
        return 1;
    else
        return 0;
}

const ComponentVector wellModel(const Intersection &intersection) const
{
    const auto element = intersection.inside();
    const int eIdxGlobal = this->variables().index(element);
    const CellData& cellData = this->variables().cellData(eIdxGlobal);
    const GlobalPosition globalPos = element.geometry().center();
    const CellArray numberOfCells = getParam<CellArray>("Grid.Cells");

    const DimMatrix permeabilityMatrix = this->spatialParams().intrinsicPermeability(element);
    GlobalPosition distVec = intersection.geometry().center() - globalPos;
    distVec = distVec/distVec.two_norm();
    Dune::FieldVector<Scalar, dim> permeability(0);
    permeabilityMatrix.mv(distVec, permeability);
    GlobalPosition unitOuterNormal = intersection.centerUnitOuterNormal();
    double deltaX = this->bBoxMax()[0]/numberOfCells[0];

    const Scalar densityN = cellData.density(nPhaseIdx);
    const Scalar mobilityN = cellData.mobility(nPhaseIdx);
    const Scalar pressureN = cellData.pressure(nPhaseIdx);

    static const Scalar angle = getParam<std::vector<Scalar>>("Grid.Angular1")[1];
    static const Scalar wellRadius = getParam<Scalar>("Grid.WellRadius");
    static const Scalar cellNumberZ = getParam<double>("Grid.Cells2");
    static const Scalar aquiferHeight = getParam<std::vector<Scalar>>("Grid.Axial2")[1];
    static const Scalar deltaZ = aquiferHeight/cellNumberZ;
    static const Scalar wellFaceArea = 2*M_PI*wellRadius*(angle/360)*deltaZ;
    const Scalar faceArea = intersection.geometry().volume();

    ComponentVector wellFlow;

    if(isExtracting_)
    {
        const Scalar densityW = cellData.density(wPhaseIdx);
        const Scalar mobilityW = cellData.mobility(wPhaseIdx);
        const Scalar pressureW = cellData.pressure(wPhaseIdx);
        const Scalar X_l_gComp = cellData.massFraction(wPhaseIdx, nCompIdx);
        const Scalar X_g_gComp = cellData.massFraction(nPhaseIdx, nCompIdx);
        const Scalar X_l_wComp = cellData.massFraction(wPhaseIdx, wCompIdx);
        const Scalar X_g_wComp = cellData.massFraction(nPhaseIdx, wCompIdx);

        const Scalar WellDeltaMax = getParam<double>("BoundaryConditions.WellDeltaMax");;
        const Scalar wellPressure = getParam<double>("BoundaryConditions.ExtractionPressureMax");
        Scalar wellDelta = pressureN-wellPressure;
        if(wellDelta > WellDeltaMax)
        {
            wellDelta = WellDeltaMax;
        }

        const Scalar flowW = mobilityW*densityW*unitOuterNormal*permeability*(wellDelta)/(0.5*deltaX)*wellFaceArea/faceArea;
        const Scalar flowN = mobilityN*densityN*unitOuterNormal*permeability*(wellDelta)/(0.5*deltaX)*wellFaceArea/faceArea;
        wellFlow[wCompIdx] = X_l_wComp*flowW + X_g_wComp*flowN;
        wellFlow[nCompIdx] = X_l_gComp*flowW + X_g_gComp*flowN;

        if(flowW<0 || flowN<0)
            std::cout << "Well flow is negative although we should be extracting!"
            << " flowW " << flowW << " flowN " << flowN
            << " mobilityW " << mobilityW
            << " pressureW " << pressureW
            << " mobilityN " << mobilityN
            << " pressureN " << pressureN
            << " wellPressure " << wellPressure
            << " unitOuterNormal*permeability " << unitOuterNormal*permeability
            << std::endl;
        else if(wellFlow[nCompIdx]> eps_ || wellFlow[wCompIdx]> eps_)
            std::cout << " wellFlow[wCompIdx] " << wellFlow[wCompIdx] << " wellFlow[nCompIdx] " << wellFlow[nCompIdx]
            << " mobilityW " << mobilityW
            << " pressureW " << pressureW
            << " mobilityN " << mobilityN
            << " pressureN " << pressureN
            << " wellPressure " << wellPressure
            << " unitOuterNormal*permeability " << unitOuterNormal*permeability
            << std::endl;
    }
    else
    {
        const Scalar WellDeltaMax = getParam<double>("BoundaryConditions.WellDeltaMax");
        const Scalar wellPressure = getParam<double>("BoundaryConditions.InjectionPressureMax");
        Scalar wellDelta = pressureN-wellPressure;
        if(-wellDelta > WellDeltaMax)
            wellDelta = -WellDeltaMax;

        wellFlow[wCompIdx] = 0.0;
        wellFlow[nCompIdx] = mobilityN*densityN*unitOuterNormal*permeability*(wellDelta)/(0.5*deltaX)*wellFaceArea/faceArea;

        if(wellFlow[nCompIdx]>0 || wellFlow[wCompIdx]>0)
            std::cout << "Well flow is positive although we should be injecting!"
            << " wellFlow[wCompIdx] " << wellFlow[wCompIdx] << " wellFlow[nCompIdx] " << wellFlow[nCompIdx]
            << " mobilityN " << mobilityN
            << " pressureN " << pressureN
            << " wellPressure " << wellPressure
            << " unitOuterNormal*permeability " << unitOuterNormal*permeability
            << std::endl;
    }

    return wellFlow;
}

/*! \brief Calculates gasPlumeDist, distance of gas plume from bottom
 *
 * Stores minGasPlumeDist for all grid cells
 */
const Scalar calculateGasPlumeDist(const Scalar totalGasConcentration, const PhaseVector columnRefPressures) const
{
    const int veModel = getParam<int>("VE.VEModel");
    const Scalar domainHeight = this->bBoxMax()[dim - 1];
    const Scalar gravity = this->gravity().two_norm();

    const Scalar resSatW = this->spatialParams().materialLawParams(dummy_).swr();
    const Scalar resSatN = this->spatialParams().materialLawParams(dummy_).snr();
    const Scalar porosity = this->spatialParams().porosity(dummy_);
    const Scalar entryP = this->spatialParams().materialLawParams(dummy_).pe();
    const Scalar lambda = this->spatialParams().materialLawParams(dummy_).lambda();

    const FluidState& fluidStatePlume = this->fluidStatePlume(dummy_, columnRefPressures);
    const Scalar X_l_gComp = fluidStatePlume.massFraction(wPhaseIdx, nCompIdx);
    const Scalar X_g_gComp = fluidStatePlume.massFraction(nPhaseIdx, nCompIdx);
    const Scalar densityW = fluidStatePlume.density(wPhaseIdx);
    const Scalar densityN = fluidStatePlume.density(nPhaseIdx);

    Scalar gasPlumeDist = 0.0;

    if(totalGasConcentration < 1e-12)
        gasPlumeDist = domainHeight;
    else if (veModel == 0) //calculate gasPlumeDist for sharp interface ve model
    {
        gasPlumeDist = domainHeight - (domainHeight * totalGasConcentration /
        ((porosity * (1-resSatW) * densityN * X_g_gComp) + (porosity * resSatW * densityW * X_l_gComp)));
    }

    else if (veModel == 1) //calculate gasPlumeDist for capillary fringe model
    {
        Scalar residual = 1.0;
        //check if GasPlumeDist>0, GasPlumeDist=0, GasPlumeDist<0
        const Scalar fullIntegral =
        porosity * densityN * X_g_gComp * domainHeight
        + porosity * (densityW * X_l_gComp - densityN * X_g_gComp) *
        (1.0 / (1.0 - lambda) * std::pow((domainHeight * (densityW - densityN) * gravity +
        entryP),(1.0 - lambda))
        * (1.0 - resSatW - resSatN) * std::pow(entryP, lambda) / ((densityW - densityN) * gravity)
        + resSatW * domainHeight
        - entryP * (1.0 - resSatW - resSatN) / ((1.0 - lambda) * (densityW - densityN) * gravity));
        //GasPlumeDist>0
        if (totalGasConcentration * domainHeight <= fullIntegral - 1e-6)
        {
            gasPlumeDist = domainHeight / 2.0; //XiStart
            //solve equation for
            int count = 0;
            for (; count < 100; count++)
            {
                residual =
                porosity * densityN * X_g_gComp * (domainHeight - gasPlumeDist)
                + porosity * (densityW * X_l_gComp - densityN * X_g_gComp) *
                (1.0 / (1.0 - lambda) * std::pow(((domainHeight - gasPlumeDist) * (densityW - densityN) * gravity +
                entryP),(1.0 - lambda))
                * (1.0 - resSatW - resSatN) * std::pow(entryP, lambda) / ((densityW - densityN) * gravity)
                + resSatW * (domainHeight - gasPlumeDist)
                - entryP * (1.0 - resSatW - resSatN) / ((1.0 - lambda) * (densityW - densityN) * gravity))
                - totalGasConcentration * domainHeight;

                if (fabs(residual) < 1e-12)
                {
                    break;
                }

                const Scalar derivation =
                - porosity * densityN * X_g_gComp
                + porosity * (densityW * X_l_gComp - densityN * X_g_gComp) *
                (std::pow(((domainHeight - gasPlumeDist) * (densityW - densityN) * gravity + entryP), -lambda)
                * (- 1.0 + resSatW + resSatN) * std::pow(entryP, lambda) - resSatW);

                gasPlumeDist = gasPlumeDist - residual / (derivation);

                if (count == 99)
                    std::cout << "Iterating gasPlumeDist not successfull, count is at maximum, exit iteration." << std::endl;
            }
        }
        //GasPlumeDist<0
        else if (totalGasConcentration * domainHeight > fullIntegral + 1e-6)
        {
            gasPlumeDist = 0.0; //XiStart
            //solve equation
            int count = 0;
            for (; count < 100; count++)
            {
                residual =
                porosity * densityN * X_g_gComp * domainHeight
                + porosity * (densityW * X_l_gComp - densityN * X_g_gComp) *
                (1.0 / (1.0 - lambda) * std::pow(((domainHeight - gasPlumeDist) * (densityW - densityN) * gravity + entryP), (1.0 - lambda))
                * (1.0 - resSatW - resSatN)* std::pow(entryP, lambda) / ((densityW - densityN) * gravity)
                + resSatW * domainHeight
                - 1.0 / (1.0 - lambda) * std::pow((entryP - gasPlumeDist * (densityW - densityN)
                * gravity),(1.0 - lambda)) * (1.0 - resSatW - resSatN) * std::pow(entryP, lambda)
                / ((densityW - densityN) * gravity))
                - totalGasConcentration * domainHeight;

                if (fabs(residual) < 1e-10)
                {
                    break;
                }

                const Scalar derivation =
                porosity * (densityW * X_l_gComp - densityN * X_g_gComp) *
                std::pow(((domainHeight - gasPlumeDist) * (densityW - densityN) * gravity + entryP), -lambda)
                * (- 1.0 + resSatN + resSatW) * std::pow(entryP, lambda)
                + std::pow((entryP - gasPlumeDist * (densityW - densityN) * gravity), -lambda)
                * (1.0 - resSatN - resSatW) * std::pow(entryP, lambda);

                gasPlumeDist = gasPlumeDist - residual / (derivation);

                if (count == 99)
                    std::cout << "Iterating gasPlumeDist not successfull, count is at maximum, exit iteration." << std::endl;
            }
        }
    }
    return gasPlumeDist;
}

/*! \brief Determines fluid state inside plume
 */
FluidState fluidStatePlume(const Element& element, const PhaseVector columnRefPressures) const
{
    const GlobalPosition& globalPos = element.geometry().center();

    const Scalar resSatW = this->spatialParams().materialLawParams(element).swr();
    const Scalar porosity = this->spatialParams().porosity(element);
    const Scalar temperature = this->temperatureAtPos(globalPos);

    FluidState fluidStatePlume;
    CompositionalFlash<Scalar, FluidSystem> flashSolver;
    flashSolver.saturationFlash2p2c(fluidStatePlume,
                                    resSatW,
                                    columnRefPressures,
                                    porosity,
                                    temperature);

    return fluidStatePlume;
}

/*! \brief Calculates gasPlumeDist, distance of gas plume from bottom
 *
 * Stores minGasPlumeDist for all grid cells
 */
const Scalar reconstSaturation(const Scalar height,
                               const Scalar gasPlumeDist,
                               const PhaseVector columnRefPressures) const
{
    const Scalar resSatW = this->spatialParams().materialLawParams(dummy_).swr();
    const Scalar resSatN = this->spatialParams().materialLawParams(dummy_).snr();
    const Scalar entryP = this->spatialParams().materialLawParams(dummy_).pe();
    const Scalar lambda = this->spatialParams().materialLawParams(dummy_).lambda();
    const int veModel = getParam<int>("VE.VEModel");

    const FluidState& fluidStatePlume = this->fluidStatePlume(dummy_, columnRefPressures);
    const Scalar densityW = fluidStatePlume.density(wPhaseIdx);
    const Scalar densityN = fluidStatePlume.density(nPhaseIdx);

    Scalar reconstSaturation = 0.0;

    if (veModel == 0) //reconstruct phase saturation for sharp interface ve model
    {
        reconstSaturation = 1.0;
        if(height > gasPlumeDist)
        {
            reconstSaturation = resSatW;
        }
    }
    else if (veModel == 1) //reconstruct phase saturation for capillary fringe model
    {
        reconstSaturation = 1.0;
        if(height > gasPlumeDist)
        {
            reconstSaturation = std::pow(((height - gasPlumeDist) * (densityW - densityN) * this->gravity().two_norm() + entryP), (-lambda))
            * std::pow(entryP, lambda) * (1.0 - resSatW - resSatN) + resSatW;
        }
    }

    return reconstSaturation;
}

/*! \brief Calculates integral of difference of wetting saturation over z
 *
 */
const Scalar calculateSatCriterionIntegral(const Scalar lowerBound,
                                           const Scalar upperBound,
                                           const Scalar satW,
                                           const Scalar gasPlumeDist,
                                           const PhaseVector columnRefPressures) const
{
    int intervalNumber = 10;
    Scalar deltaZ = (upperBound - lowerBound)/intervalNumber;

    Scalar satIntegral = 0.0;
    for(int count=0; count<intervalNumber; count++ )
    {
        satIntegral += std::abs((reconstSaturation(lowerBound + count*deltaZ, gasPlumeDist, columnRefPressures)
        + reconstSaturation(lowerBound + (count+1)*deltaZ, gasPlumeDist, columnRefPressures))/2.0 - satW);
    }
    satIntegral = satIntegral * deltaZ;

    return satIntegral;
}

/*! \brief Calculates integral of difference of relative wetting permeability over z
 *
 */
const Scalar calculateKrwCriterionIntegral(const Scalar lowerBound,
                                     const Scalar upperBound,
                                     const Scalar satW,
                                     const Scalar gasPlumeDist,
                                     const PhaseVector columnRefPressures) const
{
    int intervalNumber = 10;
    Scalar deltaZ = (upperBound - lowerBound)/intervalNumber;

    Scalar krwIntegral = 0.0;
    for(int count=0; count<intervalNumber; count++ )
    {
        Scalar sat1 = reconstSaturation(lowerBound + count*deltaZ, gasPlumeDist, columnRefPressures);
        Scalar sat2 = reconstSaturation(lowerBound + (count+1)*deltaZ, gasPlumeDist, columnRefPressures);
        Scalar krw1 = MaterialLaw::krw(this->spatialParams().materialLawParams(dummy_), sat1);
        Scalar krw2 = MaterialLaw::krw(this->spatialParams().materialLawParams(dummy_), sat2);
        Scalar krw = MaterialLaw::krw(this->spatialParams().materialLawParams(dummy_), satW);
        krwIntegral += std::abs((krw1 + krw2)/2.0 - krw);
    }
    krwIntegral = krwIntegral * deltaZ;

    return krwIntegral;
}

void addOutputVtkFields()
{
    ParentType::addOutputVtkFields();
    int size = this->gridView().size(0);
    using SolutionTypes = GetProp<TypeTag, Properties::SolutionTypes>;
    using ScalarSolution = typename SolutionTypes::ScalarSolution;
    ScalarSolution *permeability = this->resultWriter().allocateManagedBuffer (size);

    for (const auto& element : Dune::elements(this->gridView()))
    {
        const int eIdxGlobal = this->variables().index(element);
        (*permeability)[eIdxGlobal] = this->spatialParams().intrinsicPermeability(element)[dim-1][dim-1];
    }

    this->resultWriter().attachCellData(*permeability, "permeability");
    return;
}

private:
GlobalPosition lowerLeft_;
GlobalPosition upperRight_;

static constexpr Scalar eps_ = 1e-6;
std::string name_;

std::multimap<int, Element> mapColumns_;
std::ofstream outputFile_;
int veModel_;
Element dummy_;
Scalar segTime_;
Scalar viscosityW_;

Scalar mass_;
Scalar isExtracting_;
};
} //end namespace

#endif
