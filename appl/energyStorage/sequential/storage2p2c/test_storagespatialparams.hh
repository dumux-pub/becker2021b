// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup SequentialTwoPTwoCTests
 * \brief spatial parameters for the sequential 2p2c test
 */
#ifndef TEST_STORAGESPATIALPARAMS_HH
#define TEST_STORAGESPATIALPARAMS_HH

#include <dumux/porousmediumflow/2p2c/sequential/properties.hh>
#include <dumux/material/spatialparams/sequentialfv.hh>
#include <dumux/material/fluidmatrixinteractions/2p/linearmaterial.hh>
#include <dumux/material/fluidmatrixinteractions/2p/regularizedbrookscorey.hh>
#include <dumux/material/fluidmatrixinteractions/2p/brookscoreyLambda.hh>
#include <dumux/material/fluidmatrixinteractions/2p/efftoabslaw.hh>

namespace Dumux
{
//forward declaration
template<class TypeTag>
class TestStorageSpatialParams;

namespace Properties
{
// The spatial parameters TypeTag
namespace TTag {
struct TestStorageSpatialParams {};
}

// Set the spatial parameters
template<class TypeTag>
struct SpatialParams<TypeTag, TTag::TestStorageSpatialParams> { using type = TestStorageSpatialParams<TypeTag>; };

// Set the material law
template<class TypeTag>
struct MaterialLaw<TypeTag, TTag::TestStorageSpatialParams>
{
private:
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using RawMaterialLaw = RegularizedBrooksCorey<Scalar>;
/*    using RawMaterialLaw = LinearMaterial<Scalar>;
    using RawMaterialLaw = ExponentialMaterial<Scalar>*/;
public:
    using type = EffToAbsLaw<RawMaterialLaw>;
};
}

/*!
 * \ingroup SequentialTwoPTwoCTests
 * \brief spatial parameters for the sequential 2p2c test
 */
template<class TypeTag>
class TestStorageSpatialParams : public SequentialFVSpatialParams<TypeTag>
{
    using Problem = GetPropType<TypeTag, Properties::Problem>;
    using Grid = GetPropType<TypeTag, Properties::Grid>;
    using GridView = typename GetPropType<TypeTag, Properties::GridGeometry>::GridView;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using CoordScalar = typename Grid::ctype;

    enum { dim = GridView::dimension, dimWorld = Grid::dimensionworld };
    using Element = typename Grid::Traits::template Codim<0>::Entity;

    using FieldMatrix = Dune::FieldMatrix<Scalar, dim, dim>;
    using GlobalPosition = Dune::FieldVector<CoordScalar, dimWorld>;

public:
    using MaterialLaw = GetPropType<TypeTag, Properties::MaterialLaw>;
    using MaterialLawParams = typename MaterialLaw::Params;

    const FieldMatrix& intrinsicPermeability (const Element& element) const
    {
        GlobalPosition globalPos = element.geometry().center();
        if (globalPos[0] > 30 && globalPos[0] < 40)
        {
            const auto ySphere = (((std::pow(getParam<Scalar>("Grid.DomeVerschiebungOutside"),2.0))+(std::pow((getParam<std::vector<Scalar>>("Grid.Radial0")[1]),2.0))-(std::pow((getParam<std::vector<Scalar>>("Grid.Axial2")[1]),2.0)))/(((getParam<std::vector<Scalar>>("Grid.Axial2")[1])-getParam<Scalar>("Grid.DomeVerschiebungOutside"))*2.0));
            const auto sphereRadius = ySphere + (getParam<std::vector<Scalar>>("Grid.Radial0")[1]);
            const auto radialDistance = (std::sqrt((std::pow(globalPos[0],2.0))+(std::pow(globalPos[1],2.0))));
            const auto differenceTopToDome = (sphereRadius - (std::sqrt(((std::pow(sphereRadius,2.0))-((std::pow(radialDistance,2.0)))))));
            globalPos[dim-1] = globalPos[dim-1] + differenceTopToDome - ((getParam<std::vector<Scalar>>("Grid.Axial2")[1]) - getParam<Scalar>("Grid.DomeVerschiebungOutside"));
            if(globalPos[dim-1] > 10 && globalPos[dim-1] < 20)
                return permeabilityLense_;
        }
        else if (globalPos[0] > 120 && globalPos[0] < 140)
        {
            const auto ySphere = (((std::pow(getParam<Scalar>("Grid.DomeVerschiebungOutside"),2.0))+(std::pow((getParam<std::vector<Scalar>>("Grid.Radial0")[1]),2.0))-(std::pow((getParam<std::vector<Scalar>>("Grid.Axial2")[1]),2.0)))/(((getParam<std::vector<Scalar>>("Grid.Axial2")[1])-getParam<Scalar>("Grid.DomeVerschiebungOutside"))*2.0));
            const auto sphereRadius = ySphere + (getParam<std::vector<Scalar>>("Grid.Radial0")[1]);
            const auto radialDistance = (std::sqrt((std::pow(globalPos[0],2.0))+(std::pow(globalPos[1],2.0))));
            const auto differenceTopToDome = (sphereRadius - (std::sqrt(((std::pow(sphereRadius,2.0))-((std::pow(radialDistance,2.0)))))));
            globalPos[dim-1] = globalPos[dim-1] + differenceTopToDome - ((getParam<std::vector<Scalar>>("Grid.Axial2")[1]) - getParam<Scalar>("Grid.DomeVerschiebungOutside"));
            if(globalPos[dim-1] > 20)
                return permeabilityLense_;
        }
        return constPermeability_;
    }

    double porosity(const Element& element) const
    {
        return 0.1308;
    }


    // return the parameter object for the Brooks-Corey material law which depends on the position
    const MaterialLawParams& materialLawParams(const Element &element) const
    {
            return materialLawParams_;
    }


    TestStorageSpatialParams(const Problem& problem) : SequentialFVSpatialParams<TypeTag>(problem),
    constPermeability_(0), permeabilityLense_(0)
    {
        // residual saturations
        materialLawParams_.setSwr(0.0);
        materialLawParams_.setSnr(0.1);

       // parameters for the Brooks-Corey Law
       // entry pressures
        materialLawParams_.setPe(1e5);
       // Brooks-Corey shape parameters
        materialLawParams_.setLambda(2.0);

       // parameters for the Brooks-Corey Lambda Law
       // entry pressures
//         materialLawParams_.setPe(1e5);
        // Brooks-Corey shape parameter for capillary pressure
//         materialLawParams_.setLambdaPc(2.0);
        // Brooks-Corey shape parameter for relative permeability
//         materialLawParams_.setLambdaKr(2.0);

        // parameters for the linear
        // entry pressures function
//        materialLawParams_.setEntryPc(0);
//        materialLawParams_.setMaxPc(0);

        for(int i = 0; i < dim; i++)
        {
            constPermeability_[i][i] = 2.24e-14;
        }

        for(int i = 0; i < dim; i++)
        {
            permeabilityLense_[i][i] = getParam<Scalar>("SpatialParams.PermeabilityLens");
        }
    }

private:
    MaterialLawParams materialLawParams_;
    FieldMatrix constPermeability_, permeabilityLense_;

};

} // end namespace
#endif
