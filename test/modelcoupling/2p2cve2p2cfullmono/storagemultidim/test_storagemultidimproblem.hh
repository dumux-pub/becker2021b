// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief test problem for the sequential 2p model
 */
#ifndef TEST_STORAGE_MULTIDIM_PROBLEM_HH
#define TEST_STORAGE_MULTIDIM_PROBLEM_HH

#include <dumux/material/fluidsystems/h2oh2.hh>
#include <dumux/material/fluidsystems/h2oh2tabulated.hh>
#include <dumux/material/components/h2tables.hh>

#include <dumux/porousmediumflow/2p2c/sequential/adaptiveproperties.hh>
#include <dumux/porousmediumflow/2p2c/sequential/problem.hh>

#include <dumux/decoupled/2p2cve/celldata2p2cvemultidim.hh>
#include <dumux/decoupled/2p2cve/fvtransport2p2cvemultidim.hh>
#include <dumux/decoupled/2p2cve/fvpressure2p2cvemultidim.hh>
#include "mygridadaptvefulld.hh"
#include <dumux/decoupled/2p2cve/gridadaptionindicator2p2cvefulld.hh>
#include <dumux/porousmediumflow/2p/sequential/impes/gridadaptionindicator.hh>
#include <dumux/decoupled/2pve/gridadaptinitializationindicatorvefulld.hh>
#include <dumux/decoupled/2p2cve/variableclassadaptive2p2cvefulld.hh>

#include "test_storagemultidimspatialparams.hh"

#include<dumux/porousmediumflow/2p/sequential/transport/cellcentered/evalcflfluxcoats.hh>
#include<dumux/porousmediumflow/2p/sequential/transport/cellcentered/evalcflfluxdefault.hh>

namespace Dumux
{

template<class TypeTag>
class StorageMultiDimTestProblem;

//////////
// Specify the properties
//////////
namespace Properties
{
// Create new type tags
namespace TTag {
    struct StorageMultiDimTest {
        using InheritsFrom = std::tuple<StorageMultiDimSpatialParams, SequentialTwoPTwoCAdaptive>; };
} // end namespace TTag

// Set the grid type
template<class TypeTag>
struct Grid<TypeTag, TTag::StorageMultiDimTest> { using type = Dune::UGGrid<3>; };

// Set the problem property
template<class TypeTag>
struct Problem<TypeTag, TTag::StorageMultiDimTest> { using type = StorageMultiDimTestProblem<TypeTag>; };

// Set the cell data
template<class TypeTag>
struct CellData<TypeTag, TTag::StorageMultiDimTest> { using type = CellData2P2CVEMultiDim<TypeTag>; };

//Set the transport model
template<class TypeTag>
struct TransportModel<TypeTag, TTag::StorageMultiDimTest> { using type = FVTransport2P2CVEMultiDim<TypeTag>; };

//Set the pressure model
template<class TypeTag>
struct PressureModel<TypeTag, TTag::StorageMultiDimTest> { using type = FVPressure2P2CVEMultiDim<TypeTag>; };

//Set the grid adaption model
template<class TypeTag>
struct GridAdaptModel<TypeTag, TTag::StorageMultiDimTest> { using type = MyGridAdaptVE<TypeTag, true>; };

//Set the variable class
template<class TypeTag>
struct Variables<TypeTag, TTag::StorageMultiDimTest> { using type = VariableClassAdaptive2P2CVE<TypeTag>; };

////////////////////////////////////////////////////////////////////////
//Switch to a p_n-S_w formulation
//
//SET_INT_PROP(IMPESTestProblem, Formulation,
//        DecoupledTwoPCommonIndices::pnsn);
//
////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////
//Switch to a p_global-S_w formulation
//
//SET_INT_PROP(IMPESTestProblem, Formulation,
//        DecoupledTwoPCommonIndices::pGlobalSw);
//
//Define the capillary pressure term in the transport equation -> only needed in case of a p_global-S_w formulation!
//SET_TYPE_PROP(IMPESTestProblem, CapillaryFlux, CapillaryDiffusion<TypeTag>);
//
//Define the gravity term in the transport equation -> only needed in case of a p_global-S_w formulation!
//SET_TYPE_PROP(IMPESTestProblem, GravityFlux, GravityPart<TypeTag>);
//
////////////////////////////////////////////////////////////////////////

// Set the fluid system
template<class TypeTag>
struct FluidSystem<TypeTag, TTag::StorageMultiDimTest>
{
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using type = FluidSystems::H2OH2Tabulated<Scalar, H2Tables::H2Tables>;
};

// template<class TypeTag>
// struct GridView<TypeTag, TTag::StorageMultiDimTest> { using type = Dune::UGGrid<2>::LevelGridView; };

template<class TypeTag>
struct LinearSolver<TypeTag, TTag::StorageMultiDimTest> { using type = SuperLUBackend; };

template<class TypeTag>
struct EnableCapillarity<TypeTag, TTag::StorageMultiDimTest> { static constexpr bool value = true; };
template<class TypeTag>
struct BoundaryMobility<TypeTag, TTag::StorageMultiDimTest> {
    static constexpr int value = GetPropType<TypeTag, Properties::Indices>::satDependent; };
    template<class TypeTag>
    struct PressureFormulation<TypeTag, TTag::StorageMultiDimTest> {
        static constexpr int value = GetPropType<TypeTag, Properties::Indices>::pressureW; };

//Adaptivity
template<class TypeTag>
struct AdaptionIndicator<TypeTag, TTag::StorageMultiDimTest> {
    using type = GridAdaptionIndicator2P2CVEFullD<TypeTag>; };

template<class TypeTag>
struct AdaptionInitializationIndicator<TypeTag, TTag::StorageMultiDimTest> {
    using type = GridAdaptInitializationIndicatorVEFullD<TypeTag>; };

template<class TypeTag>
struct VisitFacesOnlyOnce<TypeTag, TTag::StorageMultiDimTest> { static constexpr bool value = true; };
}
/*!
 * \ingroup 2PVETest
 *
 * \brief test problem for the sequential 2p model
 *
 * Water is injected from the left side into a rectangular 2D domain also
 * filled with water. Upper and lower boundary is closed (Neumann = 0),
 * and there is free outflow on the right side.
 *
 * To run the simulation execute the following line in shell:
 * <tt>./test_impes -parameterFile ./test_impes.input</tt>,
 * where the arguments define the parameter file..
 */
template<class TypeTag>
class StorageMultiDimTestProblem: public IMPETProblem2P2C<TypeTag>
{
    using ParentType = IMPETProblem2P2C<TypeTag>;
    using Grid = GetPropType<TypeTag, Properties::Grid>;
    using GridView = typename GetPropType<TypeTag, Properties::GridGeometry>::GridView;
    using LeafGridView = typename Grid::LeafGridView;
    using GlobalIdSet = typename Grid::GlobalIdSet;
    using IdType = typename Grid::LocalIdSet::IdType;
    using Indices = GetPropType<TypeTag, Properties::Indices>;

    using FluidSystem = GetPropType<TypeTag, Properties::FluidSystem>;

    using TimeManager = GetPropType<TypeTag, Properties::TimeManager>;

    enum
    {
        dim = GridView::dimension, dimWorld = GridView::dimensionworld
    };

    enum
    {
        wPhaseIdx = Indices::wPhaseIdx,
        nPhaseIdx = Indices::nPhaseIdx,
        wCompIdx = Indices::wPhaseIdx,
        nCompIdx = Indices::nPhaseIdx,
        numComponents = getPropValue<TypeTag, Properties::NumComponents>()
    };

    enum VEModel
    {
        sharpInterface,
        capillaryFringe
    };

    using Scalar = GetPropType<TypeTag, Properties::Scalar>;

    using Element = typename GridView::Traits::template Codim<0>::Entity;
    using Intersection = typename GridView::Intersection;
    using GlobalPosition = typename Dune::FieldVector<Scalar, dimWorld>;

    using BoundaryTypes = GetPropType<TypeTag, Properties::SequentialBoundaryTypes>;
    using SolutionTypes = GetProp<TypeTag, Properties::SolutionTypes>;
    using PrimaryVariables = typename SolutionTypes::PrimaryVariables;

    using CellData = GetPropType<TypeTag, Properties::CellData>;
    using SpatialParams = GetPropType<TypeTag, Properties::SpatialParams>;
    using MaterialLaw = GetPropType<TypeTag, Properties::MaterialLaw>;

    using CellArray = std::array<unsigned int, dimWorld>;

public:
    StorageMultiDimTestProblem(TimeManager &timeManager, Grid &grid) :
    ParentType(timeManager, grid), eps_(1e-6), visualizationWriter_(this->grid().leafGridView(), "gridAfterReconstruction")
    , normWriter_(this->grid().leafGridView(), "gridAfterRefinement")
    , debugWriter_(this->grid().leafGridView(), "debugGridAfterAdaptation")
    {
        // initialize the tables of the fluid system
        FluidSystem::init(280.0, 400.0, 121, 1e5, 1000e5, 1000);

        this->grid().setClosureType(Dune::UGGrid<dim>::ClosureType::NONE);

        int outputInterval = 1e9;
        if (hasParam("Problem.OutputInterval"))
        {
            outputInterval = getParam<Scalar>("Problem.OutputInterval");
        }
        this->setOutputInterval(outputInterval);

        Scalar outputTimeInterval = 1e6;
        if (hasParam("Problem.OutputTimeInterval"))
        {
            outputTimeInterval = getParam<Scalar>("Problem.OutputTimeInterval");
        }
        this->setOutputTimeInterval(outputTimeInterval);

        this->spatialParams().plotMaterialLaw();

        name_ = getParam<std::string>("Problem.Name");

        if (hasParam("Grid.UpperRight"))
            aquiferHeight_ = getParam<std::vector<Scalar>>("Grid.UpperRight")[dim-1];
        else if (hasParam("Grid.Axial" +  std::to_string(dim-1)))
            aquiferHeight_ = getParam<std::vector<Scalar>>("Grid.Axial" +  std::to_string(dim-1)).back();
        else
            DUNE_THROW(Dune::GridError, "Unknown grid specification. Domain height could not be identified.");

        if (hasParam("Grid.UpperRight"))
            aquiferLength_ = getParam<std::vector<Scalar>>("Grid.UpperRight")[0];
        else if (hasParam("Grid.Radial" +  std::to_string(0)))
            aquiferLength_ = getParam<std::vector<Scalar>>("Grid.Radial" +  std::to_string(0)).back();
        else
            DUNE_THROW(Dune::GridError, "Unknown grid specification. Domain length could not be identified.");

        updateColumnMap();

        //calculate length of capillary transition zone (CTZ)
        Scalar swr = this->spatialParams().materialLawParams(dummy_).swr();
        Scalar snr = this->spatialParams().materialLawParams(dummy_).snr();
        Scalar satW1 = 1.0 - snr;
        Scalar satW2 = swr + 0.1*(1.0-swr-snr);
        Scalar pc1 = MaterialLaw::pc(this->spatialParams().materialLawParams(dummy_), satW1);
        Scalar pc2 = MaterialLaw::pc(this->spatialParams().materialLawParams(dummy_), satW2);
        GlobalPosition globalPos = dummy_.geometry().center();
        Scalar temp = temperatureAtPos(globalPos);
        Scalar pRef = referencePressureAtPos(globalPos);
        Scalar densityW = FluidSystem::H2O::liquidDensity(temp, pRef);
        Scalar densityNw = FluidSystem::H2::gasDensity(temp, pRef);
        CTZ_ = (pc2-pc1)/((densityW-densityNw)*this->gravity().two_norm());
        std::cout << "CTZ " << CTZ_ << std::endl;

        CellArray numberOfCells = this->numberOfCells();
        int numberOfColumns = numberOfCells[0];
        modelVector_.clear();
        if(modelVector_.size() != numberOfColumns)
        {
            modelVector_.resize(numberOfColumns);
        }
        for(int i=0; i<numberOfColumns; i++)
        {
            modelVector_[i] = 2;
        }

        averageSatInPlume_ = 1.0;

        //reset all output
        system("rm *.out");
}

void updateColumnMap()// TODO: more efficient way? Only change entries when column has changed
{
    mapAllColumns_.clear();
    // column number
    static CellArray numberOfCells = this->numberOfCells();
    static double deltaX = this->aquiferLength()/numberOfCells[0];
    static double transform = getParam<Scalar>("Grid.WellRadius");
    // store pointer to all elements in a map, indicated by their global index
    // iterate over all elements
    for (const auto& element : Dune::elements(this->gridView()))
    {
        GlobalPosition globalPos = element.geometry().center();
        int j = round((globalPos[0] - (deltaX/2.0) - transform)/deltaX);
        mapAllColumns_.insert(std::make_pair(j, element));
        dummy_ = element;
    }
}

void updateModelVector()
{
    CellArray numberOfCells = this->numberOfCells();
    int numberOfColumns = numberOfCells[0];
    modelVector_.clear();
    if(modelVector_.size() != numberOfColumns)
    {
        modelVector_.resize(numberOfColumns);
    }
    for(int i=0; i<numberOfColumns; i++)
    {
        typename std::map<int, Element>::iterator it = mapAllColumns_.find(i);
        int eIdxGlobal = this->variables().index(it->second);
        int veModel = this->variables().cellData(eIdxGlobal).veModel();
        modelVector_[i] = veModel;
    }

}

void setModel()// TODO: more efficient way? Only change entries when column has changed
{
    CellArray numberOfCells = this->numberOfCells();
    double deltaX = this->aquiferLength()/numberOfCells[0];
    double areaVE = this->aquiferHeight() * deltaX;//TODO: 3-D
    int veModel = getParam<int>("VE.VEModel");
    // set Model based on volume of cell: VE or full-D
    // iterate over all elements
    for (const auto& element : Dune::elements(this->gridView()))
    {
        double volume = element.geometry().volume();
        int eIdxGlobal = this->variables().index(element);
        static const Scalar angle = M_PI*(getParam<std::vector<Scalar>>("Grid.Angular1")[1])/180.0;
        double extrusion = tan(angle) * element.geometry().center()[0];
        double volumeVE = areaVE * extrusion;
        int factor = round(volumeVE/volume);
        if(factor == 1)
        {
            this->variables().cellData(eIdxGlobal).setVeModel(veModel);
        }
        else
        {
            this->variables().cellData(eIdxGlobal).setVeModel(2);
        }
    }
    updateModelVector();
}

void postTimeStep()
{
    ParentType::postTimeStep();

    int outputInterval = 1e9;
    if (hasParam("Problem.OutputInterval"))
    {
        outputInterval = getParam<Scalar>("Problem.OutputInterval");
    }

    const CellArray numberOfCells = this->numberOfCells();
    const int reconstruction = getParam<int>("Grid.Reconstruction");
    const double deltaX = this->aquiferLength()/numberOfCells[0];
    const double deltaZ = this->aquiferHeight()/(numberOfCells[dim - 1]*std::pow(2, reconstruction));

    //plot reconstructed solution
    if(this->timeManager().timeStepIndex() % outputInterval == 0 || this->timeManager().willBeFinished()
            || this->timeManager().episodeWillBeFinished() || this->timeManager().timeStepIndex() == 0)
    {
        GridView gridView = this->gridView();

        //refine VE columns
        std::unordered_map<IdType, int> mapGlobalIdx;
        this->grid().preAdapt();
        for(int i=0;i<reconstruction;i++)
        {
            for (const auto& element : Dune::elements(gridView))
            {
                if(i == 0)
                {
                    IdType globalId = gridView.grid().globalIdSet().id(element);
                    int eIdxGlobal = this->variables().index(element);
                    mapGlobalIdx.insert(std::pair<IdType,int>(globalId, eIdxGlobal));
                }
                static double transform = getParam<Scalar>("Grid.WellRadius");
                GlobalPosition globalPos = element.geometry().center();
                int columnNumber = round((globalPos[0] - (deltaX/2.0) - transform)/deltaX);
                if(modelVector_[columnNumber] == 0 || modelVector_[columnNumber] == 1)
                    this->grid().mark(element, UG::D3::HEX_BISECT_0_3, 0);
            }
            // adapt the grid
            this->grid().adapt();
        }
        this->grid().postAdapt();

        // use visualization writer
        visualizationWriter_.gridChanged();
        visualizationWriter_.beginWrite(this->timeManager().time() + this->timeManager().timeStepSize());
        //write stuff out
        using ScalarSolutionType = typename SolutionTypes::ScalarSolution;
        using CellData = GetPropType<TypeTag, Properties::CellData>;

        int size = gridView.size(0);

        ScalarSolutionType *totalConcentrationW = visualizationWriter_.allocateManagedBuffer (size);
        ScalarSolutionType *totalConcentrationN = visualizationWriter_.allocateManagedBuffer (size);
        ScalarSolutionType *pressureW = visualizationWriter_.allocateManagedBuffer (size);
        ScalarSolutionType *pressureNw = visualizationWriter_.allocateManagedBuffer (size);
        ScalarSolutionType *capillaryPressure = visualizationWriter_.allocateManagedBuffer (size);
        ScalarSolutionType *saturationW = visualizationWriter_.allocateManagedBuffer (size);
        ScalarSolutionType *saturationNw = visualizationWriter_.allocateManagedBuffer (size);
        ScalarSolutionType *mobilityW = visualizationWriter_.allocateManagedBuffer (size);
        ScalarSolutionType *mobilityNw = visualizationWriter_.allocateManagedBuffer (size);
        ScalarSolutionType *densityW = visualizationWriter_.allocateManagedBuffer (size);
        ScalarSolutionType *densityNw = visualizationWriter_.allocateManagedBuffer (size);
        ScalarSolutionType *massFractionWW = visualizationWriter_.allocateManagedBuffer (size);
        ScalarSolutionType *massFractionNW = visualizationWriter_.allocateManagedBuffer (size);
        ScalarSolutionType *veModel = visualizationWriter_.allocateManagedBuffer (size);
        ScalarSolutionType *gasPlumeDist = visualizationWriter_.allocateManagedBuffer (size);
        ScalarSolutionType *minGasPlumeDist = visualizationWriter_.allocateManagedBuffer (size);


        int i = 0;
        for (const auto& element : Dune::elements(gridView))
        {
            //identify column number of element and check if it is a VE column or a full-d column
            static double transform = getParam<Scalar>("Grid.WellRadius");
            GlobalPosition globalPos = element.geometry().center();
            int columnNumber = round((globalPos[0] - (deltaX/2.0) - transform)/deltaX);//starting with 0

            //full-d elements
            if(modelVector_[columnNumber] == 2)
            {
                IdType globalId = gridView.grid().globalIdSet().id(element);
                int eIdxGlobal2D = mapGlobalIdx.find(globalId)->second;
                CellData& cellData = this->variables().cellData(eIdxGlobal2D);
                (*totalConcentrationW)[i] = cellData.totalConcentration(wCompIdx);
                (*totalConcentrationN)[i] = cellData.totalConcentration(nCompIdx);
                (*pressureW)[i]  = cellData.pressure(wPhaseIdx);
                (*pressureNw)[i] = cellData.pressure(nPhaseIdx);
                (*capillaryPressure)[i] = cellData.capillaryPressure();
                (*saturationW)[i] = cellData.saturation(wPhaseIdx);
                (*saturationNw)[i] = cellData.saturation(nPhaseIdx);
                (*mobilityW)[i] = cellData.mobility(wPhaseIdx);
                (*mobilityNw)[i] = cellData.mobility(nPhaseIdx);
                (*densityW)[i] = cellData.density(wPhaseIdx);
                (*densityNw)[i] = cellData.density(nPhaseIdx);
                (*massFractionWW)[i] = cellData.massFraction(wPhaseIdx, wCompIdx);
                (*massFractionNW)[i] = cellData.massFraction(nPhaseIdx, wCompIdx);
                (*veModel)[i] = cellData.veModel();
                (*gasPlumeDist)[i] = std::min(this->aquiferHeight(), cellData.gasPlumeDist());
                (*minGasPlumeDist)[i] = std::min(this->aquiferHeight(), cellData.minGasPlumeDist());
            }
            else
            {
                const int eIdxGlobal = this->variables().index(element);

                Element veElement = mapAllColumns_.find(columnNumber)->second;
                const GlobalPosition& globalPosVE = veElement.geometry().center();
                int eIdxGlobalVE = this->variables().index(veElement);
                CellData& cellData = this->variables().cellData(eIdxGlobalVE);

                const Scalar localTransform = globalPosVE[dim-1] - this->aquiferHeight()/2.0;
                const Scalar top = (globalPos[dim - 1] + deltaZ/2.0) - localTransform;
                const Scalar bottom = (globalPos[dim - 1] - deltaZ/2.0) - localTransform;
                const Scalar positionZ = globalPos[dim - 1] - localTransform;

                const Dune::FieldVector<Scalar,numComponents> totalConcentration =
                this->pressureModel().concentrationIntegral(bottom, top, veElement)/(top-bottom);

                const Scalar satW = this->pressureModel().saturationIntegral(bottom, top, veElement)/(top-bottom);
                const Scalar satNw = 1.0-satW;

                (*pressureW)[i] = this->pressureModel().reconstPressure(positionZ, wPhaseIdx, veElement);
                (*pressureNw)[i] = this->pressureModel().reconstPressure(positionZ, nPhaseIdx, veElement);
                (*totalConcentrationW)[i] = totalConcentration[wCompIdx];
                (*totalConcentrationN)[i] = totalConcentration[nCompIdx];
                (*capillaryPressure)[i] = this->pressureModel().reconstCapillaryPressure(positionZ, veElement);
                (*saturationW)[i] = satW;
                (*saturationNw)[i] = satNw;
                (*mobilityW)[i] = this->pressureModel().relPermeabilityIntegral(bottom, top, veElement, wPhaseIdx)
                / (top-bottom) / cellData.viscosity(wPhaseIdx);
                (*mobilityNw)[i] = this->pressureModel().relPermeabilityIntegral(bottom, top, veElement, nPhaseIdx)
                / (top-bottom) / cellData.viscosity(nPhaseIdx);
                (*densityW)[i] = this->pressureModel().densityLiquidAverage(bottom, top, veElement);
                (*densityNw)[i] = cellData.density(nPhaseIdx);
                (*massFractionWW)[i] = this->pressureModel().massFractionLiquidAverage(bottom, top, veElement)[wCompIdx];
                (*massFractionNW)[i] = cellData.massFraction(nPhaseIdx, wCompIdx);
                (*veModel)[i] = cellData.veModel();
                (*gasPlumeDist)[i] = cellData.gasPlumeDist();
                (*minGasPlumeDist)[i] = cellData.minGasPlumeDist();
            }
            i++;
        }
        visualizationWriter_.attachCellData(*totalConcentrationW, "total concentration W");
        visualizationWriter_.attachCellData(*totalConcentrationN, "total concentration N");
        visualizationWriter_.attachCellData(*pressureW, "wetting pressure");
        visualizationWriter_.attachCellData(*pressureNw, "non-wetting pressure");
        visualizationWriter_.attachCellData(*capillaryPressure, "capillary pressure");
        visualizationWriter_.attachCellData(*saturationW, "wetting saturation");
        visualizationWriter_.attachCellData(*saturationNw, "non-wetting saturation");
        visualizationWriter_.attachCellData(*mobilityW, "wetting mobility");
        visualizationWriter_.attachCellData(*mobilityNw, "non-wetting mobility");
        visualizationWriter_.attachCellData(*densityW, "wetting density");
        visualizationWriter_.attachCellData(*densityNw, "non-wetting density");
        visualizationWriter_.attachCellData(*massFractionWW, "mass fraction W in wetting phase");
        visualizationWriter_.attachCellData(*massFractionNW, "mass fraction W in non-wetting phase");
        visualizationWriter_.attachCellData(*veModel, "VE model");
        visualizationWriter_.attachCellData(*gasPlumeDist, "gas plume distance");
        visualizationWriter_.attachCellData(*minGasPlumeDist, "min gas plume distance");
        visualizationWriter_.endWrite();

//        //refine grid for error norm calculation
//        this->grid().preAdapt();
//        const int additionalRefinementSteps = 2;
//        for(int i=0;i<additionalRefinementSteps;i++)
//        {
//            for (const auto& element : Dune::elements(gridView))
//            {
//                GlobalPosition globalPos = element.geometry().center();
//                this->grid().mark(1, element);
//            }
//            // adapt the grid
//            this->grid().adapt();
//            this->grid().postAdapt();
//        }
//        const double deltaZRefined = this->aquiferHeight()/(numberOfCells[dim - 1]*std::pow(2, (reconstruction+additionalRefinementSteps)));
//
//        // use error norm writer
//        normWriter_.gridChanged();
//        normWriter_.beginWrite(this->timeManager().time() + this->timeManager().timeStepSize());
//
//        size = gridView.size(0);
//        ScalarSolutionType *pressureW1 = normWriter_.allocateManagedBuffer (size);
//        ScalarSolutionType *pressureNw1 = normWriter_.allocateManagedBuffer (size);
//        ScalarSolutionType *potentialW1 = normWriter_.allocateManagedBuffer (size);
//        ScalarSolutionType *potentialNw1 = normWriter_.allocateManagedBuffer (size);
//        ScalarSolutionType *capillaryPressure1 = normWriter_.allocateManagedBuffer (size);
//        ScalarSolutionType *saturationW1 = normWriter_.allocateManagedBuffer (size);
//        ScalarSolutionType *saturationNw1 = normWriter_.allocateManagedBuffer (size);
//        ScalarSolutionType *mobilityW1 = normWriter_.allocateManagedBuffer (size);
//        ScalarSolutionType *mobilityNw1 = normWriter_.allocateManagedBuffer (size);
//        ScalarSolutionType *veModel1 = normWriter_.allocateManagedBuffer (size);
//
//        //write output on refined grid
//        i = 0;
//        for (const auto& element : Dune::elements(gridView))
//        {
//            //identify column number of element and check if it is a VE column or a full-d column
//            GlobalPosition globalPos = element.geometry().center();
//            int columnNumber = floor(globalPos[0]/deltaX);//starting with 0
//
//            //identify ID of cell, same as ID of parallel run with 20 blocks
//            //determine block number starting from 0
//            int blockNumberX = floor(globalPos[0]/35);
//            int blockNumberY = floor(globalPos[dim-1]/15);
//            int blockNumber = blockNumberY*10 + blockNumberX;
//            //determine local cell ID in block, starting from 0
//            Scalar localX = globalPos[0] - (blockNumberX*35);
//            Scalar localY = globalPos[dim-1] - (blockNumberY*15);
//            int localNumberX = floor(localX/0.25);
//            int localNumberY = floor(localY/(30.0/512.0));
//            int localID = localNumberY*140 + localNumberX;
//            //determine global ID, starting from 0
//            int globalID = blockNumber*35840 + localID;
//
//            //full-d elements
//            if(modelVector_[columnNumber] == 2)
//            {
//                IdType globalId = gridView.grid().globalIdSet().id(element.father().father());
//                int eIdxGlobal2D = mapGlobalIdx.find(globalId)->second;
//                CellData& cellData = this->variables().cellData(eIdxGlobal2D);
//                (*pressureW1)[globalID]  = cellData.pressure(wPhaseIdx);
//                (*pressureNw1)[globalID] = cellData.pressure(nPhaseIdx);
//                (*potentialW1)[globalID]  = cellData.potential(wPhaseIdx);
//                (*potentialNw1)[globalID] = cellData.potential(nPhaseIdx);
//                (*capillaryPressure1)[globalID] = cellData.capillaryPressure();
//                (*saturationW1)[globalID] = cellData.saturation(wPhaseIdx);
//                (*saturationNw1)[globalID] = cellData.saturation(nPhaseIdx);
//                (*mobilityW1)[globalID] = cellData.mobility(wPhaseIdx);
//                (*mobilityNw1)[globalID] = cellData.mobility(nPhaseIdx);
//                (*veModel1)[globalID] = cellData.veModel();
//            }
//            else
//            {
//                Element veElement = mapAllColumns_.find(columnNumber)->second;
//                int eIdxGlobalVE = this->variables().index(veElement);
//                CellData& cellData = this->variables().cellData(eIdxGlobalVE);
//
//                Scalar top = globalPos[dim - 1] + deltaZRefined/2.0;
//                Scalar bottom = globalPos[dim - 1] - deltaZRefined/2.0;
//
//                Scalar pRef = referencePressureAtPos(globalPos);
//                Scalar temp = temperatureAtPos(globalPos);
//
//                (*pressureW1)[globalID] = this->pressureModel().reconstPressure(globalPos[dim-1], wPhaseIdx, veElement);
//                (*pressureNw1)[globalID] = this->pressureModel().reconstPressure(globalPos[dim-1], nPhaseIdx, veElement);
//                (*potentialW1)[globalID] = this->pressureModel().reconstPotential(globalPos[dim-1], wPhaseIdx, veElement);
//                (*potentialNw1)[globalID] = this->pressureModel().reconstPotential(globalPos[dim-1], nPhaseIdx, veElement);
//                (*capillaryPressure1)[globalID] = this->pressureModel().reconstCapillaryPressure(globalPos[dim-1], veElement);
//                (*saturationW1)[globalID] = this->pressureModel().saturationIntegral(bottom, top, veElement)/(top-bottom);
//                (*saturationNw1)[globalID] = 1.0 - this->pressureModel().saturationIntegral(bottom, top, veElement)/(top-bottom);
//                (*mobilityW1)[globalID] = this->pressureModel().calculateRelPermeabilityCoarse(bottom, top, veElement, wPhaseIdx)/WettingPhase::viscosity(temp, pRef);
//                (*mobilityNw1)[globalID] = this->pressureModel().calculateRelPermeabilityCoarse(bottom, top, veElement, nPhaseIdx)/NonWettingPhase::viscosity(temp, pRef);
//                (*veModel1)[globalID] = cellData.veModel();
//            }
//            i++;
//        }
//        normWriter_.attachCellData(*pressureW1, "wetting pressure");
//        normWriter_.attachCellData(*pressureNw1, "non-wetting pressure");
//        normWriter_.attachCellData(*potentialW1, "wetting potential");
//        normWriter_.attachCellData(*potentialNw1, "non-wetting potential");
//        normWriter_.attachCellData(*capillaryPressure1, "capillary pressure");
//        normWriter_.attachCellData(*saturationW1, "wetting saturation");
//        normWriter_.attachCellData(*saturationNw1, "non-wetting saturation");
//        normWriter_.attachCellData(*mobilityW1, "wetting mobility");
//        normWriter_.attachCellData(*mobilityNw1, "non-wetting mobility");
//        normWriter_.attachCellData(*veModel1, "veModel");
//        normWriter_.endWrite();
//
//        //coarsen grid after error norm calculation
//        for(int i=0;i<additionalRefinementSteps;i++)
//        {
//            for (const auto& element : Dune::elements(gridView))
//            {
//                GlobalPosition globalPos = element.geometry().center();
//                this->grid().mark(element, UG::D3::COARSE, 0);
//            }
//            // adapt the grid
//            this->grid().adapt();
//            this->grid().postAdapt();
//            this->grid().preAdapt();
//        }

        //coarsen ve-columns
        for(int i=0;i<reconstruction;i++)
        {
            for (const auto& element : Dune::elements(gridView))
            {
                static double transform = getParam<Scalar>("Grid.WellRadius");
                GlobalPosition globalPos = element.geometry().center();
                int columnNumber = round((globalPos[0] - (deltaX/2.0) - transform)/deltaX);
                if(modelVector_[columnNumber] == 0 || modelVector_[columnNumber] == 1)
                    this->grid().mark(element, UG::D3::COARSE, 0);
            }
            // adapt the grid
            this->grid().preAdapt();
            this->grid().adapt();
            this->grid().postAdapt();
        }
    }

    if (getParam<bool>("Output.PlotDissolvedMass"))
    {
        bool plotCriteria = false;
        const int timeStepIdx = this->timeManager().timeStepIndex();
        //only plot VE criteria over domain for specific timeStepIdx
        if (timeStepIdx % 10 == 0)
        {
            plotCriteria = true;
        }

        if(plotCriteria)
        {
            Scalar totalMassGas = 0.0;
            Scalar totalMassDissolvedGas = 0.0;
            //iterate over all cells
            for (const auto& element : elements(this->gridView()))
            {
                const Scalar volume = element.geometry().volume();
                const GlobalPosition globalPos = element.geometry().center();
                const int eIdxGlobal = this->variables().index(element);
                CellData& cellData = this->variables().cellData(eIdxGlobal);

                totalMassGas += volume * cellData.totalConcentration(nCompIdx);
                totalMassDissolvedGas += cellData.saturation(wPhaseIdx) * volume
                * this->spatialParams().porosity(element) * cellData.density(wPhaseIdx)
                * cellData.massFraction(wPhaseIdx, nCompIdx);
            }

            //write data to mass-plots
            std::ostringstream oss;
            oss << "massDissolvedGasVEMultiDim" << ".out";
            std::string fileName = oss.str();
            outputFile_.open(fileName, std::ios::app);
            outputFile_ << this->timeManager().time() << " " << totalMassDissolvedGas <<  " " <<  totalMassDissolvedGas/totalMassGas*100 << std::endl;
            outputFile_.close();
        }
    }

    if (getParam<bool>("Output.PlotOperationOverTime"))
    {
        Scalar gasPressureAtWell = 0.0;
        Scalar waterInGasAtWell = 0.0;
        Scalar saturationAtWell = 0.0;
        for (const auto& element : elements(this->gridView()))
        {
            for (const auto& intersection : intersections(this->gridView(), element))
            {
                BoundaryTypes bcType;
                this->boundaryTypes(bcType, intersection);
                if (bcType.isNeumann(Indices::contiNEqIdx))
                {
                    const GlobalPosition &globalPos = intersection.geometry().center();
                    static const Scalar wellHeight = getParam<double>("BoundaryConditions.WellHeight");
                    const CellArray numberOfCells = this->numberOfCells();
                    const int reconstruction = getParam<int>("Grid.Reconstruction");
                    const double deltaZ = this->aquiferHeight()/(numberOfCells[dim - 1]*std::pow(2, reconstruction));
                    // plot bottom well pressure
                    if(isWell(globalPos) && globalPos[dim-1] > wellHeight && globalPos[dim-1] < wellHeight+deltaZ)
                    {
                        const auto element = intersection.inside();
                        const int eIdxGlobal = this->variables().index(element);
                        const CellData& cellData = this->variables().cellData(eIdxGlobal);
                        const GlobalPosition globalPos = element.geometry().center();;
                        gasPressureAtWell = cellData.pressure(nPhaseIdx);
                        waterInGasAtWell = cellData.massFraction(nPhaseIdx, wCompIdx);
                        saturationAtWell = cellData.saturation(nPhaseIdx);
                    }
                }
            }
        }

        //write data to plot
        std::ostringstream oss;
        oss << "operationOverTimeVEMultiDim" << ".out";
        std::string fileName = oss.str();
        outputFile_.open(fileName, std::ios::app);
        outputFile_ << this->timeManager().time() + this->timeManager().timeStepSize()
        << " " << gasPressureAtWell
        << " " << waterInGasAtWell
        << " " << saturationAtWell << std::endl;
        outputFile_.close();
    }

    if (getParam<bool>("Output.PlotNumberOfCells"))
    {
        //write data to plot
        std::ostringstream oss;
        oss << "numberOfCells" << ".out";
        std::string fileName = oss.str();
        outputFile_.open(fileName, std::ios::app);
        outputFile_ << this->timeManager().time() + this->timeManager().timeStepSize()
        << " " << this->gridView().size(0) << std::endl;
        outputFile_.close();
    }

//
//     //write out average column saturation
//     CellArray numberOfCells = this->numberOfCells();
//     std::vector<Scalar> averageSatColumn(0);
//     Scalar gasPlumeVolume = 0.0;// total volume of gas plume
//     averageSatColumn.resize(numberOfCells[0]);
//     for (int i = 0; i != averageSatColumn.size(); ++i)
//     {
//         Scalar totalVolume = 0.0;// total volume of column
//         typename std::map<int, Element>::iterator it = mapAllColumns_.lower_bound(i);
//         for (; it != mapAllColumns_.upper_bound(i); ++it)
//         {
//             int globalIdxI = this->variables().index(it->second);
//             GlobalPosition globalPos = (it->second).geometry().center();
//             Scalar satW = this->variables().cellData(globalIdxI).saturation(wPhaseIdx);
//             Scalar volume = it->second.geometry().volume();
//             averageSatColumn[i] += satW * volume;
//             totalVolume += volume;
//
//             if(globalPos[0]<=TwoDArea_ && satW<1.0-eps_)//attention!
//             {
//                 averageSatInPlume_ += satW * volume;
//                 gasPlumeVolume += volume;
//             }
//         }
//         averageSatColumn[i] = averageSatColumn[i]/totalVolume;//average wetting saturation in column (equals gasPlumeDist for SI and no compressibility)
//         outputFile_.open("averageSatColumn.out", std::ios::app);
//         outputFile_ << " " << averageSatColumn[i];
//         outputFile_.close();
//     }
//    averageSatInPlume_ = averageSatInPlume_/gasPlumeVolume;//average wetting saturation in plume
//    if(gasPlumeVolume<eps_)
//    {
//        averageSatInPlume_ = 1.0;
//    }
//
//    //calculate error for certain column
//    Scalar errorSat(0.0);
//    Scalar gasPlumeHeight2D(0.0);
//
//    const int maxLevel = getParam<int>("GridAdapt.MaxLevel");
//    double deltaZ = this->aquiferHeight()/(numberOfCells[dim - 1]*std::pow(2, MaxLevel));
//
//    int veModel = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, int, VE, VEModel);
//
//    int columnNumber = 9;
//    typename std::map<int, Element>::iterator it = mapAllColumns_.lower_bound(columnNumber);
//    Scalar gasPlumeDist = calculateGasPlumeDist(it->second, averageSat[columnNumber]);
//
//    for (; it != mapAllColumns_.upper_bound(columnNumber); ++it)
//    {
//        int globalIdxI = this->variables().index(it->second);
//        GlobalPosition globalPos = (it->second).geometry().center();
//        Scalar top = globalPos[dim - 1] + deltaZ/2.0;
//        Scalar bottom = globalPos[dim - 1] - deltaZ/2.0;
//
//        Scalar satW = this->variables().cellData(globalIdxI).saturation(wPhaseIdx);
//        Scalar resSatW = this->spatialParams().materialLawParams(it->second).swr();
//
//        if(veModel == sharpInterface)//calculate error for VE model
//        {
//            if (top <= gasPlumeDist)
//            {
//                errorSat += std::abs(deltaZ * (satW - 1.0));
//            }
//            else if (bottom >= gasPlumeDist)
//            {
//                errorSat += std::abs(deltaZ * (satW - resSatW));
//
//            }
//            else
//            {
//                Scalar lowerDelta = gasPlumeDist - bottom;
//                Scalar upperDelta = top - gasPlumeDist;
//                errorSat += std::abs(lowerDelta * (satW - 1.0)) + std::abs(upperDelta * (satW - resSatW));
//            }
//        }
//        else if(veModel == capillaryFringe)//calculate error for capillary fringe model
//        {
//            if (top <= gasPlumeDist)
//            {
//                errorSat += std::abs(deltaZ * (satW - 1.0));
//            }
//            else if (bottom >= gasPlumeDist)
//            {
//                errorSat += calculateErrorSatIntegral(bottom, top, satW, gasPlumeDist);
//            }
//            else
//            {
//                Scalar lowerDelta = gasPlumeDist - bottom;
//                Scalar upperDelta = top - gasPlumeDist;
//                errorSat += std::abs(lowerDelta * (satW - 1.0)) + calculateErrorSatIntegral(gasPlumeDist, top, satW, gasPlumeDist);
//            }
//        }
//        if(satW<0.99)
//        {
//            gasPlumeHeight2D += (top-bottom);
//        }
//    }
//
//    errorSat = errorSat/(this->aquiferHeight()-gasPlumeDist);
//
//    if(averageSat[columnNumber]>1.0-eps_)
//    {
//        errorSat = 0.0;
//    }
//
//    std::cout << "errorSat " << errorSat;
//
//    outputFile_.open("errorSat.out", std::ios::app);
//    outputFile_ << this->timeManager().time() << " " << errorSat << std::endl;
//    outputFile_.close();
//
//     outputFile_.open("averageSatColumn.out", std::ios::app);
//     outputFile_ << " " << std::endl;
//     outputFile_.close();
//
//     //check mass conservativity:
//     GridView GridView = this->gridView();
//     Scalar totalMassN = 0.0;
//     int numberOfCellsTotal = 0;
//     for (const auto& element : Dune::elements(GridView))
//     {
//         int eIdxGlobal = this->variables().index(element);
//         CellData& cellData = this->variables().cellData(eIdxGlobal);
//         GlobalPosition globalPos = element.geometry().center();
//         Scalar temp = temperatureAtPos(globalPos);
//         Scalar pRef = referencePressureAtPos(globalPos);
//         Scalar densityNw = FluidSystem::H2::gasDensity(temp, pRef);
//         Scalar massN = cellData.saturation(nPhaseIdx) * element.geometry().volume() * this->spatialParams().porosity(element) * densityNw;
//         totalMassN += massN;
//         numberOfCellsTotal += 1;
//     }
//     Scalar totalMassNExpected = -getParam<Scalar>("BoundaryConditions.InjectionrateN") * (this->timeManager().time()+this->timeManager().timeStepSize())
//             * this->aquiferHeight();
//     std::cout << "Error in mass: " << totalMassNExpected - totalMassN << ". ";
//
//     outputFile_.open("numberOfCells.out", std::ios::app);
//     outputFile_ << this->timeManager().time() << " " << numberOfCellsTotal << std::endl;
//     outputFile_.close();
}

/*!
 * \brief Capability to introduce problem-specific routines after grid adaptation
 *
 * Function is called at the end of the standard grid
 * modification routine, GridAdapt::adaptGrid() , to allow
 * for problem-specific output etc.
 */
void debugPlot()
{
    std::cout << " debugPlot ";
    // write out new grid
//     if(this->timeManager().timeStepIndex() % outputInterval_ == 0)
//     {
        debugWriter_.gridChanged();
        debugWriter_.beginWrite(this->timeManager().time());
        //write stuff out
        using ScalarSolutionType = typename SolutionTypes::ScalarSolution;
        using CellData = GetPropType<TypeTag, Properties::CellData>;

        int size = this->gridView().size(0);
        ScalarSolutionType *pressureW = debugWriter_.allocateManagedBuffer (size);
        ScalarSolutionType *pressureNw = debugWriter_.allocateManagedBuffer (size);
        ScalarSolutionType *totalConcentrationW = debugWriter_.allocateManagedBuffer (size);
        ScalarSolutionType *totalConcentrationN = debugWriter_.allocateManagedBuffer (size);
        ScalarSolutionType *capillaryPressure = debugWriter_.allocateManagedBuffer (size);
        ScalarSolutionType *saturationW = debugWriter_.allocateManagedBuffer (size);
        ScalarSolutionType *saturationNw = debugWriter_.allocateManagedBuffer (size);
        ScalarSolutionType *mobilityW = debugWriter_.allocateManagedBuffer (size);
        ScalarSolutionType *mobilityNw = debugWriter_.allocateManagedBuffer (size);
        ScalarSolutionType *densityWetting = debugWriter_.allocateManagedBuffer(size);
        ScalarSolutionType *densityNonwetting = debugWriter_.allocateManagedBuffer(size);
        ScalarSolutionType *massfraction1W = debugWriter_.allocateManagedBuffer (size);
        ScalarSolutionType *massfraction1NW = debugWriter_.allocateManagedBuffer (size);
        ScalarSolutionType *volErr = debugWriter_.allocateManagedBuffer (size);
        ScalarSolutionType *veModel = visualizationWriter_.allocateManagedBuffer (size);
        ScalarSolutionType *gasPlumeDist = debugWriter_.allocateManagedBuffer (size);
        ScalarSolutionType *minGasPlumeDist = debugWriter_.allocateManagedBuffer (size);

        int i = 0;
        for (const auto& element : Dune::elements(this->gridView()))
        {
            //identify column number of element and check if it is a VE column or a full-d column
            int eIdxGlobal = this->variables().index(element);
            CellData& cellData = this->variables().cellData(eIdxGlobal);
            (*pressureW)[i]  = cellData.pressure(wPhaseIdx);
            (*pressureNw)[i] = cellData.pressure(nPhaseIdx);
            (*totalConcentrationW)[i] = cellData.totalConcentration(wCompIdx);
            (*totalConcentrationN)[i] = cellData.totalConcentration(nCompIdx);
            (*capillaryPressure)[i] = cellData.capillaryPressure();
            (*saturationW)[i] = cellData.saturation(wPhaseIdx);
            (*saturationNw)[i] = cellData.saturation(nPhaseIdx);
            (*mobilityW)[i] = cellData.mobility(wPhaseIdx);
            (*mobilityNw)[i] = cellData.mobility(nPhaseIdx);
            (*densityWetting)[i] = cellData.density(wPhaseIdx);
            (*densityNonwetting)[i] = cellData.density(nPhaseIdx);
            (*massfraction1W)[i] = cellData.massFraction(wPhaseIdx,wCompIdx);
            (*massfraction1NW)[i] = cellData.massFraction(nPhaseIdx,wCompIdx);
            (*volErr)[i] = cellData.volumeError();
            (*veModel)[i] = cellData.veModel();
            (*gasPlumeDist)[i] = std::min(this->aquiferHeight(), cellData.gasPlumeDist());
            (*minGasPlumeDist)[i] = std::min(this->aquiferHeight(), cellData.minGasPlumeDist());
            i++;
        }
        debugWriter_.attachCellData(*pressureW, "wetting pressure");
        debugWriter_.attachCellData(*pressureNw, "non-wetting pressure");
        debugWriter_.attachCellData(*totalConcentrationW, "total concentration W");
        debugWriter_.attachCellData(*totalConcentrationN, "total concentration N");
        debugWriter_.attachCellData(*capillaryPressure, "capillary pressure");
        debugWriter_.attachCellData(*saturationW, "wetting saturation");
        debugWriter_.attachCellData(*saturationNw, "non-wetting saturation");
        debugWriter_.attachCellData(*mobilityW, "wetting mobility");
        debugWriter_.attachCellData(*mobilityNw, "non-wetting mobility");
        debugWriter_.attachCellData(*densityWetting, "wetting density");
        debugWriter_.attachCellData(*densityNonwetting, "nonwetting density");
        std::ostringstream oss1, oss2;
//         oss1 << "mass fraction " << FluidSystem::componentName(0) << " in " << FluidSystem::phaseName(0) << "-phase";
//         debugWriter_.attachCellData(*massfraction1W, oss1.str());
//         oss2 << "mass fraction " << FluidSystem::componentName(0) << " in " << FluidSystem::phaseName(1) << "-phase";
//         debugWriter_.attachCellData(*massfraction1NW, oss2.str());
        debugWriter_.attachCellData(*volErr, "volume Error");
        debugWriter_.attachCellData(*veModel, "VE model");
        debugWriter_.attachCellData(*gasPlumeDist, "gas plume distance");
        debugWriter_.attachCellData(*minGasPlumeDist, "min gas plume distance");
        debugWriter_.endWrite();
//     }
}

/*! \brief Calculates gasPlumeDist, distance of gas plume from bottom
 *
 * Stores minGasPlumeDist for all grid cells
 */
// Scalar calculateGasPlumeDist(const Element& element, Scalar satW)
// {
//     Scalar domainHeight = this->aquiferHeight();
//     Scalar resSatW = this->spatialParams().materialLawParams(*element).swr();
//     Scalar resSatN = this->spatialParams().materialLawParams(*element).snr();
//     Scalar gravity = this->gravity().two_norm();
//     int veModel = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, int, VE, VEModel);
//
//     Scalar gasPlumeDist = 0.0;
//
//     if (veModel == sharpInterface) //calculate gasPlumeDist for sharp interface ve model
//     {
//         gasPlumeDist = domainHeight * (satW - resSatW) / (1.0 - resSatW);
//     }
//
//     else if (veModel == capillaryFringe) //calculate gasPlumeDist for capillary fringe model
//     {
//         GlobalPosition globalPos = element.geometry().center();
//         Scalar pRef = referencePressureAtPos(globalPos);
//         Scalar tempRef = temperatureAtPos(globalPos);
//         Scalar densityW = WettingPhase::density(tempRef, pRef);
//         Scalar densityNw = NonWettingPhase::density(tempRef, pRef);
//         Scalar lambda = this->spatialParams().materialLawParams(*element).lambda();
//         Scalar entryP = this->spatialParams().materialLawParams(*element).pe();
//
//         Scalar Xi = domainHeight / 2.0; //XiStart
//
//         Scalar fullIntegral = 1.0 / (1.0 - lambda) * (1.0 - resSatW - resSatN) / ((densityW - densityNw) * gravity) * (std::pow(entryP, lambda)
//         - std::pow(entryP, 2.0 - lambda) + std::pow((domainHeight * (densityW - densityNw) * gravity + entryP), (1.0 - lambda)));
//         //GasPlumeDist>0
//         if (fullIntegral < satW * domainHeight)
//         {
//             //solve equation for
//             for (int count = 0; count < 100; count++)
//             {
//                 Scalar residual = 1.0 / (1.0 - lambda) * std::pow(((domainHeight - Xi) * (densityW - densityNw) * gravity + entryP),(1.0 - lambda))
//                 * (1.0 - resSatW - resSatN) * std::pow(entryP, lambda) / ((densityW - densityNw) * gravity) + resSatW * (domainHeight - Xi)
//                 - entryP * (1.0 - resSatW - resSatN) / ((1.0 - lambda) * (densityW - densityNw) * gravity) + Xi - satW * domainHeight;
//
//                 if (fabs(residual) < 1e-10)
//                     break;
//
//                 Scalar derivation = std::pow(((domainHeight - Xi) * (densityW - densityNw) * gravity + entryP), -lambda) * (resSatN + resSatW - 1.0)
//                         * std::pow(entryP, lambda) - resSatW + 1.0;
//
//                 Xi = Xi - residual / (derivation);
//             }
//         }
//         //GasPlumeDist<0
//         else if (fullIntegral > satW * domainHeight)
//         {
//             //solve equation
//             for (int count = 0; count < 100; count++)
//             {
//                 Scalar residual = 1.0 / (1.0 - lambda) * std::pow(((domainHeight - Xi) * (densityW - densityNw) * gravity + entryP),
//                         (1.0 - lambda)) * (1.0 - resSatW - resSatN)* std::pow(entryP, lambda) / ((densityW - densityNw) * gravity)
//                         + resSatW * domainHeight - 1.0 / (1.0 - lambda) * std::pow(((-Xi) * (densityW - densityNw)
//                         * gravity + entryP),(1.0 - lambda)) * (1.0 - resSatW - resSatN) * std::pow(entryP, lambda) / ((densityW - densityNw)
//                                 * gravity)
//                                 - satW * domainHeight;
//                 if (fabs(residual) < 1e-10)
//                     break;
//
//                 Scalar derivation = std::pow(((domainHeight - Xi) * (densityW - densityNw) * gravity + entryP), -lambda)
//                 * (resSatN + resSatW - 1.0) * std::pow(entryP, lambda) + std::pow(((-Xi) * (densityW - densityNw) * gravity + entryP),
//                         -lambda) * (1.0 - resSatN - resSatW) * std::pow(entryP, lambda);
//
//                 Xi = Xi - residual / (derivation);
//             };
//         }
//         //GasPlumeDist=0
//         else
//         {
//             Xi = 0.0;
//         }
//         gasPlumeDist = Xi;
//     }
//
//     return gasPlumeDist;
// }

/*! \brief Calculates integral of difference of wetting saturation over z
 *
 */
// Scalar calculateErrorSatIntegral(Scalar lowerBound, Scalar upperBound, Scalar satW, Scalar gasPlumeDist)
// {
//     int intervalNumber = 10;
//     Scalar deltaZ = (upperBound - lowerBound)/intervalNumber;
//
//     Scalar satIntegral = 0.0;
//     for(int count=0; count<intervalNumber; count++ )
//     {
//         satIntegral += std::abs((reconstSaturation(lowerBound + count*deltaZ, gasPlumeDist)
//                 + reconstSaturation(lowerBound + (count+1)*deltaZ, gasPlumeDist))/2.0 - satW);
//     }
//     satIntegral = satIntegral * deltaZ;
//
//     return satIntegral;
// }

/*! \brief Calculates gasPlumeDist, distance of gas plume from bottom
 *
 * Stores minGasPlumeDist for all grid cells
 */
// Scalar reconstSaturation(Scalar height, Scalar gasPlumeDist)
// {
//     Scalar domainHeight = this->aquiferHeight();
//     GlobalPosition globalPos = dummy_.geometry().center();
//     Scalar pRef = referencePressureAtPos(globalPos);
//     Scalar tempRef = temperatureAtPos(globalPos);
//     Scalar resSatW = this->spatialParams().materialLawParams(dummy_).swr();
//     Scalar resSatN = this->spatialParams().materialLawParams(dummy_).snr();
//     Scalar densityW = WettingPhase::density(tempRef, pRef);
//     Scalar densityNw = NonWettingPhase::density(tempRef, pRef);
//     Scalar entryP = this->spatialParams().materialLawParams(dummy_).pe();
//     Scalar lambda = this->spatialParams().materialLawParams(dummy_).lambda();
//     int veModel = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, int, VE, VEModel);
//
//     Scalar reconstSaturation = 0.0;
//
//     if (veModel == sharpInterface) //reconstruct phase saturation for sharp interface ve model
//     {
//         reconstSaturation = 1.0;
//         if(height > gasPlumeDist)
//         {
//             reconstSaturation = resSatW;
//         }
//     }
//     else if (veModel == capillaryFringe) //reconstruct phase saturation for capillary fringe model
//     {
//         reconstSaturation = 1.0;
//         if(height > gasPlumeDist)
//         {
//             reconstSaturation = std::pow(((height - gasPlumeDist) * (densityW - densityNw) * this->gravity().two_norm() + entryP), (-lambda))
//             * std::pow(entryP, lambda) * (1.0 - resSatW - resSatN) + resSatW;
//         }
//     }
//
//     return reconstSaturation;
// }

/*!
 *
 * return
 */
Scalar averageSatInPlume()
{
    return averageSatInPlume_;
}

/*!
 * \name Problem parameters
 */
// \{

/*!
 * \brief The problem name.
 *
 * This is used as a prefix for files generated by the simulation.
 */
const std::string name() const
{
    return name_;
}

bool shouldWriteRestartFile() const
{
    return false;
}

/*!
 * \brief Returns the temperature within the domain.
 *
 * This problem assumes a temperature of 10 degrees Celsius.
 */
Scalar temperatureAtPos(const GlobalPosition& globalPos) const
{
    return 326.0; // -> 53°C
}

// \}

//! Returns the reference pressure for evaluation of constitutive relations
Scalar referencePressureAtPos(const GlobalPosition& globalPos) const
{
    return 1.7e7; // -> 100 bar
}

/*!
* \brief Returns the type of boundary condition.
*
* BC for pressure equation can be dirichlet (pressure) or neumann (flux).
*
* BC for saturation equation can be dirichlet (saturation), neumann (flux), or outflow.
*/
void boundaryTypesAtPos(BoundaryTypes &bcTypes, const GlobalPosition& globalPos) const
{
    static const Scalar angle = M_PI*(getParam<std::vector<Scalar>>("Grid.Angular1")[1])/180.0/2.0;
    static const Scalar length = getParam<std::vector<Scalar>>("Grid.Radial0")[1]+getParam<Scalar>("Grid.WellRadius");
    static const Scalar xCoord = cos(angle)*cos(angle)*length;
    static const Scalar yCoord = sin(angle)*cos(angle)*length;
    if (globalPos[0] > xCoord - eps_ && globalPos[0] < xCoord + eps_
        && globalPos[1] > yCoord - eps_ && globalPos[1] < yCoord + eps_)
    {
        bcTypes.setAllDirichlet();
    }
    // all other boundaries
    else
    {
        bcTypes.setAllNeumann();
    }
}

//! Flag for the type of Dirichlet conditions
/*! The Dirichlet BCs can be specified by a given concentration (mass of
 * a component per total mass inside the control volume) or by means
 * of a saturation.
 *
 * \param bcFormulation The boundary formulation for the conservation equations.
 * \param intersection The intersection on the boundary.
 */
void boundaryFormulation(typename Indices::BoundaryFormulation &bcFormulation, const Intersection& intersection) const
{
    bcFormulation = Indices::concentration;
}


//! Values for dirichlet boundary condition \f$ [Pa] \f$ for pressure and \f$ \frac{mass}{totalmass} \f$ or \f$ S_{\alpha} \f$ for transport.
/*! In case of a dirichlet BC, values for all primary variables have to be set. In the sequential 2p2c model, a pressure
 * is required for the pressure equation and component fractions for the transport equations. Although one BC for the two
 * transport equations can be deduced by the other, it is seperately defined for consistency reasons with other models.
 * Depending on the boundary Formulation, either saturation or total mass fractions can be defined.
 *
 * \param bcValues Vector holding values for all equations (pressure and transport).
 * \param globalPos The global Position
 */
void dirichletAtPos(PrimaryVariables &bcValues ,const GlobalPosition& globalPos) const
{
    Scalar pRef = referencePressureAtPos(globalPos);
    Scalar temp = temperatureAtPos(globalPos);
    bcValues[Indices::pressureEqIdx] = (pRef + (this->bBoxMax()[dim-1] - globalPos[dim-1])
    * FluidSystem::H2O::liquidDensity(temp, pRef)
    * this->gravity().two_norm());

    bcValues[Indices::contiWEqIdx] = 1.;
    bcValues[Indices::contiNEqIdx] = 1.- bcValues[Indices::contiWEqIdx];
}


//! Value for neumann boundary condition \f$ [\frac{kg}{m^3 \cdot s}] \f$.
/*! In case of a neumann boundary condition, the flux of matter
 *  is returned. Both pressure and transport module regard the neumann-bc values
 *  with the tranport (mass conservation -) equation indices. So the first entry
 *  of the vector is superflous.
 *  An influx into the domain has negative sign.
 *
 * \param neumannValues Vector holding values for all equations (pressure and transport).
 * \param globalPos The global Position
 */
void neumann(PrimaryVariables &neumannValues,
             const Intersection &intersection) const
{
    this->setZero(neumannValues);

    const GlobalPosition &globalPos = intersection.geometry().center();
    if (isWell(globalPos))
    {
        static const Scalar cylcesDev = getParam<double>("BoundaryConditions.CyclesDev");
        static const Scalar injectionDurationDev = getParam<double>("BoundaryConditions.InjectionDurationDev");
        static const Scalar idleDurationDev = getParam<double>("BoundaryConditions.IdleDurationDev");
        static const Scalar developmentDuration = cylcesDev*(injectionDurationDev+idleDurationDev);
        static const Scalar wellHeight = getParam<double>("BoundaryConditions.WellHeight");
        const Scalar time = this->timeManager().time() + this->timeManager().timeStepSize();
        if(time <= developmentDuration && globalPos[dim-1] > wellHeight)
        {
            const int cycleNumber = std::floor(time/(injectionDurationDev+idleDurationDev));
            const Scalar localTimeInCycle = time - cycleNumber*(injectionDurationDev+idleDurationDev);
            if(localTimeInCycle < injectionDurationDev)
                neumannValues[Indices::contiNEqIdx] = getParam<double>("BoundaryConditions.InjectionRateDev");
        }
        else if(this->timeManager().time() + this->timeManager().timeStepSize() > developmentDuration
            && globalPos[dim-1] > wellHeight)
        {
            static const Scalar injectionDurationOp = getParam<double>("BoundaryConditions.InjectionDurationOp");
            static const Scalar extractionDurationOp = getParam<double>("BoundaryConditions.ExtractionDurationOp");
            const int cycleNumberOp = std::floor((time-developmentDuration)/(injectionDurationOp+extractionDurationOp));
            const Scalar localTimeInCycle = time-developmentDuration-cycleNumberOp*(injectionDurationOp+extractionDurationOp);
            if(localTimeInCycle < extractionDurationOp)
                neumannValues[Indices::contiNEqIdx] = getParam<double>("BoundaryConditions.ExtractionRateOp");
            else
                neumannValues[Indices::contiNEqIdx] = getParam<double>("BoundaryConditions.InjectionRateOp");
        }
    }
}

//! Source of mass \f$ [\frac{kg}{m^3 \cdot s}] \f$
/*! Evaluate the source term for all phases within a given
 *  volume. The method returns the mass generated (positive) or
 *  annihilated (negative) per volume unit.
 *  Both pressure and transport module regard the neumann-bc values
 *  with the tranport (mass conservation -) equation indices. So the first entry
 *  of the vector is superflous.
 *
 * \param sourceValues Vector holding values for all equations (pressure and transport).
 * \param globalPos The global Position
 */
void sourceAtPos(PrimaryVariables &sourceValues, const GlobalPosition& globalPos) const
{
    this->setZero(sourceValues);
}

//! Flag for the type of initial conditions
/*! The problem can be initialized by a given concentration (mass of
 * first component per total mass inside the control volume) or by means
 * of a saturation (of first phase). VE models: choose concentration.
 */
void initialFormulation(typename Indices::BoundaryFormulation &initialFormulation, const Element& element) const
{
    initialFormulation = Indices::concentration;
}

//! Concentration initial condition (dimensionless)
/*! VE models: always 1 (zero gas).
 */
Scalar initConcentrationAtPos(const GlobalPosition& globalPos) const
{
    const Scalar gasPlumeDistGlobal = getParam<Scalar>("Initial.gasPlumeDistGlobal");
    if(globalPos[dim-1] > gasPlumeDistGlobal)
    {
        const Scalar lambda = 2.0; // as in spatial params
        const Scalar entryP = 1e5; // as in spatial params
        const Scalar resSatW = 0.0; // as in spatial params
        const Scalar resSatN = 0.01; // as in spatial params
        const Scalar porosity = 0.1308; // as in spatial params
        const Scalar pRef = referencePressureAtPos(globalPos);
        const Scalar temp = temperatureAtPos(globalPos);
        const Scalar densityW = FluidSystem::H2O::liquidDensity(temp, pRef);
        const Scalar densityN = FluidSystem::H2::gasDensity(temp, pRef);
        const Scalar gravity = this->gravity().two_norm();
        const Scalar satW = std::pow((globalPos[dim-1] - gasPlumeDistGlobal)*(densityW-densityN)*gravity + entryP, -lambda)
        * std::pow(entryP, lambda) * (1-resSatW-resSatN) + resSatW;
        const Scalar totalConcW = porosity*satW*densityW;
        const Scalar totalConcG = porosity*(1-satW)*densityN;
        if(totalConcW < 1e-8 || totalConcG < 1e-8)
        {
            std::cout << " totalConcW " << totalConcW << " totalConcG " << totalConcG << std::endl;
        }
        return totalConcW/(totalConcW + totalConcG);
    }
    else
        return 1;
}

const bool isWell(const GlobalPosition& globalPos) const
{
    const Scalar angle = M_PI*(getParam<std::vector<Scalar>>("Grid.Angular1")[1])/180.0/2.0;
    const Scalar xCoord = cos(angle)*cos(angle)*getParam<Scalar>("Grid.WellRadius");
    const Scalar yCoord = sin(angle)*cos(angle)*getParam<Scalar>("Grid.WellRadius");
    if (globalPos[0] > xCoord - eps_ && globalPos[0] < xCoord + eps_
        && globalPos[1] > yCoord - eps_ && globalPos[1] < yCoord + eps_)
        return 1;
    else
        return 0;
}

std::multimap<int, Element>& getColumnMap()
{
    return mapAllColumns_;
}

int getColumnModel(int columnIndex)
{
    return modelVector_[columnIndex];
}

Scalar residualSegSaturation() const
{
    DUNE_THROW(Dune::NotImplemented,"residualSegSaturation not implemented in problem!");
}

Scalar residualSegSatAverage() const
{
    DUNE_THROW(Dune::NotImplemented,"residualSegSatAverage not implemented in problem!");
}

Scalar capillaryTransitionZone() const
{
    return CTZ_;
}

const Scalar aquiferHeight() const
{
    return aquiferHeight_;
}

const Scalar aquiferLength() const
{
    return aquiferLength_;
}

const CellArray numberOfCells() const
{
    if (hasParam("Grid.Cells"))
        return getParam<CellArray>("Grid.Cells");
    else if (hasParam("Grid.Cells" +  std::to_string(0)))
    {
        CellArray numberOfCells;
        for (int i = 0; i < dim; ++i)
        {
            numberOfCells[i] = getParam<Scalar>("Grid.Cells" +  std::to_string(i));
        }
        return numberOfCells;
    }
    else
        DUNE_THROW(Dune::GridError, "Unknown grid specification. Number of cells could not be identified.");
}


private:

const Scalar eps_;
std::string name_;
Dumux::VtkMultiWriter<LeafGridView> visualizationWriter_;
Dumux::VtkMultiWriter<LeafGridView> normWriter_;
Dumux::VtkMultiWriter<LeafGridView> debugWriter_;
std::multimap<int, Element> mapAllColumns_;
std::ofstream outputFile_;
Scalar TwoDArea_;
Scalar averageSatInPlume_;
Element dummy_;
Scalar CTZ_, aquiferHeight_, aquiferLength_;
std::vector<int> modelVector_;
};
} //end namespace

#endif
