// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief spatial parameters for the sequential 2p test
 */
#ifndef TEST_STORAGE_MULTIDIM_SPATIALPARAMS_HH
#define TEST_STORAGE_MULTIDIM_SPATIALPARAMS_HH

#include <dumux/material/spatialparams/sequentialfv.hh>
#include <dumux/material/fluidmatrixinteractions/2p/linearmaterial.hh>
#include <dumux/material/fluidmatrixinteractions/2p/regularizedbrookscorey.hh>
#include <dumux/material/fluidmatrixinteractions/2p/efftoabslaw.hh>
#include <dumux/io/gnuplotinterface.hh>
#include <dumux/io/plotmateriallaw.hh>

namespace Dumux
{

//forward declaration
template<class TypeTag>
class StorageMultiDimSpatialParams;

namespace Properties
{
// The spatial parameters TypeTag
namespace TTag {
    struct StorageMultiDimSpatialParams {};
}

// Set the spatial parameters
template<class TypeTag>
struct SpatialParams<TypeTag, TTag::StorageMultiDimSpatialParams> { using type = StorageMultiDimSpatialParams<TypeTag>; };

// Set the material law
template<class TypeTag>
struct MaterialLaw<TypeTag, TTag::StorageMultiDimSpatialParams>
{
private:
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using RawMaterialLaw = RegularizedBrooksCorey<Scalar>;
    //     using RawMaterialLaw = LinearMaterial<Scalar>;
public:
    using type = EffToAbsLaw<RawMaterialLaw>;
};
}

/*!
 *
 * \ingroup IMPETtests
 * \brief spatial parameters for the sequential 2p test
 */
template<class TypeTag>
class StorageMultiDimSpatialParams: public SequentialFVSpatialParams<TypeTag>
{
    using ParentType = SequentialFVSpatialParams<TypeTag>;
    using Problem = GetPropType<TypeTag, Properties::Problem>;
    using Grid = GetPropType<TypeTag, Properties::Grid>;
    using GridView = typename GetPropType<TypeTag, Properties::GridGeometry>::GridView;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using CoordScalar = typename GridView::ctype;
    using Indices = GetPropType<TypeTag, Properties::Indices>;

    enum
    {
        dim = GridView::dimension,
        dimWorld = Grid::dimensionworld,
        wPhaseIdx = Indices::wPhaseIdx,
        nPhaseIdx = Indices::nPhaseIdx,
        numPhases = getPropValue<TypeTag, Properties::NumPhases>()
    };
    enum VEModel
    {
        sharpInterface,
        capillaryFringe
    };

    using Element = typename Grid::Traits::template Codim<0>::Entity;
    using GlobalPosition = Dune::FieldVector<CoordScalar, dimWorld>;
    typedef Dune::FieldMatrix<Scalar, dim, dim> FieldMatrix;

    using CellData = GetPropType<TypeTag, Properties::CellData>;

public:
    using MaterialLaw = GetPropType<TypeTag, Properties::MaterialLaw>;
    using MaterialLawParams = typename MaterialLaw::Params;


    /*!
     * \brief This is called from the problem and creates a gnuplot output
     *        of e.g the pc-Sw curve
     */
    void plotMaterialLaw()
    {
        PlotMaterialLaw<Scalar, MaterialLaw> plotMaterialLaw;
        GnuplotInterface<Scalar> gnuplot(plotFluidMatrixInteractions_);
        gnuplot.setOpenPlotWindow(plotFluidMatrixInteractions_);
        plotMaterialLaw.addpcswcurve(gnuplot, materialLawParams_);
        plotMaterialLaw.addkrcurves(gnuplot, materialLawParams_);
    }

    const FieldMatrix& intrinsicPermeability(const Element& element) const
    {
        GlobalPosition globalPos = element.geometry().center();
        const int eIdxGlobal = problem_.variables().index(element);
        const CellData& cellData = problem_.variables().cellData(eIdxGlobal);
        const int veModel = cellData.veModel();
        if (globalPos[0] > 30 && globalPos[0] < 40)
        {
            if(veModel == sharpInterface || veModel == capillaryFringe)
            {
                return permeabilityLenseVE_;
            }
            else
            {
                const auto ySphere = (((std::pow(getParam<Scalar>("Grid.DomeVerschiebungOutside"),2.0))+(std::pow((getParam<std::vector<Scalar>>("Grid.Radial0")[1]),2.0))-(std::pow((getParam<std::vector<Scalar>>("Grid.Axial2")[1]),2.0)))/(((getParam<std::vector<Scalar>>("Grid.Axial2")[1])-getParam<Scalar>("Grid.DomeVerschiebungOutside"))*2.0));
                const auto sphereRadius = ySphere + (getParam<std::vector<Scalar>>("Grid.Radial0")[1]);
                const auto radialDistance = (std::sqrt((std::pow(globalPos[0],2.0))+(std::pow(globalPos[1],2.0))));
                const auto differenceTopToDome = (sphereRadius - (std::sqrt(((std::pow(sphereRadius,2.0))-((std::pow(radialDistance,2.0)))))));
                globalPos[dim-1] = globalPos[dim-1] + differenceTopToDome - ((getParam<std::vector<Scalar>>("Grid.Axial2")[1]) - getParam<Scalar>("Grid.DomeVerschiebungOutside"));
                if(globalPos[dim-1] > 10 && globalPos[dim-1] < 20)
                    return permeabilityLense_;
            }
        }
        else if (globalPos[0] > 120 && globalPos[0] < 140)
        {
            if(veModel == sharpInterface || veModel == capillaryFringe)
            {
                return permeabilityLenseVE_;
            }
            else
            {
                const auto ySphere = (((std::pow(getParam<Scalar>("Grid.DomeVerschiebungOutside"),2.0))+(std::pow((getParam<std::vector<Scalar>>("Grid.Radial0")[1]),2.0))-(std::pow((getParam<std::vector<Scalar>>("Grid.Axial2")[1]),2.0)))/(((getParam<std::vector<Scalar>>("Grid.Axial2")[1])-getParam<Scalar>("Grid.DomeVerschiebungOutside"))*2.0));
                const auto sphereRadius = ySphere + (getParam<std::vector<Scalar>>("Grid.Radial0")[1]);
                const auto radialDistance = (std::sqrt((std::pow(globalPos[0],2.0))+(std::pow(globalPos[1],2.0))));
                const auto differenceTopToDome = (sphereRadius - (std::sqrt(((std::pow(sphereRadius,2.0))-((std::pow(radialDistance,2.0)))))));
                globalPos[dim-1] = globalPos[dim-1] + differenceTopToDome - ((getParam<std::vector<Scalar>>("Grid.Axial2")[1]) - getParam<Scalar>("Grid.DomeVerschiebungOutside"));
                if(globalPos[dim-1] > 20)
                    return permeabilityLense_;
            }
        }
        return permeability_;
    }

    double porosity(const Element& element) const
    {
        return porosity_;
    }


    // return the parameter object for the Brooks-Corey material law which depends on the position
//    const MaterialLawParams& materialLawParamsAtPos(const GlobalPosition& globalPos) const
    const MaterialLawParams& materialLawParams(const Element& element) const
    {
            return materialLawParams_;
    }

    //set the residual saturations for the phases
    void setResidualSaturation(Scalar residualSat[numPhases])
    {
        materialLawParams_.setSwr(residualSat[wPhaseIdx]);
        materialLawParams_.setSnr(residualSat[nPhaseIdx]);
    }


    StorageMultiDimSpatialParams(const Problem& problem)
    : ParentType(problem), permeability_(0), permeabilityLense_(0), permeabilityLenseVE_(0), problem_(problem)
    {
        // residual saturations
        materialLawParams_.setSwr(0.0);
        materialLawParams_.setSnr(0.1);

        plotFluidMatrixInteractions_ = getParam<bool>("Output.PlotFluidMatrixInteractions");
        Scalar lambda = getParam<Scalar>("SpatialParams.Lambda");
        Scalar entryPressure = getParam<Scalar>("SpatialParams.EntryPressure");
        Scalar maxPc = getParam<Scalar>("SpatialParams.MaxPc");
        Scalar exponent = getParam<Scalar>("SpatialParams.Exponent");
        Scalar permeabilityH = getParam<Scalar>("SpatialParams.PermeabilityHorizontal");
        Scalar permeabilityV = getParam<Scalar>("SpatialParams.PermeabilityVertical");
        Scalar porosity = getParam<Scalar>("SpatialParams.Porosity");

//        // parameters for the Brooks-Corey Law
//        // entry pressures
        materialLawParams_.setPe(entryPressure);
//        // Brooks-Corey shape parameters
        materialLawParams_.setLambda(lambda);

        // parameters for the linear law
//        materialLawParams_.setEntryPc(entryPressure);
//        materialLawParams_.setMaxPc(maxPc);

        // parameters for the exponential law
//        materialLawParams_.setPe(entryPressure);
//        materialLawParams_.setMaxPc(maxPc);
//        materialLawParams_.setExponent(exponent);

        permeability_[0][0] = permeabilityH;
        permeability_[1][1] = permeabilityH;
        permeability_[2][2] = permeabilityV;
        permeabilityLense_[0][0] = getParam<Scalar>("SpatialParams.PermeabilityLens");;
        permeabilityLense_[1][1] = getParam<Scalar>("SpatialParams.PermeabilityLens");;
        permeabilityLense_[2][2] = getParam<Scalar>("SpatialParams.PermeabilityLens");;
        permeabilityLenseVE_[0][0] = 1.49408e-14; // this is a dummy, the real value is set in fvpressure
        permeabilityLenseVE_[1][1] = 1.49408e-14;
        permeabilityLenseVE_[2][2] = 1.49408e-14;
        porosity_ = porosity;
    }

private:
    MaterialLawParams materialLawParams_;
    bool plotFluidMatrixInteractions_;
    FieldMatrix permeability_, permeabilityLense_, permeabilityLenseVE_;
    Scalar porosity_;
    const Problem& problem_;
};

} // end namespace
#endif
